//
//  ContainerViewViewController.h
//  横向三级选择
//
//  Created by 邵瑞 on 15/10/30.
//  Copyright © 2015年 wonders_hengxiangSelect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContainerViewViewController : UIViewController
+(ContainerViewViewController *)shareContainerView;
-(void)showShareType;
@end

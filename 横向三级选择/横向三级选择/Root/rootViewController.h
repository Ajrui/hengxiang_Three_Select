//
//  rootViewController.h
//  横向三级选择
//
//  Created by 邵瑞 on 15/10/27.
//  Copyright © 2015年 wonders_hengxiangSelect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface rootViewController : UIViewController<UIScrollViewDelegate>
@property (strong,nonatomic)UIScrollView * ContainerTab;
@end

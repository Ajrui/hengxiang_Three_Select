//
//  rootViewController.m
//  横向三级选择
//
//  Created by 邵瑞 on 15/10/27.
//  Copyright © 2015年 wonders_hengxiangSelect. All rights reserved.
//

#import "rootViewController.h"
#import "ContainerViewViewController.h"
#import "ContainerView.h"
@interface rootViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *viewOut;

@end

@implementation rootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
////    <a href="#">
////    <img src="http://192.168.1.235:8080/images/logo/20151026095841_978.png" width="234" height="133" border="0"/>
////    </a>
//    self.title =  @"横向三级菜单选择Demo";
//    NSString *st = @"\n重庆名豪实业（集团）股份有限公司（以下简称“名豪集团”）成立于1993年，在职员工近4000名，现已发展成为一个集地产、旅游、生态农业、精品百货、高端酒店、物业管理等产业于一体的多元化集团公司。\n20年来，名豪集团立足重庆，延展至贵州，产业布局永川、梁平、璧山、大足、江津、城口、道真等地，坚持打造城市综合体的开发理念，先后完成投资建设璧山名豪商贸区、梁平名豪商贸区、永川名豪商贸区、江津名豪商贸区、大足名豪商贸区等项目，旗下拥有永川名豪国际酒店（五星级）、梁平名豪国际酒店（四星级）、永川名豪购物广场、璧山名豪购物广场、江津名豪精品超市等产业，并于2012年启动90万方泛旅游综合体项目，其中包括53万方城市综合体——城口名豪百年商业文化广场项目。2014年启动了占地50平方公里的大沙河仡佬文化国际生态旅游度假区项目。\n      名豪集团在重庆民营经济领域取得了杰出成就，董事长周勇先生被评为“中国首届经济百名杰出人物”；名豪商业地产开发模式列入了重庆大学MBA教材，被称为“国内泛商业地产开发先驱”。\n      名豪集团先后荣获重庆市房地产开发五十强、重庆市企业法人知名品牌、重庆市优秀民营企业、重庆市服务企业50强、重庆市百户就业先进民营企业等荣誉称号";
//    NSString *htmlString = @"1、可以最大限度地实现劳动用工的科学配置；\n \n2、有效地防止因职务重叠而发生的工作扯皮现象；\n \n3、提高内部竞争活力，更好地发现和使用人才；\n \n4、是组织考核的依据；\n \n5、提高工作效率和工作质量；\n \n6、规范操作行为；\n \n7、减少违章行为和违章事故的发生。";
//    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    _textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    [self.textView setAttributedText:attributedString];
//    self.textView.text =[NSString stringWithFormat:@"%@",st];
//    [self.webView loadHTMLString:st baseURL:nil];
//    
//    
//    
//    NSError * error;
//    
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"localType" ofType:@"json"];
//    
//    
//    NSString * sssww1 = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
//
//    
//    NSData *data11 =[NSData dataWithContentsOfFile:sssww1];
//    
//    NSDictionary * dicPathJosn = [NSDictionary dictionaryWithContentsOfFile:path];
//    
//    
//    
//    id JsonObject=[NSJSONSerialization JSONObjectWithData:[sssww1 dataUsingEncoding:NSUTF8StringEncoding]
//                                                  options:NSJSONReadingMutableLeaves
//                                                    error:&error];
//    
    
    UIWindow * windowFrame = [UIApplication sharedApplication].keyWindow;

    
    //    初始化添加tabView
    self.ContainerTab = [[UIScrollView alloc] init];
    self.ContainerTab.translatesAutoresizingMaskIntoConstraints = NO;
    self.ContainerTab.userInteractionEnabled = YES;
    CGRect rectMake = CGRectMake(0, 30, windowFrame.frame.size.width, windowFrame.frame.size.height/2-30);
    [self.ContainerTab setFrame:rectMake];
    [self.ContainerTab setBackgroundColor:[UIColor redColor]];
    [self.ContainerTab setDelegate:self];
    [self.ContainerTab setScrollEnabled:YES];
    [self.ContainerTab setScrollsToTop:YES];
    [self.ContainerTab setPagingEnabled:YES];
    
//    [self.ContainerTab setShowsHorizontalScrollIndicator:YES];
//    NSInteger numberOfImages = 3;
//    for (int i = 0; i< numberOfImages; i++)
//        
//    {
//        
//        UIImageView * imageV = [[UIImageView alloc]initWithFrame:CGRectMake(rectMake.size.width*i, 0, rectMake.size.width, rectMake.size.height)];
//        
//        imageV.contentMode = UIViewContentModeTop;
//        
//        UIImage * image = [UIImage imageNamed:  [NSString stringWithFormat:@"%d.png",i+1] ];
//        
//        imageV.image = image;
//        
//
//        
//        
//        [ self.ContainerTab addSubview:imageV];
//        
//    }
//    
//    self.ContainerTab.contentSize = CGSizeMake(rectMake.size.width * numberOfImages,  windowFrame.frame.size.height/2);
//    
//    
//    [self.view addSubview:self.ContainerTab];
//    
//        NSLayoutConstraint * hightValues = [NSLayoutConstraint constraintWithItem:self.ContainerTab attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.00 constant:20];
//    
//    [self.view addConstraint:hightValues];
//    
//            NSLayoutConstraint *  Bottom = [NSLayoutConstraint constraintWithItem:self.ContainerTab attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.00 constant:400];
//    
//    [self.view addConstraint:Bottom];
//    
//    NSLayoutConstraint *  right80 = [NSLayoutConstraint constraintWithItem:self.ContainerTab attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.00 constant:80];
//    [self.view addConstraint:right80];
//
//    
//    NSLayoutConstraint *  left80 = [NSLayoutConstraint constraintWithItem:self.ContainerTab attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.00 constant:80];
//    [self.view addConstraint:left80];

//    [self.view setBackgroundColor:[UIColor grayColor]];
    

        //    //设置父view允许子view自动布局
        //    self.autoresizesSubviews=YES;


    
    
//    [self.view addSubview:[[ContainerViewViewController shareContainerView] view]];
    
//    [self.view bringSubviewToFront:[[ContainerViewViewController shareContainerView] view]];
//    [[ContainerViewViewController shareContainerView] showShareType];
    // Do any additional setup after loading the view from its nib.
    
    [[ContainerView shareContainerView]showShareType];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

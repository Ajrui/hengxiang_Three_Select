//
//  NSString+JSON.h
//  横向三级选择
//
//  Created by 邵瑞 on 15/10/29.
//  Copyright © 2015年 wonders_hengxiangSelect. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString_JSON : NSString
+(NSString *) jsonStringWithDictionary:(NSDictionary *)dictionary;
+(NSString *) jsonStringWithArray:(NSArray *)array;
+(NSString *) jsonStringWithString:(NSString *) string;
+(NSString *) jsonStringWithObject:(id) object;

@end

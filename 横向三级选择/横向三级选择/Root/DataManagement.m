//
//  DataManagement.m
//  横向三级选择
//
//  Created by 邵瑞 on 15/10/30.
//  Copyright © 2015年 wonders_hengxiangSelect. All rights reserved.
//

#import "DataManagement.h"

@implementation DataManagement
//第一步：静态实例，并初始化
static DataManagement * manageData;

+(DataManagement *)shareDataManage
{

    static dispatch_once_t  onceToken;
    dispatch_once(&onceToken, ^{
        manageData = [[DataManagement alloc] init];
        
    });
    
        return manageData;
}


-(NSDictionary *)getALocalPathToJsonStrToDicObject:(NSString *)loalFileName andFileType:(NSString *)fileType
{
    
    NSDictionary * Dictionary = [[NSDictionary alloc] init];
    
    NSError * error;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"localType" ofType:@"json"];
    
    
    NSString * josnStr = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];

    
Dictionary =[NSJSONSerialization JSONObjectWithData:[ josnStr dataUsingEncoding:NSUTF8StringEncoding]
                                                  options:NSJSONReadingMutableLeaves
                                                    error:&error];
    
    return Dictionary;

}
//+(DataManagement *)shareDataManag
//{
////第一步：静态实例，并初始化
//    static DataManagement * dataManage;
//    
//    @synchronized(self) {
//        if (dataManage == nil) {
//            
//        dataManage  = [[DataManagement alloc] init];
//    }
//    }
//    return dataManage;
//}

@end

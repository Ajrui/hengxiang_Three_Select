//
//  ContainerView.m
//  横向三级选择
//
//  Created by 邵瑞 on 15/10/30.
//  Copyright © 2015年 wonders_hengxiangSelect. All rights reserved.
//

#import "ContainerView.h"
#import "TableViewCell.h"
@implementation ContainerView





static  ContainerView * shareContainer;

+(ContainerView *)shareContainerView
{
    
    @synchronized(self)
    {
        if (!shareContainer) {
            shareContainer = [[ContainerView alloc] init];
        }
    }
    return shareContainer;
    
}
-(instancetype)init
{
    if (self) {
        self  = [super init];
    }
    return self;
}
-(void)showShareType
{
    
    

    UIWindow * windowFrame = [UIApplication sharedApplication].keyWindow;
    windowFrame.translatesAutoresizingMaskIntoConstraints = NO;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    CGRect  selfRect  =CGRectMake(0,windowFrame.frame.size.height/2,  windowFrame.frame.size.width,windowFrame.frame.size.height/2);
    [self setFrame:selfRect];
    
    [self setBackgroundColor:[UIColor orangeColor]];
    
    [windowFrame addSubview:self];
    NSLayoutConstraint *  BottomLow = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:windowFrame attribute:NSLayoutAttributeBottom multiplier:1.00 constant:0];
    
    [windowFrame addConstraint:BottomLow];
    
    NSLayoutConstraint *  rightLow = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:windowFrame attribute:NSLayoutAttributeTrailing multiplier:1.00 constant:0];
    [windowFrame addConstraint:rightLow];
    
    
    NSLayoutConstraint *  leftLow = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:windowFrame attribute:NSLayoutAttributeLeading multiplier:1.00 constant:0];
    [windowFrame addConstraint:leftLow];
    
    
//    /////////////
    
    self.view  =[[UIView alloc] init];
    self.view.frame = CGRectMake(0,0,  windowFrame.frame.size.width,  windowFrame.frame.size.height);
    [self.view setBackgroundColor: [UIColor redColor]];
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.view];
    
//        self
        NSLayoutConstraint * top1 = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.00 constant:windowFrame.frame.size.height/2];
    
        [self addConstraint:top1];
    
    NSLayoutConstraint *  Bottom1 = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.00 constant:0];
    
    [self addConstraint:Bottom1];
    
    NSLayoutConstraint *  right = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.00 constant:0];
    [self addConstraint:right];
    
    
    NSLayoutConstraint *  left = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.00 constant:0];
    [self addConstraint:left];
    
    //    准备添加第二层的View
    self.viewLead  =[[UIView alloc] init];
    self.viewLead.translatesAutoresizingMaskIntoConstraints = NO;
    [self.viewLead setFrame:CGRectMake(0, 0,windowFrame.frame.size.width,30)];
    //  第二层view的颜色
    self.viewLead.backgroundColor = [UIColor greenColor];
    //添加第二层子视图
    [self.view addSubview:self.viewLead];

//        viewLead
//        NSLayoutConstraint * top2 = [NSLayoutConstraint constraintWithItem:self.viewLead attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.00 constant:0.0];
//    
//        [self addConstraint:top2];
    
        NSLayoutConstraint *  Bottom2 = [NSLayoutConstraint constraintWithItem:self.viewLead attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.00 constant:0];
    
        [self addConstraint:Bottom2];
    
        NSLayoutConstraint *  right2 = [NSLayoutConstraint constraintWithItem:self.viewLead attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.00 constant:0];
        [self addConstraint:right2];
    
    
        NSLayoutConstraint *  left2 = [NSLayoutConstraint constraintWithItem:self.viewLead attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.00 constant:0];
        [self addConstraint:left2];

        self.labAddViewLead  = [[UILabel alloc] init];
        self.labAddViewLead.frame = self.viewLead.bounds;
        self.labAddViewLead.translatesAutoresizingMaskIntoConstraints = NO;
        [self.labAddViewLead setText:@"重庆市"];
        [self.labAddViewLead setTextAlignment:NSTextAlignmentCenter];
        [self.labAddViewLead setTextColor:[UIColor grayColor]];
        [self.viewLead addSubview:self.labAddViewLead];
    //    viewLead
    NSLayoutConstraint * top3 = [NSLayoutConstraint constraintWithItem:self.labAddViewLead attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.viewLead attribute:NSLayoutAttributeTop multiplier:1.00 constant:0.0];
    
    [self.viewLead addConstraint:top3];
    
    NSLayoutConstraint *  Bottom3 = [NSLayoutConstraint constraintWithItem:self.labAddViewLead attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.viewLead attribute:NSLayoutAttributeBottom multiplier:1.00 constant:0];
    
    [self.viewLead addConstraint:Bottom3];
    
    NSLayoutConstraint *  right3 = [NSLayoutConstraint constraintWithItem:self.labAddViewLead attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.viewLead attribute:NSLayoutAttributeTrailing multiplier:1.00 constant:0];
    [self.viewLead addConstraint:right3];
    
    
    NSLayoutConstraint *  left3 = [NSLayoutConstraint constraintWithItem:self.labAddViewLead attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.viewLead attribute:NSLayoutAttributeLeading multiplier:1.00 constant:0];
    [self.viewLead addConstraint:left3];
    

    
    
    


////看不见背景颜色
////    [self setBackgroundColor:[UIColor redColor]];
//    
//
//    
//    
//
//
////

//    
//    
//    

//    
//
//    
//    
//    
//    
//    
//
///
//    
//    
////    初始化添加tabView
//    self.ContainerTab = [[UIScrollView alloc] init];
//   
//    self.ContainerTab.userInteractionEnabled = YES;
//    self.ContainerTab.translatesAutoresizingMaskIntoConstraints = NO;
//
//    CGRect rectMake = CGRectMake(0, 30, windowFrame.frame.size.width, windowFrame.frame.size.height/2-30);
////    [self.ContainerTab setFrame:rectMake];
//
//    [self.ContainerTab setBackgroundColor:[UIColor orangeColor]];
//    [self.ContainerTab setDelegate:self];
//    [self.ContainerTab setScrollEnabled:YES];
//    [self.ContainerTab setScrollsToTop:YES];
//    [self.ContainerTab setPagingEnabled:YES];
//    [self.ContainerTab setShowsHorizontalScrollIndicator:YES];
//
//    
//    [self.view addSubview:self.ContainerTab];
//    
//    
//    
//    
//    [self addSubview:self.view]
//    
//    
//    ;
//
//
//        NSInteger numberOfImages = 3;
//        for (int i = 0; i< numberOfImages; i++)
//    
//        {
//    
//            UIImageView * imageV = [[UIImageView alloc]initWithFrame:CGRectMake(rectMake.size.width*i, 0, rectMake.size.width, rectMake.size.height)];
//    
//            imageV.contentMode = UIViewContentModeScaleToFill;
//    
//            UIImage * image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.png",i] ];
//    
//            imageV.image = image;
//    
//    //        [windowFrame bringSubviewToFront:imageV];
//    
//    
//            [ self.ContainerTab addSubview:imageV];
//    
//        }
//    
//    
//    
//    
//    self.ContainerTab.contentSize = CGSizeMake(rectMake.size.width *numberOfImages,  windowFrame.frame.size.height/2);
//    
//    
//    
////    sc
//    self.ContainerTab.contentSize = CGSizeMake(rectMake.size.width *numberOfImages,  windowFrame.frame.size.height/2);
//    NSLayoutConstraint * hightValues = [NSLayoutConstraint constraintWithItem:self.ContainerTab attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.00 constant:30];
//    
//    [self addConstraint:hightValues];
//    
//    NSLayoutConstraint *  Bottom = [NSLayoutConstraint constraintWithItem:self.ContainerTab attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.00 constant:0];
//    
//    [self addConstraint:Bottom];
//    
//    NSLayoutConstraint *  right80 = [NSLayoutConstraint constraintWithItem:self.ContainerTab attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.00 constant:0];
//    [self.view addConstraint:right80];
//    
//    
//    NSLayoutConstraint *  left80 = [NSLayoutConstraint constraintWithItem:self.ContainerTab attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.00 constant:0];
//    [self.view addConstraint:left80];
    

    
//    
//    NSInteger numberOfImages = 3;
//    for (int i = 0; i< numberOfImages; i++)
//        
//    {
//        
//        UIImageView * imageV = [[UIImageView alloc]initWithFrame:CGRectMake(rectMake.size.width*i+1, 0, rectMake.size.width, rectMake.size.height)];
//        
//        imageV.contentMode = UIViewContentModeScaleToFill;
//        
//        UIImage * image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.png",i] ];
//        
//        imageV.image = image;
//        
////        [windowFrame bringSubviewToFront:imageV];
//
//        
//        [ self.ContainerTab addSubview:imageV];
//        
//    }
//    UIImageView * imageV = [[UIImageView alloc]initWithFrame:CGRectMake(rectMake.size.width, 0, rectMake.size.width, rectMake.size.height)];
//    
//    imageV.contentMode = UIViewContentModeScaleToFill;
//    
//    UIImage * image = [UIImage imageNamed:[NSString stringWithFormat:@"1.png"] ];
//    
//    imageV.image = image;
//    
//    //        [windowFrame bringSubviewToFront:imageV];
//
//    
//    [ self.ContainerTab addSubview:imageV];

//    self.ContainerTab.contentSize = CGSizeMake(rectMake.size.width * numberOfImages,  windowFrame.frame.size.height/2);
    


    //    [windowFrame bringSubviewToFront:self.ContainerTab];
    
    
    
    //  ContainerViewTab  用于 CGSize contentSize ：设置UIScrollView的滚动范围
//    self.ContainerViewTab = [[UIView alloc] init];
//    CGRect rectMakeView = CGRectMake(10, 10, windowFrame.frame.size.width*8,self.ContainerTab.frame.size.height-20);
////    设置frame
//    [self.ContainerViewTab setFrame:rectMakeView];
//
////    [self.ContainerTab setContentSize:_ContainerViewTab.frame.size];
////    [self.ContainerTab setContentSize:CGSizeMake(self.view.frame.size.width*2, 500)];

//    [self.ContainerTab addSubview:self.ContainerViewTab];
    
    
//    UIView * viewCC = [[ UIView alloc] init];
//    viewCC.frame =  CGRectMake(10, 10, 440, 600);
//    [viewCC setBackgroundColor:[UIColor redColor]];
//    [self.ContainerTab addSubview:viewCC];
   
    
//    self.TabOne = [[UITableView alloc] init];
//    CGRect rectMakeTabOne = CGRectMake(0, 0, rectMakeView.size.width/3,self.ContainerTab.frame.size.height);
//    [self.TabOne setBackgroundColor:[UIColor grayColor]];
//    [self.TabOne setDataSource:self];
//    [self.TabOne setDelegate:self];
//    [self.TabOne setTag:1000];
//
//    [self.TabOne setFrame:rectMakeTabOne];
//    [self.ContainerTab addSubview:self.TabOne];
//    
//    
//    self.tabTwo = [[UITableView alloc] init];
//    CGRect rectMakeTabTwo = CGRectMake( self.TabOne.frame.size.width, 0, rectMakeView.size.width/3,self.ContainerTab.frame.size.height);
//    [self.tabTwo setBackgroundColor:[UIColor grayColor]];
//    [self.tabTwo setDataSource:self];
//    [self.tabTwo setDelegate:self];
//    [self.tabTwo setTag:2000];
//
//    [self.tabTwo setFrame:rectMakeTabTwo];
//    [self.ContainerTab addSubview:self.tabTwo];
//    
//    self.tabThree = [[UITableView alloc] init];
//    CGRect rectMakeTabtabThree = CGRectMake( self.tabTwo.frame.size.width, 0, rectMakeView.size.width/3,self.ContainerTab.frame.size.height);
//    [self.tabThree setBackgroundColor:[UIColor grayColor]];
//    [self.tabThree setTag:3000];
//    [self.tabThree setDataSource:self];
//    [self.tabThree setDelegate:self];
//    
//    [self.tabThree setFrame:rectMakeTabtabThree];
//    
//    [self.ContainerTab addSubview:self.tabThree];
    


//    [windowFrame addSubview:self];
//    [windowFrame bringSubviewToFront:self.view];
//    
//    UINib * nibTabOne = [UINib nibWithNibName:NSStringFromClass([TableViewCell class]) bundle:nil];
//    [self.TabOne registerNib:nibTabOne forCellReuseIdentifier:[NSString stringWithFormat:@"%@",[TableViewCell class]]];
//    self.TabOne.separatorStyle = UITableViewCellSeparatorStyleNone;
//    
//
//    UINib * nibTabTwo = [UINib nibWithNibName:NSStringFromClass([TableViewCell class]) bundle:nil];
//    [self.tabTwo registerNib:nibTabTwo forCellReuseIdentifier:[NSString stringWithFormat:@"%@",[TableViewCell class]]];
//    self.tabTwo.separatorStyle = UITableViewCellSeparatorStyleNone;
//    
//    UINib * nibTabThree = [UINib nibWithNibName:NSStringFromClass([TableViewCell class]) bundle:nil];
//    [self.tabThree registerNib:nibTabThree forCellReuseIdentifier:[NSString stringWithFormat:@"%@",[TableViewCell class]]];
//    self.tabThree.separatorStyle = UITableViewCellSeparatorStyleNone;


    
    
    
//    wind
    //    sc
//    self.ContainerTab.contentSize = CGSizeMake(rectMake.size.width *numberOfImages,  windowFrame.frame.size.height/2);
//    NSLayoutConstraint * topLow = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:windowFrame attribute:NSLayoutAttributeTop multiplier:1.00 constant:windowFrame.frame.size.height/2];
    

//    [windowFrame addConstraint:topLow];
 

    
    
    

//
//    
//    
//    self.translatesAutoresizingMaskIntoConstraints = NO;
//
//    windowFrame.backgroundColor = [UIColor redColor];
//    //
//    CGRect rect =  self.view.frame;
//    [self setFrame:rect];
//    self.view.hidden = NO;
//    self.hidden = NO;
//    
//    [windowFrame addSubview:self];
//    
//    
//    
//    NSDictionary *views = NSDictionaryOfVariableBindings(self.viewLead);
    
  
    
    
    
    
    
    


    
//    NSLayoutConstraint * hightValues = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.00 constant:windowFrame.frame.size.height/2];
//    
//    NSLayoutConstraint *  Bottom = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:windowFrame attribute:NSLayoutAttributeBottom multiplier:1.00 constant:0];
//    //    //设置父view允许子view自动布局
//    //    self.autoresizesSubviews=YES;
//    NSLayoutConstraint *  right0 = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:windowFrame attribute:NSLayoutAttributeTrailing multiplier:1.00 constant:0];
//    NSLayoutConstraint *  left0 = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:windowFrame attribute:NSLayoutAttributeLeading multiplier:1.00 constant:0];
//    
//    
//    
//    [windowFrame addConstraint:Bottom];
//    [windowFrame addConstraint:right0];
//    [windowFrame addConstraint:left0];
//    
//    [self.view addConstraint:hightValues];
    
//
//    self.transform = CGAffineTransformMakeScale(1/300.0f, 1/270.0f);
//    self.alpha = 0;
//    [UIView animateWithDuration:0.35f animations:^{
//        self.transform = CGAffineTransformMakeScale(1, 1);
//        self.alpha = 1;
//    } completion:^(BOOL finished) {
//        
//    }];
//    
//    
//    
//    
//    
    
    
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1000) {
        static NSString * identifer = @"TableViewCell";
        TableViewCell * cell = (TableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifer];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        

        
        return cell;
        
        
    }else if (tableView.tag == 2000)
    {
        
        static NSString * identifer = @"TableViewCell";
        TableViewCell * cell = (TableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifer];
   
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        
        return cell;
        
    }else if (tableView.tag == 3000)
    {
        
        static NSString * identifer = @"TableViewCell";
        TableViewCell * cell = (TableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifer];
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        
        return cell;
        
    }
    
    else
        return nil;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

//
//  ContainerView.h
//  横向三级选择
//
//  Created by 邵瑞 on 15/10/30.
//  Copyright © 2015年 wonders_hengxiangSelect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContainerView : UIView<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
/***包含总的容器View***/
@property (strong, nonatomic)  UIView *view;
/***顶部的ViewLead显示城市或者其他***/
@property (strong, nonatomic) UIView *viewLead;
/***显示市区位置***/
@property (strong, nonatomic) UILabel *labAddViewLead;
/***用于装载横向滑动的三个Tab***/
@property(strong,nonatomic) UIScrollView * ContainerTab;
/***套装容的Content width ***/
@property (strong, nonatomic) UIView * ContainerViewTab;

/***用于用于一级显示专业类型或者职位类型***/
@property(strong,nonatomic) UITableView * TabOne;
/***用于用于二级显示专业类型或者职位类型***/
@property(strong,nonatomic) UITableView * tabTwo;
/***用于用于三级显示专业类型或者职位类型
 *有则显示没有则不显示*
 ***/
@property(strong,nonatomic) UITableView * tabThree;

+(ContainerView *)shareContainerView;

-(void)showShareType;

@end

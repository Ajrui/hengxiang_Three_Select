//
//  DataManagement.h
//  横向三级选择
//
//  Created by 邵瑞 on 15/10/30.
//  Copyright © 2015年 wonders_hengxiangSelect. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManagement : NSObject
/***
 *GCD线程单列
 *意味着代码仅会被运行一次，而且还是线程安全的
 ***/
+(DataManagement *)shareDataManage;
/***
 *加锁，保持单例的唯一性(防止同时申请多个对象)
 *
 ***/

//+(DataManagement *)shareDataManag;

/***得到一个字典返回一个可变数***/
-(NSMutableArray * )getADicObcject:(NSDictionary *)desc;
/***返回一级数据查询出来的NSMutableArr***/
-(NSMutableArray *)getLevelOneID:(NSNumber *)idNumOne;
/***返回由级数据的父id查询出来的二级数据NSMutableArr***/
-(NSMutableArray *)getLevelTwoID:(NSNumber *)idNumTwo;
/***返回由二级数据父id查询出来三级子数据的NSMutableArr***/
-(NSMutableArray *)getLevelThreeID:(NSNumber *)idNumThree;
/**
 *返回由一级数据id查询出来的NSMutableArr *
 *次接口为预留接口*
 ***/
-(NSMutableArray *)getLevelOForID:(NSNumber *)idNumFour;
/***
 *得到本地json文件路径解析为字符串之后转发json object 由 NSDictionary 来接受*
 *loalFileName 本地文件名称*
 *fileType 本地文件名称类型 如 json text xml db sqlite *
 ***/
-(NSDictionary *)getALocalPathToJsonStrToDicObject:(NSString *)loalFileName andFileType:(NSString *)fileType;

@end

//
//  Common.h
//  ON265
//
//  Created by Suto Dai on 12-4-12.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


//#import "JSON.h"

@interface Common : NSObject

//-------------------------------User
//是否登陆
+(BOOL)isLogin;
//得到登陆用户Id
+(NSString *)getUserId;
//用户登出
+(void)logoutUser;

//得到用户上下文
+(NSUserDefaults *) getUserDefaults;
//设置用户上下文
+(void) setUserDefaults:(id) value key:(NSString *) key;

//------------------------------JSON
//JSON解析
+(id)parserJsonString:(NSString *)jsonString;
//转化成JSON
+(NSString *)jsonStringWithObject:(id)object;


//-------------------------------Encryption
//-----MD5
+(NSString *)md5_16:(NSString *)str;
+(NSString *)md5_32:(NSString *)str;
//DES加密
+(NSString  *)DESPassword:(NSString *)pw WithKey:(NSString *)key_des;


//------------------------------Coding
//Base64编码
+ (NSString * )encodeBase64:(NSString * )input;
//Base64解码
+ (NSString * )decodeBase64:(NSString * )input;


//-------------------------------Else
//得到运营商
+(NSString*)getCarrier;

//得到文件路径
+ (NSString *) documentsFilePath:(NSString *) fileName;

//字符串折行
+ (NSString *) stringWrap:(NSString *)Word;

//得到iOS版本
+ (double) getSystemVersion;



@end

//
//  MainTabbarController.h
//  smartVillageApp
//
//  Created by Jabez on 25/4/15.
//  Copyright (c) 2015 ihome023.com All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopLineView.h"
#import "EAIntroPage.h"
#import "EAIntroView.h"
@class BarItemAnimation;

@interface MainTabbarController : UIViewController<EAIntroDelegate>

@property (weak, nonatomic) IBOutlet BarItemAnimation *btnHome;
@property (weak, nonatomic) IBOutlet BarItemAnimation *btnMerchants;

@property (weak, nonatomic) IBOutlet BarItemAnimation *btnMe;
@property (weak, nonatomic) IBOutlet BarItemAnimation *btnPurchaseCar;

@property (weak, nonatomic) IBOutlet UIView *vMainContent;
@property (weak, nonatomic) IBOutlet TopLineView *TopLine;

+(MainTabbarController *)ShareTabBarController;
-(void)checkIsUserLogin;

-(BOOL)SelectTab:(NSInteger)index;

-(void)showMessage:(NSString *)message;
-(NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;


@end

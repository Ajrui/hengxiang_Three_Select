//
//  BarItemAnimation.h
//  smartVillageApp
//
//  Created by Jabez on 25/4/15.
//  Copyright (c) 2015 osoons. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BarItemAnimation : UIButton
@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIImageView *imvMain;
@property (strong, nonatomic) IBOutlet UILabel     *lalTitle;


@property (strong, nonatomic) UIImage *imHight;
@property (strong, nonatomic) UIImage *imNormal;

-(void)PlayAnimation;

@end

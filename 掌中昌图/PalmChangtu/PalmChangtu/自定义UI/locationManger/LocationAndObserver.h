//
//  LocationAndObserver.h
//  GroupDeal
//
//  Created by lewang on 14-7-16.
//  Copyright (c) 2014年 groupdeal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
//#import <GoogleMaps/GoogleMaps.h>



@class GMSAddress;
@protocol LocationObserver <NSObject>

@optional
- (void)GeocodeLocation:(CLPlacemark *)placemark;
- (void)LocationDidChange:(CLLocation *)updatedLocation;

@end


@interface LocationAndObserver : NSObject<CLLocationManagerDelegate>


@property (nonatomic, readonly)CLLocation *FinalLocation;
@property (nonatomic, readonly)BOOL isGetLocation;//是否已取得坐标
@property (nonatomic, readonly)BOOL isGetGeoCode; //是否已经取得反地理


@property (nonatomic, readonly)CLPlacemark *placemark;
@property (nonatomic, strong) CLLocationManager* locationManager;
@property (nonatomic, strong) NSMutableArray *observers;

-(void)ReverseGeocodeLocation;
-(void)UpdateLocation;

+ (LocationAndObserver*)sharedSingleton;

//取距离
+(double )distanceLoca1:(CLLocationCoordinate2D)loca1 ToLoca2:(CLLocationCoordinate2D)loca2;

@end

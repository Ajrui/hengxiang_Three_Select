//
//  GeocodeShare.h
//  smart_village
//
//  Created by Jabez on 10/3/15.
//  Copyright (c) 2015 osoons. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface GeocodeShare : NSObject

//CLPlacemark *placemark;

//显示是否刷新成功
@property(nonatomic, readonly)NSNumber *isGetLocation;
@property(nonatomic, readonly)NSNumber *isGetGecode;

@property(nonatomic, strong)NSString *administrativeArea;
@property(nonatomic, strong)NSString *locality;
@property(nonatomic, strong)NSString *subLocality;
@property(nonatomic, strong)NSString *thoroughfare;
@property(nonatomic, strong)NSString *subThoroughfare;
// 地理位置信息
@property(nonatomic, strong)NSString *name;
@property(nonatomic, strong)CLLocation *FinalLocation;

+ (GeocodeShare*)sharedGecode;

-(void)UpdataGecodeWithLoca:(CLLocation *)FinalLocation;
-(void)UpdataGecodeWithPlace:(CLPlacemark *)placeMark AndLocation:(CLLocation *)FinalLocation;
//@property (strong,nonatomic)GeocodeShare * GeocodeShareObject;
//-(id)initWithCoder:(NSCoder *)aDecoder;
//-(void)encodeWithCoder:(NSCoder *)aCoder;
@end

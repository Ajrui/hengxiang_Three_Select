//
//  GeocodeShare.m
//  smart_village
//
//  Created by Jabez on 10/3/15.
//  Copyright (c) 2015 osoons. All rights reserved.
//

#import "GeocodeShare.h"

static GeocodeShare* sharedGecode_;



//反地理坐标
@implementation GeocodeShare

- (id)init
{
    self = [super init];
    
    if(self)
    {
        _isGetLocation = [NSNumber numberWithBool:NO];
        _isGetGecode = [NSNumber numberWithBool:NO];
        
        
        NSData *dataGeo = [[NSUserDefaults standardUserDefaults] objectForKey:@"GeocodeShare"];
        GeocodeShare *user = [NSKeyedUnarchiver unarchiveObjectWithData:dataGeo];
        if (user)
        {
            if (user.FinalLocation) {
                _isGetLocation = [NSNumber numberWithBool:YES];
            }
            
            if (user.locality) {
                _isGetGecode = [NSNumber numberWithBool:YES];
            }
            
            
            _administrativeArea = user.administrativeArea;
            _locality = user.locality;
            _subLocality = user.subLocality;
            _thoroughfare = user.thoroughfare;
            _subThoroughfare = user.subThoroughfare;
            _name = user.name;
            _FinalLocation = user.FinalLocation;
        }
        else{
            _administrativeArea = nil;
            _locality = nil;
            _subLocality = nil;
            _thoroughfare = nil;
            _subThoroughfare = nil;
            _name = nil;
            _FinalLocation = nil;
        }
    }
    
    return self;
}




+ (GeocodeShare*)sharedGecode
{
    if(!sharedGecode_) {
        @synchronized(sharedGecode_)
        {
            if (!sharedGecode_) {
                sharedGecode_ = [[GeocodeShare alloc] init];
            }
            
        }
    }
    
    return sharedGecode_;
}

#pragma mark - main method
-(void)UpdataGecodeWithPlace:(CLPlacemark *)placeMark AndLocation:(CLLocation *)FinalLocation
{
    @synchronized(sharedGecode_)
    {
        _administrativeArea = placeMark.administrativeArea;
        _locality = placeMark.locality;
        _subLocality = placeMark.subLocality;
        _thoroughfare = placeMark.thoroughfare;
        _subThoroughfare = placeMark.subThoroughfare;
        _name = placeMark.name;
        
        _FinalLocation = FinalLocation;
        
        _isGetGecode = [NSNumber numberWithBool:YES];
        _isGetLocation = [NSNumber numberWithBool:YES];
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:sharedGecode_] forKey:@"GeocodeShare"];
    }
}

-(void)UpdataGecodeWithLoca:(CLLocation *)FinalLocation
{
    @synchronized(sharedGecode_)
    {
        _isGetLocation = [NSNumber numberWithBool:YES];
        _FinalLocation = FinalLocation;
        
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:sharedGecode_] forKey:@"GeocodeShare"];
        
    }
    
}


#pragma mark - nsdata coder
-(void)encodeWithCoder:(NSCoder*)encoder
{
    [encoder encodeObject:_isGetGecode   forKey:@"isGetGecode"];
    [encoder encodeObject:_isGetLocation forKey:@"isGetLocation"];
    [encoder encodeObject:_administrativeArea forKey:@"administrativeArea"];
    [encoder encodeObject:_locality forKey:@"locality"];
    [encoder encodeObject:_subThoroughfare  forKey:@"subThoroughfare"];
    [encoder encodeObject:_thoroughfare forKey:@"thoroughfare"];
    [encoder encodeObject:_name forKey:@"name"];
    [encoder encodeObject:_FinalLocation forKey:@"FinalLocation"];
}

-(id)initWithCoder:(NSCoder*)decoder
{
    _isGetLocation = [decoder decodeObjectForKey:@"isGetLocation"];
    _isGetGecode   = [decoder decodeObjectForKey:@"isGetGecode"];
    _administrativeArea = [decoder decodeObjectForKey:@"administrativeArea"];
    _locality = [decoder decodeObjectForKey:@"locality"];
    _subLocality = [decoder decodeObjectForKey:@"subLocality"];
    _thoroughfare = [decoder decodeObjectForKey:@"thoroughfare"];
    _subThoroughfare= [decoder decodeObjectForKey:@"subThoroughfare"];
    _name = [decoder decodeObjectForKey:@"name"];
    _FinalLocation = [decoder decodeObjectForKey:@"FinalLocation"];
    return self;
}



@end

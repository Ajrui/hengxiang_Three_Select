//
//  LoadingView.m
//  GroupDeal
//
//  Created by lewang on 14-7-9.
//  Copyright (c) 2014年 groupdeal. All rights reserved.
//

#import "LoadingView.h"

#define ANIMATION_DURATION 1
#define VIEW_ALPHA 0.6





static LoadingView *_loadingView;




@implementation LoadingView

#pragma life cycle
+(LoadingView *)shareLoadingView
{
    @synchronized(self){  //为了确保多线程情况下，仍然确保实体的唯一性
        
        if (!_loadingView)
        {
            //该方法会调用 allocWithZone
            _loadingView = [[LoadingView alloc] init];
        }
    }
    return  _loadingView;
}


+(id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if (!_loadingView)
        {
            _loadingView = [super allocWithZone:zone]; //确保使用同一块内存地址
            return _loadingView;
        }
    }
    
    return nil;
}


- (id)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.alpha = 0;
        
        spinning_ = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        //[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self addSubview:spinning_];
        
        
        isAnimating_ = NO;
        //设置动画帧
        spinning_.animationImages=[NSArray arrayWithObjects: [UIImage imageNamed:@"loading1"],
                                   [UIImage imageNamed:@"loading2"],
                                   [UIImage imageNamed:@"loading3"],
                                   [UIImage imageNamed:@"loading4"],
                                   [UIImage imageNamed:@"loading5"],
                                   [UIImage imageNamed:@"loading6"],
                                   [UIImage imageNamed:@"loading7"],
                                   [UIImage imageNamed:@"loading8"],
                                   [UIImage imageNamed:@"loading9"],
                                   [UIImage imageNamed:@"loading10"],
                                   [UIImage imageNamed:@"loading11"],
                                   nil ];
        [spinning_ setAnimationRepeatCount:20];
        [spinning_ setAnimationDuration:ANIMATION_DURATION];

    }
    return self;
}


-(void)show:(NSString *)strStatus
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [_lalStatus setText:strStatus];
    [self showInView:window animated:YES];
    
    
}
#pragma  mark - methods
- (void)show {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [self showInView:window animated:YES];
}

- (void)showAnimated:(BOOL)animated {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [self showInView:window animated:animated];
}

- (void)showInView:(UIView *)view animated:(BOOL)animated
{
    CGFloat width = view.frame.size.width;
    CGFloat height = view.frame.size.height;
    self.frame = CGRectMake(0, 0, width, height);
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [view addSubview:self];
    
    if (animated)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:ANIMATION_DURATION];
        
        isAnimating_ = YES;
        [spinning_ startAnimating];
    }
    
    self.alpha = VIEW_ALPHA;
    
    if (animated) {
        [UIView commitAnimations];
    }
}

- (void)dismissAnimated:(BOOL)animated
{
    if (animated) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:ANIMATION_DURATION];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(didFinishLoadingAnimation)];
    }
    
    self.alpha = 0;
    if (!animated) {
        [self removeFromSuperview];
    }
    
    if (animated) {
        [UIView commitAnimations];
    }
    
    [spinning_ stopAnimating];
    isAnimating_ = NO;
}

- (void)dismiss {
    [self dismissAnimated:YES];
}

- (void)didFinishLoadingAnimation {
    [self removeFromSuperview];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat yCenter = (self.frame.size.height -  (80.0/400 * self.frame.size.width)) * (1 - 0.58);
    spinning_.center = CGPointMake(self.frame.size.width / 2, yCenter);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

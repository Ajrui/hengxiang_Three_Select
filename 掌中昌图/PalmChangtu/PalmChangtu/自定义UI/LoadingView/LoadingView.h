//
//  LoadingView.h
//  GroupDeal
//
//  Created by lewang on 14-7-9.
//  Copyright (c) 2014年 groupdeal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView
{
//    UIActivityIndicatorView *_spinning;
    UIImageView *spinning_;
    BOOL        isAnimating_;
    UILabel * _lalStatus;
}

+(LoadingView *)shareLoadingView;
- (void)show:(NSString *)strStatus;

- (void)show;
- (void)showAnimated:(BOOL)animated;
- (void)showInView:(UIView *)view animated:(BOOL)animated;
- (void)dismissAnimated:(BOOL)animated;
- (void)dismiss;

@end

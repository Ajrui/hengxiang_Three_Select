//
//  NetWarningView.h
//  secondUnited
//
//  Created by lewang on 14-1-23.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NetWarningView : UIView
{
    NSTimer *relayteTimer;
    UILabel *_lalWarning;
}

+(NetWarningView *)sharedNetWarningView;
- (void)show;
- (void)dismiss;

@end

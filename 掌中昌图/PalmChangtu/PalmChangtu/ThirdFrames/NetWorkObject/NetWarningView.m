//
//  NetWarningView.m
//  secondUnited
//
//  Created by lewang on 14-1-23.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "NetWarningView.h"


#define ANIMATION_DURATION 0.6
//#define VIEW_ALPHA 0.6
static NetWarningView *netWarningView = nil;

@implementation NetWarningView


#pragma mark static method
+(NetWarningView *)sharedNetWarningView
{
    @synchronized(self){  //为了确保多线程情况下，仍然确保实体的唯一性
        
        if (!netWarningView) {
            
            //该方法会调用 allocWithZone
            UIWindow *windows = [[[UIApplication sharedApplication] delegate] window];
            netWarningView = [[NetWarningView alloc] initWithFrame:CGRectMake(0.0f, windows.bounds.size.height/4, windows.bounds.size.width, 50.0)];
        }
    }
    return  netWarningView;
}


+(id)allocWithZone:(NSZone *)zone{
    
    @synchronized(self){
        
        if (!netWarningView) {
            netWarningView = [super allocWithZone:zone]; //确保使用同一块内存地址
            return netWarningView;
            
        }
        
    }
    
    return nil;
    
}


#pragma mark - life cycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
        
        _lalWarning = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 300.0f,40.0f)];
        [_lalWarning setTextAlignment:NSTextAlignmentCenter];
        [_lalWarning setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.6f]];
        [_lalWarning setTextColor:[UIColor whiteColor]];
        [_lalWarning setShadowColor:[UIColor blackColor]];
        [_lalWarning setShadowOffset:CGSizeMake(0.5, 0.5)];
        
        [_lalWarning setText:@"您的网络好像有问题哦……"];
        _lalWarning.layer.masksToBounds = YES;
        _lalWarning.layer.cornerRadius = 10;//设置圆角
        
        
        [self addSubview:_lalWarning];
        NSLog(@"add warningView");
        
    }
    return self;
}


#pragma mark - main method
- (void)show {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [self showInView:window animated:YES];
}

- (void)dismiss {
    [self dismissAnimated:YES];
}

#pragma mark - private method


- (void)showAnimated:(BOOL)animated {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [self showInView:window animated:animated];
}


//按定时隐现
- (void)showInView:(UIView *)view animated:(BOOL)animated
{
    CGFloat width = view.frame.size.width;
    CGFloat height = view.frame.size.height;
    self.frame = CGRectMake(0, height*0.8, width, 50.0);
    _lalWarning.frame = CGRectMake(10, 0, width - 20, 40.0);
    
    
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [view addSubview:self];
    
    if (animated)
    {
        self.alpha = 0.0;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:ANIMATION_DURATION];
    }
    self.alpha = 1.0;
    
    if (animated) {
        [UIView commitAnimations];
    }
    
    //启动定时
    if (relayteTimer ) {
        [relayteTimer invalidate];
        relayteTimer = nil;
    }
    relayteTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(dismiss) userInfo:nil repeats:NO];
    
}

- (void)dismissAnimated:(BOOL)animated
{
    if (animated) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:ANIMATION_DURATION];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(didFinishLoadingAnimation)];
    }
    
    self.alpha = 0;
    if (!animated) {
        [self removeFromSuperview];
    }
    
    if (animated) {
        [UIView commitAnimations];
    }
}



- (void)didFinishLoadingAnimation {
    [self removeFromSuperview];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 10;//设置圆角
}


@end

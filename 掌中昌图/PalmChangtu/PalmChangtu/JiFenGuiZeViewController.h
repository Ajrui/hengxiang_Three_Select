//
//  JiFenGuiZeViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/9/29.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface JiFenGuiZeViewController :  UIViewController<UIGestureRecognizerDelegate>
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;

@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;

@end

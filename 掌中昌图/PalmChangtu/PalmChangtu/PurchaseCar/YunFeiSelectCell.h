//
//  YunFeiSelectCell.h
//  PalmChangtu
//
//  Created by osoons on 15/7/21.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YunFeiSelectCell : UITableViewCell
/**
 * Varchar 1是区域 2是菜单类型
 *
 *
 ***/
@property (strong, nonatomic) IBOutlet UILabel *labTyepe;
/**
 * 运费价格
 ***/
@property (strong, nonatomic) IBOutlet UILabel *labPrice;


@end

//
//  YunFeiSelectViewController.h
//  PalmChangtu
//
//  Created by osoons on 15/7/21.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"

@interface YunFeiSelectViewController : UIViewController<UITextViewDelegate,UITableViewDataSource,UITableViewDelegate>

//@property(nonatomic,assign) id <getBuyerDeliveryInfoValueDelegate> delegate;

@property (strong,nonatomic) IBOutlet UITableView * tableView;
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;

@property (strong, nonatomic) IBOutlet UITextView *textVeiwContentr;
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;

@end

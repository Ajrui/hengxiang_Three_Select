//
//  FreightTicketViewController.m
//  PalmChangtu
//
//  Created by shaorui on 15/7/16.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "FreightTicketViewController.h"
#import "CommentViewController.h"
#import "commentsCell.h"
#import "BaseUrlConfig.h"
#import "BannerCell.h"
#import "ShareUserModel.h"
#import "MainTabbarController.h"
#import "JSONKit.h"
#import "Common.h"
#import "LoadingView.h"
#import "NetWarningView.h"
#import "AFNetworking.h"
#import "BaseUrlConfig.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "all_commodity_TypeModel.h"
#import "localJSONModel.h"
#import "MTLJSONAdapter.h"
#import "GetRemandModle.h"
#import "ShangPinListModel.h"
#import "PurchaseCarModel.h"
#import "ShippingAddressViewController.h"
@interface FreightTicketViewController ()

@end

@implementation FreightTicketViewController

-(void)viewDidAppear:(BOOL)animated
{
    [self getFreightTicketActionRequest];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_v_Top.lalTitel setText:@"您当前优惠卷"];
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    [_v_Top.btnGoback removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_v_Top.btnGoback addTarget:self action:@selector(btnGobackAction:) forControlEvents:UIControlEventTouchUpInside];
    

    // Do any additional setup after loading the view from its nib.
}

-(void)btnGobackAction:(UIButton *)action
{
    
    
    [self dismissViewControllerAnimated:YES completion:^{

        [self.delegate getYouHuiQuanShuLiang:self.strNum];
    }];
}


#pragma mark - network request getFreightTicketAction

-(void)getFreightTicketActionRequest
{
    [[LoadingView shareLoadingView] show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,getFreightTicketAction];
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];
    
    
    
    NSDictionary *params = @{
                             
                             
                             @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId],
                             @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.loginName],
                             @"userId":[NSString stringWithFormat:@"%@",shareUser.user.user_id]
                             
                             
                             };
    
    
    
    
    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
     
     
         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     
     NSDictionary * dicRespon = responseObject;
     [self getUserScoreActionviewJiFenRequestSuccessWith:dicRespon];
     
     
     
     [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
     [[LoadingView shareLoadingView] dismissAnimated:YES];
     [[NetWarningView sharedNetWarningView] show];  //警告网络异常
                                                    //              NSData *doubi = operation.error    ;
                                                    //                  //     JSON格式的NSString转换成NSMutableDictionary
                                                    //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
                                                    //
                                                    //              NSError *err = nil;
                                                    //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSLog(@"Error: %@", error);
     NSLog(@"请求出错了: - %@ \n%@",
           [error localizedDescription],
           [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];
    
}


-(void)getUserScoreActionviewJiFenRequestSuccessWith:(NSDictionary *)dicRespon
{
    
    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
        {
        
        
        
        NSInteger freightTicketNo = [[dicRespon  objectForKey:@"freightTicketNo"] integerValue];

        
        if (freightTicketNo<0) {
            
            freightTicketNo = 0;
              self.labFreightNum.text = [NSString stringWithFormat:@"您目前的运费卷数量是 %@ ",[dicRespon objectForKey:@"freightTicketNo"]];
        }
        else
            {
              self.labFreightNum.text = [NSString stringWithFormat:@"您目前的运费卷数量是 %@ ",[dicRespon objectForKey:@"freightTicketNo"]];
            }
      

            self.strNum  = [NSString stringWithFormat:@"%@",[NSNumber numberWithInteger:freightTicketNo]];
        
        
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        
        }
    else
        {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

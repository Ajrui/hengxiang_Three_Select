//
//  PurchaseCarViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/14.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
#import "MyCustomCell.h"
@interface PurchaseCarViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MyCustomCellDelegate>

@property (strong, nonatomic) IBOutlet UITableView *MyTableView;

/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;
/**
 * 去结算
*/
@property (strong, nonatomic) IBOutlet UIButton *settlementBtn;
/**
 *  总计结算去结算
 */
@property (strong, nonatomic) IBOutlet UILabel *allPriceLab;
/**
 *  全不选选择
 */
@property(strong,nonatomic)IBOutlet UIButton *allSelectBtn;
/**
 *  删除
 */
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;



@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;


@property (strong, nonatomic) IBOutlet UIButton *btnSettlement;
@property (strong, nonatomic) NSMutableArray * arrModelData;
@property (strong, nonatomic) IBOutlet UILabel *labTotalNum;

@end

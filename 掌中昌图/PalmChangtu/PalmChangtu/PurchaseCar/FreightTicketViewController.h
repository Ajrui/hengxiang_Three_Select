//
//  FreightTicketViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/7/16.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
#import "SettlementViewController.h"
@interface FreightTicketViewController : UIViewController
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
@property(nonatomic,assign) id <getBuyerDeliveryInfoValueDelegate> delegate;

@property (strong, nonatomic) IBOutlet UILabel *labFreightNum;
@property (strong, nonatomic) NSString  * strNum;
@end

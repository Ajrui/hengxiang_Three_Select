//
//  MyCustomCell.h
//  ShoppingList_Demo
//
//  Created by 李金华 on 15/4/20.
//  Copyright (c) 2015年 LJH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseUrlConfig.h"
#import "PurchaseCarModel.h"
#import "UIButton+WebCache.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+AFNetworking.h"

//添加代理，用于按钮加减的实现
@protocol MyCustomCellDelegate <NSObject>

-(void)btnClick:(UITableViewCell *)cell andFlag:(int)flag;


@end



@interface MyCustomCell : UITableViewCell

@property(strong,nonatomic) IBOutlet UIImageView *goodsImgV;//商品图片
@property(strong,nonatomic)IBOutlet UILabel *goodsTitleLab;//商品标题
@property(strong,nonatomic)IBOutlet UILabel *priceTitleLab;//价格标签
@property(strong,nonatomic)IBOutlet UILabel *priceLab;//具体价格
@property(strong,nonatomic)IBOutlet UILabel *goodsNumLab;//购买数量标签
@property(strong,nonatomic)IBOutlet UILabel *numCountLab;//购买商品的数量
@property(strong,nonatomic)IBOutlet UIButton *addBtn;//添加商品数量
@property(strong,nonatomic)IBOutlet UIButton *deleteBtn;//删除商品数量
@property(strong,nonatomic)IBOutlet UIButton *isSelectBtn;//是否选中按钮
@property(strong,nonatomic)IBOutlet UIImageView *isSelectImg;//是否选中图片

@property(assign,nonatomic)BOOL selectState;//选中状态

@property(assign,nonatomic)id<MyCustomCellDelegate>delegate;

-(void)addBtnAction:(UIButton *)sender;

-(void)deleteBtnAction:(UIButton *)sender;


//赋值
-(void)addTheValue:(PurchaseCarModel *)goodsModel;

@end

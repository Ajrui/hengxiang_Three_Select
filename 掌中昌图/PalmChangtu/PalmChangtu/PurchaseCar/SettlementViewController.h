//
//  SettlementViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/18.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
#import "ShangPinListModel.h"
#import "getBuyerDeliveryInfoModel.h"


@protocol getBuyerDeliveryInfoValueDelegate <NSObject>

@optional

-(void)getBuyerDeliveryInfo:(getBuyerDeliveryInfoModel *)valueModel andTypeIDStr:(NSString *)typestr;
-(void)getYouHuiQuanShuLiang:(NSString *)YouHuiQuanNum;


@end

@interface SettlementViewController : UIViewController<UITextViewDelegate,UITableViewDataSource,UITableViewDelegate,getBuyerDeliveryInfoValueDelegate>



//@property(nonatomic,assign) id <getBuyerDeliveryInfoValueDelegate> delegate;
@property (strong,nonatomic) getBuyerDeliveryInfoModel * moeldValues;

@property (strong,nonatomic) IBOutlet UITableView * tableView;
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;

@property (strong, nonatomic) IBOutlet UITextView *textVeiwContentr;
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
/**
 * 留言
 */
@property (strong, nonatomic) IBOutlet UITextView *leaveTextView;
/**
 * 总额
 */
@property (strong, nonatomic) IBOutlet UILabel *labTatal;
/**
 * 运费
 */
@property (strong, nonatomic) IBOutlet UILabel *labYunFei;
/**
 * 单价价格
 */
@property (strong, nonatomic) IBOutlet UILabel *labPrice;
/**
 * 确认
 */
@property (strong, nonatomic) IBOutlet UIButton *btnSure;
/**
 * 电话
 */
@property (strong, nonatomic) IBOutlet UILabel *labTell;
/**
 * 收货人
 */
@property (strong, nonatomic) IBOutlet UILabel *labName;
/**
 * 收货地址
 */
@property (strong, nonatomic) IBOutlet UILabel *labAddress;

@property (strong, nonatomic) IBOutlet UIView *viewAddress;
@property (strong, nonatomic) NSString *stringAddress;


//settleAccounts_immediateAction
/**
 * 表示一键购买
 */
@property (strong, nonatomic) NSString *strYiJianGouMai;

@property (strong, nonatomic) IBOutlet UIView *viewTouch1;

@property (strong, nonatomic) IBOutlet UIView *viewTouch2;
@property (strong, nonatomic) IBOutlet UIView *viewTouch3;

/**
 * 商品ID
 */
@property (strong, nonatomic) NSString *shangPingId;
/**
 * 商品freightTicketNo使用优惠卷数量
 */
@property (strong, nonatomic) NSString *freightTicketNo;

/**
 *
 * 运费
 */
@property (strong, nonatomic) NSString *freight;
/**
 *  总金额
 *
 **/
@property (strong, nonatomic) NSString *allTotalprice;


@property (strong, nonatomic) IBOutlet UIView *viewFreightTicket;

/***
 * 选取运费
 *
 **/
@property (strong, nonatomic) IBOutlet UIButton *btnYunFeiSelect;


@property (strong,nonatomic) NSMutableArray * arrYunFeiData;


@property (strong,nonatomic) NSString * strYunFeiPrice;

@property (strong, nonatomic) IBOutlet UILabel *labYHNum;




@end

//
//  YunFeiSelectViewController.m
//  PalmChangtu
//
//  Created by osoons on 15/7/21.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "YunFeiSelectViewController.h"

#import "YunFeiSelectCell.h"
@interface YunFeiSelectViewController ()

@end

@implementation YunFeiSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];




    [_v_Top.lalTitel setText:@"选择运费"];
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    UINib *nibCell = [UINib nibWithNibName:NSStringFromClass([YunFeiSelectCell class]) bundle:nil];
    [self.tableView registerNib:nibCell forCellReuseIdentifier:[NSString stringWithFormat:@"%@", [YunFeiSelectCell class]]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;

    // Do any additional setup after loading the view from its nib.
}


#pragma mark - standard tableview datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{


    return 60;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{



    if([_arrData count])
    {
        return [_arrData count];
    }
    else

        return 0;



}


#pragma mark - Standard TableView delegates
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString * identifier = @"YunFeiSelectCell";
    YunFeiSelectCell * Cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    Cell.selectionStyle  = UITableViewCellSelectionStyleNone;



        if ([_arrData count]!=0) {


            NSDictionary  * dicModel  = [_arrData objectAtIndex:indexPath.row];


            Cell.labPrice.text  = [NSString
                                  stringWithFormat:@"%@",[dicModel objectForKey:@"price"]];
            Cell.labTyepe.text = [NSString
                                  stringWithFormat:@"%@",[dicModel objectForKey:@"price"]];
               }

    return Cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{


    //    InDeliveryTabCell * cell =  (InDeliveryTabCell *)[tableView cellForRowAtIndexPath:indexPath];
    //    cell.contentView.backgroundColor = [UIColor clearColor];
    //
    //
    //    ProdectDetailViewController * detail  = [[ProdectDetailViewController alloc] initWithNibName:@"ProdectDetailViewController" bundle:nil  ];
    //
    //    [self.navigationController pushViewController:detail animated:YES ];



}
- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated
{

    //    InDeliveryTabCell * cell =  (InDeliveryTabCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    //    cell.contentView.backgroundColor = [UIColor clearColor];



}
-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{


    return YES;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

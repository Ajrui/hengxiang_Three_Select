//
//  BaseUrlConfig.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/14.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#ifndef PalmChangtu_BaseUrlConfig_h
#define PalmChangtu_BaseUrlConfig_h


#define HostServer @"http://182.92.225.85:8080/palmct/http/"
    //服务器目录，取用户上传的图片，下载用户上传的图片 也用这个地址
#define HostHtml   @"http://182.92.225.85:8080/palmct/http/"//Url请求目录
#define HostImage  @"http://182.92.225.85:8080/palmct/app_upload/commodity/" //商品视频图片目录
#define HostServerGetArea @"http://182.92.225.85:8080/palmct/"



#define hostUserImage @"http://182.92.225.85:8080/palmct/app_upload/user/"
//账号注册 协议
#define statementAaction @"statement.action"
//账号注册
#define register_commit  @"register_commit.action"
//3.账号登陆
#define loginAction @"login.action"
//意见反馈
#define feedbackAction @"feedback.action"
//我的留言
#define my_messageAction @"my_message.action"
//联系我们
#define contact_usAction @"contact_us.action"
//获取商品种类list
#define all_commodity_typeAction @"all_commodity_type.action"
//.售后申诉
#define buyer_complaintAaction @"buyer_complaint.action"

//.添加买家收货信息
#define insert_BuyerDeliveryInfoAction @"insert_BuyerDeliveryInfo.action"
//获取买家收货信息
#define getBuyerDeliveryInfoAction @"getBuyerDeliveryInfo.action"
//删除买家收货信息
#define delete_BuyerDeliveryInfoAction @"delete_BuyerDeliveryInfo.action"
//修改买家收货信息
#define updateBuyerDeliveryInfoAction @"updateBuyerDeliveryInfo.action"
//设置买家默认收货信息
#define setup_DefalutBuyerDeliveryInfoAction @"setup_DefalutBuyerDeliveryInfo.action"
//48 获取运费

#define webGetFreightAction @"web/getFreight.action"
//短信验证接口
#define CodeGetCodeAction @"Code/getCode.action"


//45 一键购买

#define settleAccounts_immediateAction @"settleAccounts_immediate.action"

//getFreightTicket.action
//46 获取运费卷
#define getFreightTicketAction @"getFreightTicket.action"
// . 修改买家是否推送
#define updateBuyerPushAction @"updateBuyerPush.action"

//  推荐商品
#define getRecommendCommoditysAction @"getRecommendCommoditys.action"

//getHotCommoditys.action  热门商品
#define getHotCommoditysAction @"getHotCommoditys.action"
//22. 获取商品详细

#define getCommodityDetailAction @"getCommodityDetail.action"
//28.加入购物车
#define addShoppingCartAction @"addShoppingCart.action"
//30.获取购物车
//getShoppingCart.action

#define getShoppingCartAction @"getShoppingCart.action"
//}29. 修改购物车
#define updateShoppingCartAction @"updateShoppingCart.action"

//removeCommodity.action 删除商品
#define removeCommodityAction @"removeCommodity.action"

//44.获取商家是否已经提交信息

#define getSellerIsAuditAction @"getSellerIsAudit.action"


//47 获取区域
//web/getArea.action

#define webGetAreaAction @"web/getArea.action"


//23.评价商品
//URLConection：http://localhost:8080/palmct/http/

//
//evaluation_commodity.action
#define  evaluation_commodityAction @"evaluation_commodity.action"

//
//getOrders.action 已购商品
#define  getOrdersAction @"getOrders.action"

//31. 结算
#define settleAccountsAction @"settleAccounts.action"
//32.发布商品
#define publish_commodityAction @"publish_commodity.action"
// save_seller.action
//33. 保存商家信息

#define save_sellerAction @"save_seller.action"
// 42.修改用户名

#define updateUserNameAction @"updateUserName.action"
//

// 商家基本信息

#define getSellerAction @"getSeller.action"
//16.更换手机号码

#define modify_phoneAction @"modify_phone.action"

//21.获取商品列表
#define getCommoditysAction @"getCommoditys.action"

// . 上传头像
#define analysis_imgAction @"analysis_img.action"

//37.编辑商品
#define update_commodityAction @"update_commodity.action"
// 32. 发布商品

#define publish_commodityAction @"publish_commodity.action"



//24. 获取商品品牌

#define commodity_breandsAction @"commodity_breands.action"

//39.获取我的留言
#define getMyMessageListAction @"getMyMessageList.action"
//40.获取我的积分
#define getUserScoreAction @"getUserScore.action"

//41.首页搜索
#define getCommoditys_homeAction @"getCommoditys_home.action"
//42.修改用户名
#define updateUserNameAction @"updateUserName.action"
//35. 获取商家发布商品

#define get_seller_commoditysAction @"get_seller_commoditys.action"

//判断字符串是否为空
#define strIsEmpty(str) (str == nil ||[str isEqualToString:@"<null>"]||[str isEqualToString:@"(null)"]|| [str isKindOfClass:[NSNull class]] || [str length]<1 ? YES : NO )
//
#define modify_passwordAction @"modify_password.action"

#define keyName @"name"
#define KeyImageName @"ImageName"

    //-------------------------------------------键
#define Key_User_Identity           @"user_identity"                //用户登录唯一标识

#define Key_User_AutoLogin          @"key_user_autoLogin"           //自动登录
#define Key_User_RememberPWD        @"key_user_rememberpwd"         //自动基准密码
#define Key_User_Id                 @"user_id"                      //用户Id
#define Key_User_Name               @"key_user_name"                //用户名
#define Key_User_Password           @"key_user_password"            //用户密码
#define Key_User_type               @"key_user_type"                //用户类型
#define Key_City_Id                 @"key_city_id"                  //城市Id
#define Key_City_Name               @"key_city_name"                //城市名
#define User_Collect_Info           @"my_collect_info"              //用户收藏

#define winSize [[UIScreen mainScreen] bounds].size                 //屏幕大小
                                                                    //设备屏幕大小
#define __MainScreenFrame   [[UIScreen mainScreen] bounds]
    //设备屏幕宽
#define __MainScreen_Width  __MainScreenFrame.size.width
    //设备屏幕高 20,表示状态栏高度.如3.5inch 的高,得到的__MainScreenFrame.size.height是480,而去掉电量那条状态栏,我们真正用到的是460;
#define __MainScreen_Height __MainScreenFrame.size.height

    //---------UIAlertView文本
#define AlertView_Title_Text        @"提示"                         //提示标题文本
#define AlertView_CancelButon_Text  @"取消"                         //提示取消按钮文本
#define Loading @"正在加载"
#define PullDownoRefresh @"下拉加载更多"
#define ReleaseRefresh @"松手即可刷新"
#define TemporarilyNoData @"暂无数据"
#define AlertView_Title_Text        @"提示"                         //提示标题文本
#define AlertView_CancelButon_Text  @"取消"                         //提示取消按钮文本
#define headerPullToRefreshTextkey  @"下拉可以刷新了"
#define headerReleaseToRefreshTextkey @"松开马上刷新了"
#define headerRefreshingTextKey @"正在刷新中..."

#define footerPullToRefreshTextKey  @"上拉可以加载更多数据了"
#define footerReleaseToRefreshTextKey @"松开立即加载更多数据了"

#define footerRefreshingTextKey @"正在加载中..."

#define TITLE_HEIGHT 64
#define winSize [[UIScreen mainScreen] bounds].size
#define subject_color  [UIColor colorWithRed:65/255.0 green:185/255.0 blue:249/255.0 alpha:1.0]
#define Color(r,g,b,a) [UIColor colorWithRed:(r/(float)255) green:(g/(float)255) blue:(b/(float)255) alpha:a]



#endif

//
//  LoginViewController.h
//  E_Supermarket_Slient
//
//  Created by osoons on 15/6/11.
//  Copyright (c) 2015年 E_Supermarket_Slient. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ForgotPasswordViewController.h"
#import "UITopBanner.h"
@interface LoginViewController : UIViewController<UITextFieldDelegate>
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;

/** 账号输入框 ***/
@property (weak, nonatomic) IBOutlet UITextField *textPhone;

/** 密码输入框 ***/
@property (weak, nonatomic) IBOutlet UITextField *textPass;
/** 注册按钮 ***/
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;
/** 登录按钮 ***/
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
/**
 * 1买家 2商家
 **/

@property (weak, nonatomic) NSString * string;


@end

//
//  NotLoggedInViewController.m
//  PalmChangtu
//
//  Created by osoons on 15/6/23.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "NotLoggedInViewController.h"
#import "MeranchRegisterViewController.h"
#import "UserRegisterViewController.h"
#import "LoginViewController.h"
@interface NotLoggedInViewController ()

@end

@implementation NotLoggedInViewController
-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_v_Top.lalTitel setText:@"登录或注册"];
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    
    [_v_Top.btnGoback removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_v_Top.btnGoback addTarget:self action:@selector(btnBack:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [_btnLogin removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnLogin addTarget:self action:@selector(btnLogin:) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnRegister removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnRegister addTarget:self action:@selector(btnRegister:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark - btnRegister

-(void)btnLogin:(UIButton *)Login
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    
    LoginViewController * Loginview = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];

    Loginview.string = self.string;
    [self presentViewController:Loginview animated:YES completion:^{
        
    }];
    
    
    
    
}
#pragma mark - btnRegister

-(void)btnRegister:(UIButton *)Register
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];

   UserRegisterViewController  * RegisterView =[[UserRegisterViewController alloc] initWithNibName:@"UserRegisterViewController" bundle:nil];
    [self presentViewController:RegisterView animated:YES completion:^{
        
            }];
//    MeranchRegisterViewController * registere = [[MeranchRegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
//    
//    [self presentViewController:registere animated:YES completion:^{
//        
//    }];
    
    
    
    
}

-(void)btnBack:(UIButton *)btnback
{
    
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

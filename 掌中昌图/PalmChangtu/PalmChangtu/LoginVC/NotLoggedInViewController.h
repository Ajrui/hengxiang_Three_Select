//
//  NotLoggedInViewController.h
//  PalmChangtu
//
//  Created by osoons on 15/6/23.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface NotLoggedInViewController : UIViewController
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;

/** 注册按钮 ***/
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
/** 登录按钮 ***/
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
/**
 * 1买家 2商家
 **/

@property (weak, nonatomic) NSString * string;

@end

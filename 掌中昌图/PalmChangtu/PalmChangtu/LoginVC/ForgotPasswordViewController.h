//
//  ForgotPasswordViewController.h
//  PalmChangtu
//
//  Created by osoons on 15/6/23.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface ForgotPasswordViewController : UIViewController
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;

@property (weak, nonatomic) IBOutlet UIButton *btnVerification;

/**
 * 电话号码
 **/
@property (weak, nonatomic) IBOutlet UITextField *textPhoneNum;
/**
 * 输入密码
 **/
@property (weak, nonatomic) IBOutlet UITextField *textPassWord;
/**
 * 再次确认密码
 **/
@property (weak, nonatomic) IBOutlet UITextField *textAlingPasss;

/**
 * 获取验证码
 **/
@property (weak, nonatomic) IBOutlet UITextField *textVerificationCode;
//得到的验证码
@property (strong, nonatomic) NSString * strCode;

- (IBAction)btnSureAction:(id)sender;


@end

//
//  TestCollectCell.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/26.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestCollectCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnImage;
@property (strong, nonatomic) IBOutlet UILabel *labName;

@end

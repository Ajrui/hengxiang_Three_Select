//
//  TestCollectionViewController.m
//  PalmChangtu
//
//  Created by shaorui on 15/6/26.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "TestCollectionViewController.h"

#import "TestCollectCell.h"
@interface TestCollectionViewController ()

@end

@implementation TestCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_v_Top.lalTitel setText:@"测试"];
    [_v_Top.lalTitel setHidden:NO];
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;

    [self SetBannerCollectionView];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - 布局collectionview

-(void)SetBannerCollectionView
{
    
        //user nib
    CGFloat widthFlow  = self.RollingScrollView.frame.size.width;
    CGFloat heightFlow = self.RollingScrollView.frame.size.height;
    
    UICollectionViewFlowLayout *flowOut = [[UICollectionViewFlowLayout alloc] init];
    [flowOut setItemSize:CGSizeMake(widthFlow, heightFlow)];
    
    [flowOut setMinimumLineSpacing:0.0];
    [flowOut setMinimumInteritemSpacing:0.0];
    [flowOut setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [self.RollingScrollView setCollectionViewLayout:flowOut];
    
        //user nib
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([TestCollectCell class]) bundle:nil];
    [self.RollingScrollView registerNib:nib forCellWithReuseIdentifier:[NSString stringWithFormat:@"%@", [TestCollectCell class]]];
    
        //user nib
    [self.RollingScrollView setDelegate:self];
    [self.RollingScrollView setDataSource:self];
    [self.RollingScrollView setShowsVerticalScrollIndicator:NO];
    [self.RollingScrollView setAlwaysBounceVertical:NO];
    [self.RollingScrollView setAlwaysBounceHorizontal:YES];
    [self.RollingScrollView setPagingEnabled:YES];
    
    _arrayBannerData = [[NSMutableArray alloc] init];
    
        //    TableView
        //user nib

    
}



#pragma mark -UICollectionView

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
        return 1;
        
    
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 8;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
        //
    static NSString *cellIdent = @"TestCollectCell";
    TestCollectCell * cell;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdent forIndexPath:indexPath];
    [cell.btnImage removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    [cell.btnImage addTarget:self action:@selector(btnImageActin:) forControlEvents:UIControlEventTouchUpInside];
    
    
       return cell;
    
    
    
}
#pragma mark - UICollectionViewDelegate


-(void)btnImageActin:(UIButton *)sender
{


    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (collectionView==self.RollingScrollView) {
        
    }
    
        //    TestCollectCell * cell = (TestCollectCell*)[collectionView cellForItemAtIndexPath:indexPath];
    
}

#pragma mark --UICollectionViewDelegateFlowLayout
    //定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(115, 100);
}
    //定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

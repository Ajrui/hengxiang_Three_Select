//
//  TestCollectionViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/26.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface TestCollectionViewController : UIViewController<UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

/**
 * 顶上的UITopView
 */
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;



/**
 * 菜单选择控件
 * MenuCollectionView
 */

@property (strong, nonatomic) IBOutlet UICollectionView *RollingScrollView;


/**
 *  首页广告
 */
@property(strong,nonatomic) NSMutableArray *arrayBannerData;



@end

//
//  MerchantBaseInfoViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/17.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BouthLineView.h"
#import "UITopBanner.h"
@interface MerchantBaseInfoViewController : UIViewController<UIGestureRecognizerDelegate>
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
/**
 * 基本信息
 *
 */
@property (strong, nonatomic) IBOutlet BouthLineView *viewDescription;

    /**
     * 更多商品描述
     */
@property (strong, nonatomic) IBOutlet BouthLineView *textViewDescription;

/**
 *
 **/
@property(strong,nonatomic) NSMutableArray * arrData;


@property (strong, nonatomic) IBOutlet UITextField *labAcount;
@property (strong, nonatomic) IBOutlet UITextField *labIDCard;

@property (strong, nonatomic) IBOutlet UITextField *labName;

@property (strong, nonatomic) IBOutlet UITextView *labTextDesc;
@property (strong, nonatomic) IBOutlet UITextField *labNum;
@property (strong, nonatomic) IBOutlet UITextField *labAddress;

@end

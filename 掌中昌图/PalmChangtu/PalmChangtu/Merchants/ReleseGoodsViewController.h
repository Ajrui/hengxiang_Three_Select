//
//  ReleseGoodsViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/17.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QBImagePickerController.h"
#import "RelesedGoodsModel.h"
#import "UITopBanner.h"

@interface ReleseGoodsViewController : UIViewController<QBImagePickerControllerDelegate,UITextViewDelegate,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    
    
    
    
}
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;

@property (strong, nonatomic) IBOutlet UIButton *btnTypeSelect;
@property (strong, nonatomic) IBOutlet UIView *viewTypeHidden;
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
/*
 * UIcollection 控件
 */

@property (strong,nonatomic) IBOutlet UICollectionView * CollectionView;
/*产品数据**/

@property (strong,nonatomic) NSMutableArray * arrProdect;
/*产品数据**/

@property (strong,nonatomic) NSDictionary * dicProdect;



@property (strong, nonatomic) IBOutlet UIButton *btnTwo;
@property (strong, nonatomic) IBOutlet UIButton *btnSecond;
@property (strong, nonatomic) IBOutlet UIButton *btnSeondOnw;

@property (strong, nonatomic) IBOutlet UIButton *btnThird;


@property (strong, nonatomic) IBOutlet UIButton *btnOne;

@property (strong, nonatomic) IBOutlet UIButton *btnFour;

@property (strong, nonatomic) IBOutlet UIButton *btnThree;

@property (strong, nonatomic) IBOutlet UIButton *btnFive;


@property (strong, nonatomic) IBOutlet UIButton *btnSix;

@property (strong, nonatomic) IBOutlet UIButton *btnSeven;

@property (strong, nonatomic) IBOutlet UIButton *btnOther;


@property (strong, nonatomic) IBOutlet UIButton *btnCancle;
@property (assign ,nonatomic) NSInteger btnTypeId;

@property (strong, nonatomic) IBOutlet UITextView *textView;



@property (strong, nonatomic) IBOutlet UITextField *textName;
@property (strong, nonatomic) IBOutlet UITextField *textPrice;
@property (strong, nonatomic) IBOutlet UITextField *textAfterSalesAddress;

@property (strong, nonatomic) IBOutlet UIButton *btnRelese;


@property (strong, nonatomic)RelesedGoodsModel * modelTemp;


@property (strong, nonatomic)NSString  * strEditTemp;

@end

//
//  RelesedGoodsCollCell.h
//  PalmChangtu
//
//  Created by osoons on 15/7/17.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RelesedGoodsCollCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageVie;

@property (strong, nonatomic) IBOutlet UILabel *labName;
@property (strong, nonatomic) IBOutlet UILabel *labPrice;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;

@end

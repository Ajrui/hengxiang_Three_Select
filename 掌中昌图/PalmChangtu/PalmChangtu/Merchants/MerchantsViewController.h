//
//  MerchantsViewController.h
//  PalmChangtu
//
//  Created by osoons on 15/6/17.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RectangularWireLineView.h"
#import "QBImagePickerController.h"
#import "UITopBanner.h"
#import "BouthLineView.h"
@interface MerchantsViewController : UIViewController<QBImagePickerControllerDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;

@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
@property (strong, nonatomic) IBOutlet UIImageView *imageHead;
@property (strong, nonatomic) IBOutlet UIView *Viewline;

@property (strong, nonatomic) UIImage * image;


/**
 *
 * 申请注册
 */
@property (strong, nonatomic) IBOutlet UIButton *btnAplayRegister;

@property (strong, nonatomic) IBOutlet UILabel *labNameText;

/**
 * 账号登录
 *
 */
@property (strong, nonatomic) IBOutlet UIButton *btnLoginAction;

/**
 * 基本信息
 *
 */
@property (strong, nonatomic) IBOutlet BouthLineView *viewBaseInfo;
/**
 * 发布商品
 *
 */
@property (strong, nonatomic) IBOutlet BouthLineView *viewReleseGoods;

/**
 * 已发布商品
 *
 */
@property (strong, nonatomic) IBOutlet BouthLineView *viewRelesedGooss;


/**
 * 联系我们
 *
 */
@property (strong, nonatomic) IBOutlet BouthLineView *viewConnect;

/**
 * 相关协议
 *
 */
@property (strong, nonatomic) IBOutlet BouthLineView *viewAboutAgreement;

/**
 *
 *
 */
@property (strong, nonatomic) IBOutlet UIImageView *imageHeadChange;

/**
 *
 *
 */
/**
 *
 *
 */
/**
 *
 *
 */
/**
 *
 *
 */
/**
 *
 *
 */
/**
 *
 *
 */
/**
 *
 *
 */

@end

//
//  ProdectAddCell.h
//  E_Supermarket_ Merchants
//
//  Created by osoons on 15/6/9.
//  Copyright (c) 2015年 E_Supermarket_ Merchants. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProdectAddCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageProdect;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

@end

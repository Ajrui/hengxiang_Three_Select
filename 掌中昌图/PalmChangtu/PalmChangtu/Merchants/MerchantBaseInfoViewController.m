//
//  MerchantBaseInfoViewController.m
//  PalmChangtu
//
//  Created by shaorui on 15/6/17.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "MerchantBaseInfoViewController.h"
#import "MoreDescribeViewController.h"
#import "ShareUserModel.h"
#import "MainTabbarController.h"
#import "JSONKit.h"
#import "Common.h"
#import "LoadingView.h"
#import "NetWarningView.h"
#import "AFNetworking.h"
#import "BaseUrlConfig.h"

#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"

#import "all_commodity_TypeModel.h"
#import "localJSONModel.h"
#import "MTLJSONAdapter.h"

#import "GetRemandModle.h"
@interface MerchantBaseInfoViewController ()
{

    BOOL isRequest;
}
@end

@implementation MerchantBaseInfoViewController
-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    if (isRequest) {
        isRequest = NO;

        [self getSellerRequest];

    }


}

- (void)viewDidLoad {
    [super viewDidLoad];
    isRequest = YES;

    [_v_Top.lalTitel setText:@"基本信息"];
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    UITapGestureRecognizer *singleTouchGoods = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleTouchTap:)];
    singleTouchGoods.delegate = self;
    singleTouchGoods.cancelsTouchesInView = NO;
    singleTouchGoods.delaysTouchesEnded = NO;
//    [singleTouchGoods requireGestureRecognizerToFail:singleTouchScale];
    [self.viewDescription addGestureRecognizer:singleTouchGoods];

    _arrData = [[NSMutableArray alloc] init];



    // Do any additional setup after loading the view from its nib.
}
    //更多商品描述
- (void)handlesingleTouchTap:(UITapGestureRecognizer *)handlesingleTouchTap {
    
    
            MoreDescribeViewController * MoreDescribe =[[MoreDescribeViewController alloc] initWithNibName:@"MoreDescribeViewController" bundle:nil];
            MoreDescribe.strLabTitle = @"更多描述";
            MoreDescribe.strCoentText = @"更多描述更多描述更多描述更多描述更多描述更多描述更多描述更多描述更多描述更多描述更多描述";
        
        
            [self.navigationController pushViewController:MoreDescribe animated:YES];
    
    
    NSLog(@"my handlesingleTouchTap");
}




#pragma mark - network request getHotCommoditysRequest

-(void)getSellerRequest
{
    [[LoadingView shareLoadingView] show];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/javascript"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];

    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,getSellerAction];
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];

    NSDictionary *params = @{

                             @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                             @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId],
                             @"userId":[NSString stringWithFormat:@"%@",shareUser.user.user_id]

                             };




    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {


         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];

         NSDictionary * dicRespon = responseObject;
         [self getSellerRequestSuccessWith:dicRespon];

         [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [[LoadingView shareLoadingView] dismissAnimated:YES];
         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];

}

-(void)getSellerRequestSuccessWith:(NSDictionary *)dicRespon
{

    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
    {



        self.labAcount.text =[NSString stringWithFormat:@"%@",[ShareUserModel shareUserModel].user.loginName];

        self.labIDCard.text =[NSString stringWithFormat:@"%@",[dicRespon objectForKey:@"idCardNo"]];
        
        self.labName.text =[NSString stringWithFormat:@"%@",[dicRespon objectForKey:@"storeType"]];
        
        self.labTextDesc.text =[NSString stringWithFormat:@"%@",[dicRespon objectForKey:@"storeContent"]];
        self.labNum.text =[NSString stringWithFormat:@"%@",[dicRespon objectForKey:@"storePhone"]];
        self.labAddress.text =[NSString stringWithFormat:@"%@",[dicRespon objectForKey:@"storeAddress"]];
      
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        
    }
    else
    {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

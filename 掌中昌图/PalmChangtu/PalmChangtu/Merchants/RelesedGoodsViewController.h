//
//  RelesedGoodsViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/17.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface RelesedGoodsViewController :UIViewController<UITextViewDelegate,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
@property (strong,nonatomic)  NSMutableArray * arrData;

@property (strong,nonatomic) IBOutlet UITableView * tableView;
/*
 * UIcollection 控件
 */

@property (strong,nonatomic) IBOutlet UICollectionView * CollectionView;

@end

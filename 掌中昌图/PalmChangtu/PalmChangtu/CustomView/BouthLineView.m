//
//  BouthLineView.m
//  GroupDeal
//
//  Created by lewang on 14-6-18.
//  Copyright (c) 2014年 groupdeal. All rights reserved.
//

#import "BouthLineView.h"

@implementation BouthLineView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    // Drawing lines with a white stroke color
	CGContextSetRGBStrokeColor(context, 214.0/255, 214.0/255, 214.0/255, 1.0);
	CGContextSetLineWidth(context, 0.5);
	

	CGContextMoveToPoint(context, 0.0, 0.0);
	CGContextAddLineToPoint(context, self.frame.size.width, 0.0);
    
    
    CGContextMoveToPoint(context, 0.0,self.frame.size.height);
	CGContextAddLineToPoint(context, self.frame.size.width, self.frame.size.height);
    
    
	CGContextStrokePath(context);
	// Draw a connected sequence of line segments
//	CGPoint addLines[] =
//	{
//		CGPointMake(0.0, 0.0),
//		CGPointMake(self.frame.size.width, 0.0),
//	};
//	CGContextAddLines(context, addLines, sizeof(addLines)/sizeof(addLines[0]));
//	
//    
//    CGPoint addLines2[] =
//	{
//		CGPointMake(0.0,                   self.frame.size.height),
//		CGPointMake(self.frame.size.width, self.frame.size.height),
//	};
//    CGContextAddLines(context, addLines2, sizeof(addLines2)/sizeof(addLines2[0]));
//    CGContextStrokePath(context);
}


@end

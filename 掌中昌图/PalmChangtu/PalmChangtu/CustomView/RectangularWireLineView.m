//
//  RectangularWireLineView.m
//  smart_village
//
//  Created by shaorui on 15/3/18.
//  Copyright (c) 2015年 osoons. All rights reserved.
//

#import "RectangularWireLineView.h"

@implementation RectangularWireLineView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
            // Initialization code

        
    }
    return self;
}


    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    
    self.viewPrintFormatter.view.backgroundColor  =[UIColor clearColor];
    self.viewPrintFormatter.view.layer.backgroundColor = [UIColor clearColor].CGColor;
    self.viewPrintFormatter.view.layer.borderColor = [UIColor colorWithWhite:0.917 alpha:1.000].CGColor;
    self.viewPrintFormatter.view.layer.borderWidth = 0.5;
    
}

@end

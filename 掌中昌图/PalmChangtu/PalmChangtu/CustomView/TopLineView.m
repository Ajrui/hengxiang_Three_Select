//
//  TopLineView.m
//  GroupDeal
//
//  Created by lewang on 14-6-18.
//  Copyright (c) 2014年 groupdeal. All rights reserved.
//

#import "TopLineView.h"
//#import <CoreGraphics/CGContext.h>

@implementation TopLineView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}


-(void)awakeFromNib
{
    [self setBackgroundColor:[UIColor whiteColor]];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetRGBStrokeColor(context, 214.0/255, 214.0/255, 214.0/255, 1.0);
	// Draw them with a 2.0 stroke width so they are a bit more visible.
	CGContextSetLineWidth(context, 1.0);
	
	// Draw a single line from left to right
	CGContextMoveToPoint(context, 0.0, 0.0);
	CGContextAddLineToPoint(context, self.frame.size.width, 0.0);
	CGContextStrokePath(context);
	
}


@end

//
//  BouthLineButton.m
//  smart_village
//
//  Created by Jabez on 11/3/15.
//  Copyright (c) 2015 osoons. All rights reserved.
//

#import "BouthLineButton.h"

@implementation BouthLineButton


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
// Drawing code
CGContextRef context = UIGraphicsGetCurrentContext();
// Drawing lines with a white stroke color
CGContextSetRGBStrokeColor(context, 214.0/255, 214.0/255, 214.0/255, 1.0);
CGContextSetLineWidth(context, 1.0);


CGContextMoveToPoint(context, 0.0, 0.0);
CGContextAddLineToPoint(context, self.frame.size.width, 0.0);


CGContextMoveToPoint(context,    0.0,  self.frame.size.height);
    CGContextAddLineToPoint(context, self.frame.size.width, self.frame.size.height);
    
    
    CGContextStrokePath(context);
}

@end

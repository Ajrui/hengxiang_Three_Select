//
//  commentsCell.h
//  smart_village
//
//  Created by shaorui on 15/2/26.
//  Copyright (c) 2015年 osoons. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingView.h"

@interface commentsCell : UITableViewCell
/**
 *  名字
 */
@property (strong, nonatomic) IBOutlet UILabel *userName;
/**
 *  描述
 */
@property (strong, nonatomic) IBOutlet UITextView *CommentDesc;
/**
 *  评论时间
 */
@property (strong, nonatomic) IBOutlet UILabel *commentTime;

/**
 *  评分
 */
@property (strong, nonatomic) IBOutlet RatingView *viewComment;

@end

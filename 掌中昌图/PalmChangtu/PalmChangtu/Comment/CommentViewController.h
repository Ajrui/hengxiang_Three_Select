//
//  CommentViewController.h
//  maintainer
//
//  Created by shaorui on 15/3/5.
//  Copyright (c) 2015年 osoons. All rights reserved.
//

#import "STableViewController.h"
#import "RatingView.h"
@class UITopBanner;

@interface CommentViewController : STableViewController<RatingViewDelegate>
@property(nonatomic, strong, readonly) NSMutableArray *arrData;
@property(nonatomic, assign)BOOL needUpdate;
@property (strong, nonatomic) IBOutlet UITopBanner *vTopBanner;
@property (nonatomic, strong)IBOutlet UIView *vTableBg;
/**
 *  维修工 Id
 */
@property (strong, nonatomic) NSString * strRepaiIid;

@end

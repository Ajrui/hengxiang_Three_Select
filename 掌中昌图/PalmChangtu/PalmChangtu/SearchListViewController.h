//
//  SearchListViewController.h
//  
//
//  Created by shaorui on 15/10/13.
//
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
#import "ClassSerchViewController.h"
@interface SearchListViewController : UIViewController<KeyWordSearchDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
/**
 * 顶上的UITopView
 */
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
@property (strong,nonatomic) id <KeyWordSearchDelegate> delegateKeyWord;
@property (strong,nonatomic) NSMutableArray * arrData;
@property (strong,nonatomic) IBOutlet UITableView * tableView;
@property (strong, nonatomic) IBOutlet UIButton *btnSearchAction;

/**
* textSearch 关键字
 */
@property (strong,nonatomic) IBOutlet UITextField * textSearch;
@end

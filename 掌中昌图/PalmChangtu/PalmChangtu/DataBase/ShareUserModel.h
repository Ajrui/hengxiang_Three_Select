//
//  ShareUserModel.h
//  GroupDeal
//
//  Created by lewang on 14-7-28.
//  Copyright (c) 2014年 groupdeal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "USER_INFO.h"

@interface ShareUserModel : NSObject
/**
 *  USER_INFO 用户信息单例
 */
@property(nonatomic, strong)USER_INFO *user;
/**
 *  已经登录了的
 */
@property(nonatomic, assign)BOOL isLogined;

@property(nonatomic, assign)NSString *strDevice;

+(ShareUserModel *)shareUserModel;
/**
 *  保存用户信息
 */
-(void)SaveUserInfo:(NSDictionary *)DicUser WithPass:(NSString *)strPasswd;


/**
 *更新用户信息
 */
-(void)UPdateUserInfo:(NSDictionary *)DicUser;

/**
 *  已经退出登录
 */
-(void)LogOutUser;

@end

//
//  SignleModelHandler.h
//  GroupDeal
//
//  Created by lewang on 14-7-31.
//  Copyright (c) 2014年 groupdeal. All rights reserved.
//
#import <CoreLocation/CLLocation.h>
#import <Foundation/Foundation.h>
@class  USER_INFO;

@interface SignleModelHandler : NSObject

@property (nonatomic, strong)NSManagedObjectContext *objectContext;
@property (nonatomic, assign) CLLocationCoordinate2D locationCoordTmp;

+(SignleModelHandler *)shareSignleModelHandler;


#pragma mark  - 用户信息保存
-(BOOL)ClearUserInfo;
-(void)InsertUserInfo:(USER_INFO *)user;

- (USER_INFO *)UserLastLogin;

//
//-(void)UpdateSelectedCity:(SELECTED_CITY *)city;//当前选择城市
//
//- (SELECTED_CITY *)selected_city;


#pragma mark  - 数据合并
- (void)mergeChanges:(NSNotification *)notification;


@end

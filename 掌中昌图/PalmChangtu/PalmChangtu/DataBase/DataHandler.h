//
//  DataHandler.h
//  GroupDeal
//
//  Created by lewang on 14-5-21.
//  Copyright (c) 2014年 groupdeal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/NSManagedObject.h>

@interface DataHandler : NSObject

#pragma mark - dic to object
//如果没有时间格式限制，使用它
+ (void)SetValuesForKeysWithDictionary:(NSDictionary *)keyedValues ToObject:(NSManagedObject *)object;

//指定特定的时间表达格式(如 2014-12-25)
+ (void)SetValuesForKeysWithDictionary:(NSDictionary *)keyedValues dateFormatter:(NSDateFormatter *)dateFormatter  ToObject:(NSManagedObject *)object;


#pragma mark - objet to dic
+ (NSDictionary*) toDictionaryFrom:(NSManagedObject *)object;


#pragma mark  - copy object to object
//复制NSManageObject
+ (NSManagedObject*)copyObject:(NSManagedObject *)object
                     toContext:(NSManagedObjectContext*)moc;


@end

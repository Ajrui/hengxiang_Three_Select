//
//  ShareUserModel.m
//  GroupDeal
//
//  Created by lewang on 14-7-28.
//  Copyright (c) 2014年 groupdeal. All rights reserved.
//

#import "ShareUserModel.h"
#import "SignleModelHandler.h"
#import "DataHandler.h"
#import "AppDelegate.h"
#import "BaseUrlConfig.h"
#import "DEVICE.h"

static ShareUserModel *_shareUserModel;

@implementation ShareUserModel


- (instancetype)init
{
    self = [super init];
    if (self) {
        _user = nil;
        _isLogined = NO;
        _strDevice = @"";
    }
    return self;
}

+(ShareUserModel *)shareUserModel
{

    
    @synchronized(self)
    {
        if (!_shareUserModel)
        {
            _shareUserModel = [[ShareUserModel alloc] init];
        
        
        
            //读取数据库
            _shareUserModel.user = [[SignleModelHandler shareSignleModelHandler] UserLastLogin];
            if (_shareUserModel.user) {
                _shareUserModel.isLogined = YES;
            }else{
                _shareUserModel.isLogined = NO;
            }
        }
    }
    
    return _shareUserModel;
}


+(id)allocWithZone:(NSZone *)zone{
    
    @synchronized(self)
    {
        if (!_shareUserModel) {
            _shareUserModel = [super allocWithZone:zone];
            return _shareUserModel;
        }
    }
    
    return nil;
}


-(void)LogOutUser
{
    //更新状态
    ShareUserModel *shareModel = [ShareUserModel shareUserModel];
    shareModel.isLogined = NO;
    shareModel.user = nil;
    [[SignleModelHandler shareSignleModelHandler] ClearUserInfo];
}





-(void)SaveUserInfo:(NSDictionary *)DicUser WithPass:(NSString *)strPasswd
{
    
    
    if (DicUser)
    {
        dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            _shareUserModel.isLogined = YES;
            AppDelegate *deleaget = ((AppDelegate*)[[UIApplication sharedApplication] delegate]);
            _shareUserModel.user = [USER_INFO initWithContext:deleaget.managedObjectContext isTemp:YES];
            [DataHandler SetValuesForKeysWithDictionary:DicUser ToObject:_shareUserModel.user];
            
            [_shareUserModel.user setUser_id:[DicUser objectForKey:@"id"]];
            
//            [_shareUserModel.user setPassword:strPasswd];
//            
//            
//            if (![[DicUser objectForKey:@"Phone"] isEqual:[NSNull null]]) {
//                NSLog(@"%@",[DicUser objectForKey:@"phone"]);
//
//                [_shareUserModel.user setPhone:[DicUser objectForKey:@"Phone"]];
//
//            }
//            
//            else  if (!strIsEmpty([DicUser objectForKey:@"Phone"])) {
//                NSLog(@"%@",[DicUser objectForKey:@"Phone"]);
//
//                
//                [_shareUserModel.user setPhone:[DicUser objectForKey:@"Phone"]];
//
//            }
//            if (![DicUser objectForKey:@"Phone"] )
//            {
//            
//            NSLog(@"%@",[DicUser objectForKey:@"Phone"]);
//
//            }
            NSArray *arrDevice = [DicUser objectForKey:@"devices"];
            if (arrDevice && [arrDevice isKindOfClass:[NSArray class]])
            {
                for (NSDictionary *dicDevice in arrDevice) {
                    DEVICE *device = [DEVICE initWithContext:deleaget.managedObjectContext isTemp:YES];
                    [DataHandler SetValuesForKeysWithDictionary:dicDevice ToObject:device];
//                    [_shareUserModel.user addDevicesObject:device];
                }
                
            }
            
//            for (DEVICE *device in _shareUserModel.user.devices) {
//                NSLog(@"device :%@", device.pin_nickname);
//            }

        });
    
    

        [[SignleModelHandler shareSignleModelHandler] InsertUserInfo:_shareUserModel.user];
    }
}

-(void)UPdateUserInfo:(NSDictionary *)DicUser
{
    
    
    if ( _shareUserModel.user && _shareUserModel.isLogined) {
        _shareUserModel.user = [[SignleModelHandler shareSignleModelHandler] UserLastLogin];
        [DataHandler SetValuesForKeysWithDictionary:DicUser ToObject:_shareUserModel.user];
        
        
        if (![DicUser objectForKey:@"Phone"])
            
            {
                NSLog(@"%@",[DicUser objectForKey:@"Phone"]);
            
               [_shareUserModel.user setPhone:[DicUser objectForKey:@"Phone"]];
            
            }
        
    }
}


@end

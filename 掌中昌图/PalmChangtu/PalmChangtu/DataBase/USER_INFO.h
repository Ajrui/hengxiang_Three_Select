//
//  USER_INFO.h
//  PalmChangtu
//
//  Created by osoons on 15/6/24.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface USER_INFO : NSManagedObject

@property (nonatomic, retain) NSString * createTime;
@property (nonatomic, retain) NSString * loginName;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * photoUrl;
@property (nonatomic, retain) NSString * sessionId;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * versionId;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSString * success;
@property (nonatomic, retain) NSNumber * storeType;
+(USER_INFO *)initWithContext:(NSManagedObjectContext *)context isTemp:(BOOL)isTemp;

@end

//
//  DEVICE.h
//  smart_village
//
//  Created by Jabez on 30/4/15.
//  Copyright (c) 2015 osoons. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DEVICE : NSManagedObject {
@private
}
@property (nonatomic, retain) NSString * id_device;
@property (nonatomic, retain) NSString * pin_no;
@property (nonatomic, retain) NSString * pin_name;
@property (nonatomic, retain) NSString * pin_nickname;
@property (nonatomic, retain) NSString * notno;
@property (nonatomic, retain) NSString * device_no;


+(DEVICE *)initWithContext:(NSManagedObjectContext *)context isTemp:(BOOL)isTemp;

@end

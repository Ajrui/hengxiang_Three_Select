//
//  USER_INFO.m
//  PalmChangtu
//
//  Created by osoons on 15/6/24.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "USER_INFO.h"


@implementation USER_INFO

@dynamic createTime;
@dynamic loginName;
@dynamic password;
@dynamic phone;
@dynamic photoUrl;
@dynamic sessionId;
@dynamic status;
@dynamic type;
@dynamic user_id;
@dynamic userName;
@dynamic versionId;
@dynamic message;
@dynamic success;
@dynamic storeType;
+(USER_INFO *)initWithContext:(NSManagedObjectContext *)context isTemp:(BOOL)isTemp
{
    USER_INFO *modelObject = nil;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"USER_INFO" inManagedObjectContext:context];
    if (isTemp) {
        modelObject = [[USER_INFO alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
    }else
    {
        modelObject = [[USER_INFO alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    }
    return modelObject;
}
@end

//
//  DEVICE.m
//  smart_village
//
//  Created by Jabez on 30/4/15.
//  Copyright (c) 2015 osoons. All rights reserved.
//

#import "DEVICE.h"


@implementation DEVICE
@dynamic id_device;
@dynamic pin_no;
@dynamic pin_name;
@dynamic pin_nickname;
@dynamic notno;
@dynamic device_no;

+(DEVICE *)initWithContext:(NSManagedObjectContext *)context isTemp:(BOOL)isTemp
{
    DEVICE *modelObject = nil;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DEVICE" inManagedObjectContext:context];
    if (isTemp) {
        modelObject = [[DEVICE alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
    }else
    {
        modelObject = [[DEVICE alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    }
    return modelObject;
}


@end

//
//  CollectionViewController.m
//  testMenu
//
//  Created by osoons on 15/6/16.
//  Copyright (c) 2015年 TestPop. All rights reserved.
//

#import "CollectionViewController.h"
#import "localJSONModel.h"
#import "FPPopoverController.h"
#import "FPTouchView.h"
#import "MenuCell.h"
@interface CollectionViewController ()

@end

@implementation CollectionViewController
@synthesize delegate;
-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
    }
    return self;
}
static NSString * const reuseIdentifier = @"MenuCell";

-(void)initIDType:(NSString *)type
{
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    
    //user nib
    

    
    CGFloat widthFlow  = self.collectionView.frame.size.width;
    CGFloat heightFlow = self.collectionView.frame.size.height;
    
    UICollectionViewFlowLayout *flowOut = [[UICollectionViewFlowLayout alloc] init];
    [flowOut setItemSize:CGSizeMake(widthFlow/3, 40)];
    
    [flowOut setMinimumLineSpacing:0.0];
    [flowOut setMinimumInteritemSpacing:0.0];
    [flowOut setScrollDirection:UICollectionViewScrollDirectionVertical];
    [self.collectionView setCollectionViewLayout:flowOut];
    
//    user nib
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([MenuCell class]) bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:[NSString stringWithFormat:@"%@", [MenuCell class]]];
    

    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
    [self.collectionView setShowsVerticalScrollIndicator:NO];
    [self.collectionView setAlwaysBounceVertical:YES];
    [self.collectionView setAlwaysBounceHorizontal:NO];
    [self.collectionView setPagingEnabled:YES];
    
    

    

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
    
    
    
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width/3, 40);
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [_arrayMenuData count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //
    static NSString *cellIdent = @"MenuCell";
    MenuCell * cell;
    cell = (MenuCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdent forIndexPath:indexPath];
    
    localJSONModel * model = [_arrayMenuData objectAtIndex:indexPath.row];
    
    cell.labMenu.text = model.name;
    
    
    
    
    
    return cell;
    
    
    
}
#pragma mark - UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    

//    NSString * str =[NSString stringWithFormat:@"%@",[_arrayMenuData objectAtIndex:indexPath.row]];
//    UIAlertView * sss = [[UIAlertView alloc] initWithTitle:@"你点的是"message:str delegate:nil cancelButtonTitle:nil otherButtonTitles:@"CANCLE",@"OK", nil];
//    
//    [sss show];
//    
    localJSONModel * model = [_arrayMenuData objectAtIndex:indexPath.row];

    [delegate selectedOneSelectType:model];
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    

    [self.typePop dismissPopoverAnimated:YES];
    
    
    //    MenuCell * cell = (MenuCell*)[collectionView cellForItemAtIndexPath:indexPath];
    
}



@end

//
//  CollectionViewController.h
//  testMenu
//
//  Created by osoons on 15/6/16.
//  Copyright (c) 2015年 TestPop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPopoverController.h"
#import "localJSONModel.h"
@protocol CheckSelectTypeDelegate <NSObject>
/**
 *  回调方法，返回所选值
 */
-(void)selectedOneSelectType:(localJSONModel *)localMode;

@end

@interface CollectionViewController : UICollectionViewController<UICollectionViewDelegateFlowLayout,FPPopoverControllerDelegate>
@property(strong,nonatomic) NSMutableArray *arrayMenuData;
-(void)initIDType:(NSString * )type;
@property(strong,nonatomic)FPPopoverController * typePop;
@property(nonatomic,strong)id<CheckSelectTypeDelegate> delegate;


@end

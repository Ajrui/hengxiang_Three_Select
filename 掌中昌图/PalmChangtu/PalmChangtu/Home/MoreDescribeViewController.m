//
//  MoreDescribeViewController.m
//  PalmChangtu
//
//  Created by shaorui on 15/6/16.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "MoreDescribeViewController.h"

@interface MoreDescribeViewController ()

@end

@implementation MoreDescribeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_v_Top.lalTitel setText:self.strLabTitle];
    [_v_Top.lalTitel setHidden:NO];
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    _v_Top.parentController = self.navigationController;

    self.textview.text = self.strCoentText;
    _v_Top.parentController = self.navigationController;

    [_v_Top.btnGoback setHidden:NO];
    [_v_Top.btnGoback addTarget:self action:@selector(backTo:) forControlEvents:UIControlEventTouchUpInside];

    // Do any additional setup after loading the view from its nib.
}
-(void)backTo:(UIButton *)btn
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  ProdectDetailViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/16.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
#import "RectangularWireLineView.h"
#import "GetRemandModle.h"
#import "ShangPinListModel.h"
@interface ProdectDetailViewController : UIViewController<UIScrollViewDelegate,UIScrollViewAccessibilityDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>

@property (strong,nonatomic) GetRemandModle * model;
@property (strong,nonatomic) ShangPinListModel * modelshangping;
@property (strong,nonatomic) NSString  * strModelshangping;


/**
 * 顶上的UITopView
 */
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;

@property (strong,nonatomic) IBOutlet UITableView * tableView;
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;
/**
 *
 * 文本信息描述 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrTextDescData;


@property (strong, nonatomic) IBOutlet UICollectionView *RollingScrollView;
/**
 *
 * 随着图片滚动文字信息跟着改变UILabel
 */
@property (strong, nonatomic) IBOutlet UILabel *RollingText;
/**
 *
 * 滚动的点UIPageControl
 */
@property (strong, nonatomic) IBOutlet UIPageControl *RollingPagePoint;

/**
 * 菜单选择控件
 * arrayMenuData 数据源
 */

@property(strong,nonatomic) NSMutableArray * arrayBannerData;

/**
 * 点击查看更多售后服务ß
 *
 */
@property (strong, nonatomic) IBOutlet RectangularWireLineView *singleTouch;

/**
 * 点击查看更多商品描述
 */
@property (strong, nonatomic) IBOutlet RectangularWireLineView *singleTouchScale;


/**
 * 点击查看更多好评差评
 */
@property (strong, nonatomic) IBOutlet RectangularWireLineView *singleTouchGoods;


@property (strong, nonatomic) IBOutlet UILabel *labName;
@property (strong, nonatomic) IBOutlet UILabel *labTotalNum;

@property (strong, nonatomic) IBOutlet UILabel *labPrice;

@property (strong, nonatomic) IBOutlet UILabel *textViewContt;
@property (strong, nonatomic) IBOutlet UILabel *labSacleSevrices;

@property (strong, nonatomic) IBOutlet UILabel *labHaoPin;

@property (strong, nonatomic) IBOutlet UILabel *labChaPinNUm;

@property (strong, nonatomic) IBOutlet UIButton *btnAddGouWuChe;


@property (strong, nonatomic) IBOutlet UIButton *btnLiJiGouMai;
@property (strong, nonatomic) NSString *strShangPingID;


@property (strong,nonatomic) NSMutableArray * arrYiJianData;

@end

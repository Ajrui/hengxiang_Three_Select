//
//  BATableViewCell.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/22.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BATableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labName;

@end

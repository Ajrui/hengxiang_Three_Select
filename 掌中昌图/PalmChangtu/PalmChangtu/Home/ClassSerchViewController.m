//
//  SerchClassViewController.m
//  PalmChangtu
//
//  Created by shaorui on 15/6/22.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "ClassSerchViewController.h"
#import "SerchClassTabCell.h"
#import "SerchViewController.h"
#import "ShareUserModel.h"
#import "BATableViewKit/BATableViewCell.h"
#import "ShareUserModel.h"
#import "ProdectDetailViewController.h"
#import "MainTabbarController.h"
#import "JSONKit.h"
#import "Common.h"
#import "LoadingView.h"
#import "NetWarningView.h"
#import "AFNetworking.h"
#import "BaseUrlConfig.h"
#import "TestCollectionViewController.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "BusinessGoodsCell.h"
#import "all_commodity_TypeModel.h"
#import "localJSONModel.h"
#import "MTLJSONAdapter.h"
#import "PingPaiModel.h"
#import "ShangPinListModel.h"
#import "MJRefresh.h"
#import "SearchListViewController.h"
@interface ClassSerchViewController ()
{
    BOOL tabYes;
    NSInteger  currentNum;
    NSInteger pageNum;
    UIButton * butSetTitle;
    /***排序类别：1.品牌 2.销量 3.价格 4.评价 ***/
    NSInteger orderType;
    /***商品类别**/
    NSString * commodityType;
    /***排序集体的类型****/
    NSString *orderSpecificsType;
    /***商品名称（用于模糊查询）****/
    NSString * commodityName;
    
    
    NSString * strKeyWord;
    
    BOOL isFirstShow;

}
@end

@implementation ClassSerchViewController
-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
    }
    return self;
}

-(void)userSearchTextString:(NSString *)str
{

    strKeyWord = [NSString stringWithFormat:@"%@",str];
    [self getHomeActionRequest];
}
-(void)viewDidAppear:(BOOL)animated
{
        //取消选中的cell
    NSArray * indexPaths = [self.tableView indexPathsForSelectedRows];
    
    for(NSIndexPath * indexPath in indexPaths)
        {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
    
    if (isFirstShow) {
        isFirstShow = NO;
        [self getCommoditysActionRequest];
    [self   commodity_breandsActionRequest];

    }
    
//    [self getCommoditysActionRequest];



}
- (void)viewDidLoad {
    [super viewDidLoad];
    isFirstShow = YES;
    self.searchVivew.hidden = YES;

//    commodityType =   @"0";
    orderSpecificsType =@"1";
    tabYes = YES;
    orderType = 5;
    currentNum = 0;
    pageNum = 10;
    [_v_Top.lalTitel setText:@"搜索"];
    
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    [_v_Top.btnGoback removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_v_Top.btnGoback addTarget:self action:@selector(btnBack:) forControlEvents:UIControlEventTouchUpInside];
    




    
    [_btnSearch removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnSearch addTarget:self action:@selector(btnSearchText:) forControlEvents:UIControlEventTouchUpInside];
    

    [_btnPinPai removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnPinPai addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [_btnXiaoLiang removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnXiaoLiang addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnPingJia removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnPingJia addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [_btnJiaGe removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnJiaGe addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];


    [_btnCancleSearch removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];

    [_btnCancleSearch addTarget:self action:@selector(btnCancleSearch:) forControlEvents:UIControlEventTouchUpInside];


    [self createTableView];
    [self setupRefresh];
    
    // Do any additional setup after loading the view from its nib.
}

    //创建 索引 tableView
- (void) createTableView {
        // Do any additional setup after loading the view.
       self.tabView.hidden = YES;

    UINib *nibCell = [UINib nibWithNibName:NSStringFromClass([BATableViewCell class]) bundle:nil];
    [self.tabView registerNib:nibCell forCellReuseIdentifier:[NSString stringWithFormat:@"%@", [BATableViewCell class]]];
    self.tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tabView.dataSource = self;
    self.tabView.delegate = self;


    UINib *nibCell1 = [UINib nibWithNibName:NSStringFromClass([SerchClassTabCell class]) bundle:nil];
    [self.tableView registerNib:nibCell1 forCellReuseIdentifier:[NSString stringWithFormat:@"%@", [SerchClassTabCell class]]];
    
    
    
    
    UINib *nibBusinessGoodsCell = [UINib nibWithNibName:NSStringFromClass([BusinessGoodsCell class]) bundle:nil];
    [self.tableView registerNib:nibBusinessGoodsCell forCellReuseIdentifier:[NSString stringWithFormat:@"%@", [BusinessGoodsCell class]]];

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tag = 2000;
    
    _arrData = [[NSMutableArray alloc] init];
    _dataTileSource = [NSMutableArray array];
    self.dataSource = [[NSMutableArray alloc] init];

    self.dataPinPaiiSource = [[NSMutableArray alloc] init];


}
-(void)btnBack:(UIButton *)btnback
{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
-(void)btnSearchText:(UIButton *)Search

{



    [self.textSearch becomeFirstResponder];

    self.textSearch.returnKeyType = UIReturnKeySearch;


    self.searchVivew.hidden = NO;

    
    SearchListViewController *   SearchListView  = [[SearchListViewController alloc] initWithNibName:@"SearchListViewController" bundle:nil];
    SearchListView.delegateKeyWord = self;
    
    [self presentViewController:SearchListView animated:YES completion:^{
        
    }];
    


    

  }

-(void)btnCancleSearch:(UIButton *)Search

{



    [self.view endEditing:YES];


    self.textSearch.returnKeyType = UIReturnKeySearch;

    self.btnSearch.hidden = NO;
    self.searchVivew.hidden = YES;

    [self getCommoditysActionRequest];
    
    
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self getCommoditysActionRequest];
    return YES;

}
-(void)btnAction:(UIButton *)butAction
{
    
    
    [self headerRereshing];
    ShareUserModel * ShareUser =[ShareUserModel shareUserModel];
    
    
 
  
    butSetTitle  = (UIButton *) butAction;
    
    if (butAction == _btnPinPai) {
        self.tabView.tag = 1000;
        orderType = 5;

//
//        NSString * path=[[NSBundle mainBundle] pathForResource:@"localType" ofType:@"json"];
//        // path=@"app_menu";
//
//        NSData * jsonData=[NSData dataWithContentsOfFile:path];
//
//        NSString * jsonString=[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//
//        NSDictionary * dic =[[MainTabbarController ShareTabBarController] dictionaryWithJsonString:jsonString];
//
//        NSMutableArray  * ArayData = [[NSMutableArray alloc] init];
//
//        for (NSDictionary  * tempDic in [dic objectForKey:@"arr"]  ) {
//
//            if ([[tempDic objectForKey:@"parentId"] integerValue] !=0) {
//
//                NSError *error = nil;
//                all_commodity_TypeModel *modelTmp =
//                [MTLJSONAdapter modelOfClass:[all_commodity_TypeModel class] fromJSONDictionary:tempDic error:&error];
//                if(error == nil)
//                {
//                    [ArayData addObject:modelTmp];
//                }
//            }
//
//        }


        
//        self.dataSource = @[@{@"indexTitle": @"",@"data":@[@"全部"]},@{@"indexTitle": @"A",@"data":@[@"邵瑞", @"熊瑶", @"ain", @"abdul", @"anastazja", @"angelica"]},@{@"indexTitle": @"D",@"data":@[@"这个" , @"中国", @"美国", @"dragon", @"dry", @"日本", @"drums"]},@{@"indexTitle": @"F",@"data":@[@"意大利", @"英国", @"friends", @"family", @"fatish", @"funeral"]},@{@"indexTitle": @"M",@"data":@[@"Mark", @"好好"]},@{@"indexTitle": @"N",@"data":@[@"哈哈哈哈哈", @"nemo", @"name"]},@{@"indexTitle": @"O",@"data":@[@"Obama", @"Oprah", @"Omen", @"OMG OMG OMG", @"O-二位", @"Ontario"]},@{@"indexTitle": @"Z",@"data":@[@"Zeus", @"Zebra", @"zed"]}];

        _dataTileSource = [[NSMutableArray alloc] initWithObjects:@"A",@"B",@"C",@"D",@"E",@"F",@"H",@"K",@"L",@"M",@"N", nil];
//        for (NSDictionary * sectionDictionary in self.dataSource)
//        {
//            [_dataTileSource addObject:sectionDictionary[@"indexTitle"]];
//        }

        [_imagePinPai setImage:[UIImage imageNamed:@"向上箭头"]];
        [_imageXiaoLiang setImage:[UIImage imageNamed:@"下拉箭头-1"]];
        [_imageJiaGe setImage:[UIImage imageNamed:@"下拉箭头-1"]];
        [_imagePingJia setImage:[UIImage imageNamed:@"下拉箭头-1"]];


    }
    
    else if (butAction == _btnXiaoLiang) {
        orderType = 5;
        self.tabView.tag = 1001;



        NSDictionary  * dic0  =[NSDictionary dictionaryWithObjectsAndKeys: @"默认", @"name",@"1",@"idname",    nil];
        NSDictionary  * dic1  = [NSDictionary dictionaryWithObjectsAndKeys: @"从低到高",@"name", @"2",@"idname",nil];
        NSDictionary  * dic2  = [NSDictionary dictionaryWithObjectsAndKeys:   @"从高到低",@"name",@"3",@"idname", nil];

        
        
        self.dataSource =  [[NSMutableArray  alloc] initWithObjects:dic0,dic1,dic2,nil];
        [_imageXiaoLiang setImage:[UIImage imageNamed:@"向上箭头"]];
        [_imagePinPai setImage:[UIImage imageNamed:@"下拉箭头-1"]];
        [_imageJiaGe setImage:[UIImage imageNamed:@"下拉箭头-1"]];
        [_imagePingJia setImage:[UIImage imageNamed:@"下拉箭头-1"]];
        [_dataTileSource removeAllObjects];


    }
    else if (butAction == _btnJiaGe) {
        orderType = 5;
        self.tabView.tag = 1001;


        NSDictionary  * dic0  = @{

                                  @"name":@"默认",
                                  @"idname":@"1",
                                  };
        NSDictionary  * dic1  = @{

                                  @"name":@"从低到高",
                                  @"idname":@"2",
                                  };
        NSDictionary  * dic2  = @{

                                  @"name":@"从高到低",
                                  @"idname":@"3",
                                  };
        NSDictionary  * dic3  = @{

                                  @"name":@"0-499元",
                                  @"idname":@"4",
                                  };
        NSDictionary  * dic4  = @{

                                  @"name":@"500-999元",
                                  @"idname":@"5",
                                  };

        NSDictionary  * dic5  = @{

                                  @"name":@"1000-1999元",
                                  @"idname":@"6",
                                  };
        NSDictionary  * dic6  = @{

                                  @"name":@"2000-4999元元",
                                  @"idname":@"7",
                                  };
        NSDictionary  * dic7  = @{

                                  @"name":@"5000以上",
                                  @"idname":@"8",
                                  };

        self.dataSource =  [[NSMutableArray  alloc] initWithObjects:dic0,dic1,dic2,dic3,dic4,dic5,dic6,dic7, nil];



        [_dataTileSource removeAllObjects];

        [_imageJiaGe setImage:[UIImage imageNamed:@"向上箭头"]];
        [_imagePinPai setImage:[UIImage imageNamed:@"下拉箭头-1"]];
        [_imageXiaoLiang setImage:[UIImage imageNamed:@"下拉箭头-1"]];
        [_imagePingJia setImage:[UIImage imageNamed:@"下拉箭头-1"]];


    }
    else if (butAction == _btnPingJia) {
        orderType = 5;
        self.tabView.tag = 1001;


        NSDictionary  * dic0  = @{

                                  @"name":@"默认",
                                  @"idname":@"1",

                                  };
        NSDictionary  * dic1  = @{
                                  @"name":@"从高到低",
                                  @"idname":@"2",
                                  };
        NSDictionary  * dic2  = @{

                                  @"name":@"从高到低",
                                  @"idname":@"3",
                                  };

        
        self.dataSource =  [[NSMutableArray  alloc] initWithObjects:dic0,dic1,dic2,nil];

        [_dataTileSource removeAllObjects];

        [_imagePingJia setImage:[UIImage imageNamed:@"向上箭头"]];
        [_imagePinPai setImage:[UIImage imageNamed:@"下拉箭头-1"]];
        [_imageXiaoLiang setImage:[UIImage imageNamed:@"下拉箭头-1"]];
        [_imageJiaGe setImage:[UIImage imageNamed:@"下拉箭头-1"]];


    }
   
    self.tabView.hidden = NO;

    [self.tabView reloadData];

    

    
}

#pragma mark - UITableViewDataSource
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    if (tableView.tag ==  1000) {

               return _dataTileSource;
    }
    else
        return 0;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (tableView.tag == 1000) {
           NSString *key = [_dataTileSource objectAtIndex:index];
    NSLog(@"sectionForSectionIndexTitle key=%@",key);
    if (key == UITableViewIndexSearch) {
        [self.tabView setContentOffset:CGPointZero animated:NO];
        return NSNotFound;
    }
    
    return index;
    }
    else
        {
         return 0;
        }
 
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    if (tableView.tag ==  1000) {
        return nil;//self.dataSource[section][@"indexTitle"];

    }
    else if (tableView.tag == 2000) {

        return  nil;

    }

    else
    return  nil;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        // Return the number of sections.
    
    if (tableView.tag == 1000) {

        return  1;//self.dataPinPaiiSource.count;

    }
    else if(tableView.tag == 1001)
        {
        return  1;
        }
    else if (tableView.tag == 2000) {
        
        return  1;
        
    }
    
    else
        return 0;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (tableView.tag == 1000||tableView.tag ==1001) {
        return 60;
        
    }
    if (tableView.tag == 2000) {
        
        return  100;
        
    }
    
    else
        return  0;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView.tag == 1000)
    {
    
    
        return [ self.dataPinPaiiSource count];
    
    }
    else if (tableView.tag == 1001)
        {
        
         return [self.dataSource count];
    }
    
   else  if (tableView.tag == 2000) {
        
        if([_arrData count])
            {
            return [_arrData count];
            }
        else
            {
            return 0;
            }
        
    }
    
    else
        return  0;
    
    
}


#pragma mark - Standard TableView delegates
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    

        if (tableView.tag ==  1000)
        {
            static NSString * identifier = @"BATableViewCell";
            BATableViewCell * Cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            Cell.selectionStyle  = UITableViewCellSelectionStyleNone;

                PingPaiModel * model = self.dataPinPaiiSource[indexPath.row];
                Cell.labName.text = model.brandName;
        
        return Cell;
        }
    else if (tableView.tag ==  1001)
        {
            static NSString * identifier = @"BATableViewCell";
            BATableViewCell * Cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            Cell.selectionStyle  = UITableViewCellSelectionStyleNone;


            NSDictionary * dic = [_dataSource objectAtIndex:indexPath.row];
            Cell.labName.text = [dic objectForKey:@"name"];
            
            return Cell;
        }

     else if (tableView.tag == 2000) {
         
         
         
         if (strKeyWord!=nil) {
             static NSString * identifier = @"BusinessGoodsCell";
             BusinessGoodsCell * Cell = [tableView dequeueReusableCellWithIdentifier:identifier];
             Cell.selectionStyle  = UITableViewCellSelectionStyleNone;
             
             if ([_arrData count]!=0) {
                 
                 
                 GetRemandModle * dicModel  = [_arrData objectAtIndex:indexPath.row];
                 
                 
                     //        NSData * data = [[NSData alloc] initWithBase64EncodedString:[dic objectForKey:@"goods_thumb"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
                     //        UIImage *_decodedImage = [UIImage imageWithData:data];
                 
                 if (dicModel.photoUrl.length!=0) {
                     
                     [Cell.imaegProdect sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",HostImage,dicModel.photoUrl]] placeholderImage:[UIImage imageNamed:@"SDWebImageDefault"]];
                 }
                 else{
                     [Cell.imaegProdect setImage:[UIImage imageNamed:@"SDWebImageDefault"]];
                     
                 }
                 Cell.labDescription.text = [NSString stringWithFormat:@"%@",dicModel.content];
                 Cell.labTitle.text  = [NSString stringWithFormat:@"%@",dicModel.name];
                 Cell.labPrice.text = [NSString stringWithFormat:@"¥%@",dicModel.price];
                 
                 
                 
                 
                     //        Cell.btnDelteAction.tag  = indexPath.row;
                     //        [Cell.btnDelteAction removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
                     //        
                     //        [Cell.btnDelteAction addTarget:self action:@selector(btnDelteAction:) forControlEvents:UIControlEventTouchUpInside];
                 
             }
             
             
             return Cell;

         }
         else
             {
             
             
             
             static NSString * identifier = @"SerchClassTabCell";
             SerchClassTabCell * Cell = [tableView dequeueReusableCellWithIdentifier:identifier];
             Cell.selectionStyle  = UITableViewCellSelectionStyleGray;
             
             if ([_arrData count]!=0) {
                 
                 
                 ShangPinListModel * dicModel  = [_arrData objectAtIndex:indexPath.row];
                 
                 
                 [Cell.image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",HostImage,dicModel.photoUrl]] placeholderImage:[UIImage imageNamed:@"SDWebImageDefault"]];
                 Cell.labName.text = dicModel.name;
                 Cell.labContent.text = dicModel.content;
                 Cell.labPrice.text = [NSString stringWithFormat:@"¥%@",dicModel.price];
                 Cell.labScale.text =  [NSString stringWithFormat:@"%@",dicModel.saleNo];
                 
                 
                 
                 
                 
             }
                 //        [Cell.imaegProdect sd_setImageWithURL:[NSURL URLWithString:] placeholderImage:[UIImage imageNamed:@"SDWebImageDefault"]];
             
             return Cell;
             

             
             }
        
    }
    

    else
        return  nil;
  }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (tableView.tag == 1000 || tableView.tag == 1001)
        {

            tableView.hidden = YES;
            NSString * str = nil;
            if (tableView.tag == 1000) {

                PingPaiModel * mode  = [ self.dataPinPaiiSource objectAtIndex:indexPath.row];
                str =  mode.brandName;
                commodityType = [NSString stringWithFormat:@"%@",mode.commodityTypeId];
                commodityName = [NSString stringWithFormat:@"%@",mode.brandName];

//                orderSpecificsType = [NSString stringWithFormat:@"%@",mode.brandName];

            }
            else
            {


                NSDictionary  *   dic  = [self dataSource][indexPath.row ];
               str  = [dic objectForKey:@"name"];



                orderSpecificsType = [dic objectForKey:@"idname"];

            
            }



            
//            [_btnPinPai setTitle:str forState:UIControlStateNormal];
            if (butSetTitle == _btnPinPai) {
               
                [butSetTitle setTitle:str forState:UIControlStateNormal];

            
            }
            
            else if (butSetTitle == _btnXiaoLiang)
            {
                
                if([str isEqualToString:@"从高到低"])
                {
                    [_imagePXXiaoLiang setImage:[UIImage imageNamed:@"下降箭头"]];
                }
                else if ([str isEqualToString:@"从低到高"])
                {
                    [_imagePXXiaoLiang setImage:[UIImage imageNamed:@"上升箭头"]];

                }

                [butSetTitle setTitle:str forState:UIControlStateNormal];

                NSDictionary  *   dic  = [self dataSource][indexPath.row ];
                str  = [dic objectForKey:@"name"];



                orderSpecificsType = [dic objectForKey:@"idname"];

            }
            else if (butSetTitle == _btnJiaGe)
            {
                [butSetTitle setTitle:str forState:UIControlStateNormal];
                if([str isEqualToString:@"从高到低"])
                {
                    [_imagePXJiaGe setImage:[UIImage imageNamed:@"下降箭头"]];
                }
                else if ([str isEqualToString:@"从低到高"])
                {
                    [_imagePXJiaGe setImage:[UIImage imageNamed:@"上升箭头"]];
                    
                }
                else
                {
                    [_imagePXJiaGe setImage:[UIImage imageNamed:@""]];
                    
                }

                NSDictionary  *   dic  = [self.dataSource objectAtIndex: indexPath.row ];
                str  = [dic objectForKey:@"name"];



                orderSpecificsType = [dic objectForKey:@"idname"];

            }
            else if (butSetTitle == _btnPingJia) {
                [butSetTitle setTitle:str forState:UIControlStateNormal];
                
                if([str isEqualToString:@"从高到低"])
                {
                    [_imagePXPingJia setImage:[UIImage imageNamed:@"下降箭头"]];
                }
                else if ([str isEqualToString:@"从低到高"])
                {
                    [_imagePXPingJia setImage:[UIImage imageNamed:@"上升箭头"]];
                }
                else
                {
                    [_imagePXPingJia setImage:[UIImage imageNamed:@""]];

                }
                NSDictionary  *   dic  = [[self dataSource] objectAtIndex: indexPath.row ];
                str  = [dic objectForKey:@"name"];



                orderSpecificsType = [dic objectForKey:@"idname"];


            }

            [self getCommoditysActionRequest];

        }
    if (tableView.tag == 2000) {



        if ([_arrData count]!=0) {

            ShangPinListModel * modelTemp  = [_arrData objectAtIndex:indexPath.row];
            ProdectDetailViewController * detail  = [[ProdectDetailViewController alloc] initWithNibName:@"ProdectDetailViewController" bundle:nil  ];
            detail.modelshangping = modelTemp;
            detail.strModelshangping = @"ClassSerchViewController";


            [self presentViewController:detail animated:YES completion:0 ];

        }



        [tableView reloadData];
        
    }
    
    
    
}
- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated
{
    
//    SerchClassTabCell * cell =  (SerchClassTabCell *)[self.tableView cellForRowAtIndexPath:indexPath];
//    cell.contentView.backgroundColor = [UIColor clearColor];
    
    
    
}
-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return YES;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}


//

#pragma mark - network request commodity_breandsAction

-(void)commodity_breandsActionRequest
{
//    [[LoadingView shareLoadingView] show];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,commodity_breandsAction];
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];

    NSDictionary *params = @{


                             @"typeId":[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",self.mdoelType.localTypeID]],
                             @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                             @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId]

                             };




    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {


         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];

         NSDictionary * dicRespon = responseObject;
         [self commodity_breandSuccessWith:dicRespon];

//         [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
//         [[LoadingView shareLoadingView] dismissAnimated:YES];
//         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];

}

-(void)commodity_breandSuccessWith:(NSDictionary *)dicRespon
{


    NSLog(@" dicRespon %@",dicRespon);
    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
    {


        [self.dataPinPaiiSource removeAllObjects];

        NSArray *arrBussi = [dicRespon objectForKey:@"arr"];
        if (arrBussi && [arrBussi isKindOfClass:[arrBussi class]])
        {
            for (NSDictionary *dicTmp in arrBussi)
            {
                NSError * errorrr = nil;

                PingPaiModel *model =  [MTLJSONAdapter modelOfClass:[PingPaiModel class] fromJSONDictionary:dicTmp error:&errorrr];

                if (errorrr == nil)
                {
                    [ self.dataPinPaiiSource addObject:model];
                }

            }

        }



        [self.tableView reloadData];
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];

        [self.tabView reloadData];

    }
    else
    {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
    }
    
}



#pragma mark - network request getCommoditysAction  21.获取商品列表


-(void)getCommoditysActionRequest
{
//    [[LoadingView shareLoadingView] show];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,getCommoditysAction];
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];

    
    if (strIsEmpty(strKeyWord)) {
        strKeyWord = @"";
    }
    NSDictionary *params = @{


                             @"commodityName":[NSString stringWithFormat:@"%@",strKeyWord],

                             @"orderSpecificsType":[NSString stringWithFormat:@"%@",orderSpecificsType],
                             @"commodityType":[NSString stringWithFormat:@"%@",self.mdoelType.localTypeID],

                             @"orderType":[NSNumber numberWithInteger:orderType],

                             @"currentNum":[NSNumber numberWithInteger:currentNum],
                             @"pageNum":[NSNumber numberWithInteger:pageNum],
                             @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                             @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId]
                             };




    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {


         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];

         NSDictionary * dicRespon = responseObject;
         [self getCommoditysActionSuccessWith:dicRespon];

//         [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
//         [[LoadingView shareLoadingView] dismissAnimated:YES];
//         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];

}
#pragma mark - network request getCommoditysAction  21.获取商品列表

-(void)getCommoditysActionSuccessWith:(NSDictionary *)dicRespon
{

    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
    {



        NSArray *arrBussi = [dicRespon objectForKey:@"arr"];
    
        if (arrBussi.count==0) {
            
            currentNum  = 0 ;
            
            
            pageNum = 10 ;

            return ;
        }
    
        if (arrBussi && [arrBussi isKindOfClass:[arrBussi class]])
        {
        
        [_arrData removeAllObjects];

            for (NSDictionary *dicTmp in arrBussi)
            {
                NSError * errorrr = nil;

                ShangPinListModel *model =  [MTLJSONAdapter modelOfClass:[ShangPinListModel class] fromJSONDictionary:dicTmp error:&errorrr];

                if (errorrr == nil)
                {
                    [_arrData addObject:model];
                }

            }

        }




        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        [self.tableView reloadData];
        
    }
    else
    {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
    }
    
}



#pragma mark 集成刷新控件
- (void)setupRefresh
{
    
        // 1.下拉刷新(进入刷新状态就会调用self的headerRereshing)
    [self.tableView addHeaderWithTarget:self action:@selector(headerRereshing)];
        // 设置文字(也可以不设置,默认的文字在MJRefreshConst中修改)
    self.tableView.headerPullToRefreshText = headerPullToRefreshTextkey;
    self.tableView.headerReleaseToRefreshText  = headerReleaseToRefreshTextkey;
    self.tableView.headerRefreshingText = headerRefreshingTextKey;
    
        // 2.上拉加载更多(进入刷新状态就会调用self的footerRereshing)
    [self.tableView addFooterWithTarget:self action:@selector(footerRereshing)];
    self.tableView.footerPullToRefreshText = footerPullToRefreshTextKey;
    self.tableView.footerReleaseToRefreshText =footerPullToRefreshTextKey;
    self.tableView.footerRefreshingText = footerRefreshingTextKey;
    
}

-(void)headerRereshing{
    
    currentNum  = pageNum;
    
    
    pageNum = currentNum + pageNum ;
    NSLog(@"--------%d-----\n--------%d",currentNum,pageNum);
    
    
    

    [self getCommoditysActionRequest];
    
    

    
    
    [self.tableView headerEndRefreshing];
    
}

-(void)footerRereshing{
    
    currentNum  = pageNum;
    
    
    pageNum = currentNum + pageNum ;
    
    
    NSLog(@"--------%d-----\n--------%d",currentNum,pageNum);
    
    
    
    

    [self getCommoditysActionRequest];
    
    
    
}

#pragma mark - network request getHomeActionRequest

-(void)getHomeActionRequest
{
    
    [self.view endEditing:YES];
    
    
    [[[UIApplication sharedApplication].delegate window]endEditing:YES];
    [[LoadingView shareLoadingView] show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,getCommoditys_homeAction];
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];
    
    if (strIsEmpty(strKeyWord)) {
        strKeyWord = @"";
    }
    
    NSDictionary *params = @{
                             
                             
                             @"commodityName":strKeyWord,
                             @"currentNum":[NSNumber numberWithInteger:currentNum],
                             @"pageNum":[NSNumber numberWithInteger:pageNum],
                             @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                             @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId]
                             
                             };
    
    
    
    
    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
     
     
         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     
     NSDictionary * dicRespon = responseObject;
     [self getHomeActionSuccessWith:dicRespon];
     
     [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
     [[LoadingView shareLoadingView] dismissAnimated:YES];
     [[NetWarningView sharedNetWarningView] show];  //警告网络异常
                                                    //              NSData *doubi = operation.error    ;
                                                    //                  //     JSON格式的NSString转换成NSMutableDictionary
                                                    //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
                                                    //
                                                    //              NSError *err = nil;
                                                    //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSLog(@"Error: %@", error);
     NSLog(@"请求出错了: - %@ \n%@",
           [error localizedDescription],
           [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];
    
}

-(void)getHomeActionSuccessWith:(NSDictionary *)dicRespon
{
    
    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
        {
        
        
        
        strKeyWord = @"";
        [_arrData removeAllObjects];
        
        NSArray *arrBussi = [dicRespon objectForKey:@"arr"];
        if (arrBussi && [arrBussi isKindOfClass:[arrBussi class]])
            {
            for (NSDictionary *dicTmp in arrBussi)
                {
                NSError *error = nil;
                GetRemandModle *modelTmp =
                [MTLJSONAdapter modelOfClass:[GetRemandModle class] fromJSONDictionary:dicTmp error:&error];
                if(error == nil)
                    {
                    [_arrData addObject:modelTmp];
                    }
                }
            
            }
        
        
        
        [self.tableView reloadData];
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        
        }
    else
        {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

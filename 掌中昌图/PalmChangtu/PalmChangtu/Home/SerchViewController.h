//
//  SerchViewController.h
//  PalmChangtu
//
//  Created by osoons on 15/6/23.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface SerchViewController : UIViewController

@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;

@property (weak, nonatomic) IBOutlet UIButton *btnCancleSearch;

/**嗖嗖字符串***/
@property (weak, nonatomic) IBOutlet UITextField *textSearch;

@end

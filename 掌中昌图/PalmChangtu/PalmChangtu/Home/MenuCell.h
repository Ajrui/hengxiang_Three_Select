//
//  MenuCell.h
//  PalmChangtu
//
//  Created by osoons on 15/6/15.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imaegMenu;
@property (weak, nonatomic) IBOutlet UILabel *labMenu;

@end

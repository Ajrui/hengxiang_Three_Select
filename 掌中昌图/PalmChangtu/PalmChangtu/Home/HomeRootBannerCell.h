//
//  HomeRootBannerCell.h
//  PalmChangtu
//
//  Created by 邵瑞 on 15/10/27.
//  Copyright © 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeRootBannerCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imvMain;

@end

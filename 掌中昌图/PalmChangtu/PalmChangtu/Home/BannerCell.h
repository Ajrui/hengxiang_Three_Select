//
//  BannerCell.h
//  GroupDeal
//
//  Created by lewang on 14-7-6.
//  Copyright (c) 2014年 groupdeal. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface BannerCell : UICollectionViewCell

@property(nonatomic, strong)IBOutlet UIImageView *imvMain;
@property(nonatomic, strong)IBOutlet UIActivityIndicatorView  *actView;

-(void)FillDataWith:(NSURL *)url;

@end

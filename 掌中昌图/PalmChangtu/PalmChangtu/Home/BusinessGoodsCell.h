//
//  BusinessGoodsCell.h
//  PalmChangtu
//
//  Created by osoons on 15/6/15.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusinessGoodsCell : UITableViewCell
/***产品图片**/
@property (weak, nonatomic) IBOutlet UIImageView *imaegProdect;
/**
 * 产品title
 */
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
/***产品介绍**/
@property (weak, nonatomic) IBOutlet UILabel *labDescription;
/***产品价格**/
@property (weak, nonatomic) IBOutlet UILabel *labPrice;
/***产品删除**/
@property (weak, nonatomic) IBOutlet UIButton *btnDelteAction;
/***销售状态**/

@property (weak, nonatomic) IBOutlet UILabel *lanScale;
@end

//
//  SerchViewController.m
//  PalmChangtu
//
//  Created by osoons on 15/6/23.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "SerchViewController.h"

@interface SerchViewController ()

@end

@implementation SerchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_v_Top.lalTitel setText:@"搜索"];

    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    [_textSearch becomeFirstResponder];

    [_v_Top.btnGoback removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
        [_v_Top.btnGoback addTarget:self action:@selector(btnBack:) forControlEvents:UIControlEventTouchUpInside];
    [_btnCancleSearch removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnCancleSearch addTarget:self action:@selector(btnBack:) forControlEvents:UIControlEventTouchUpInside];
    

    
    // Do any additional setup after loading the view from its nib.
}
-(void)btnBack:(UIButton *)btnback
{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

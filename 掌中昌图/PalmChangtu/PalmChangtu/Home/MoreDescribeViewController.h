//
//  MoreDescribeViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/16.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface MoreDescribeViewController : UIViewController
/**
 * 顶上的UITopView
 */
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
/**
 * 更多描述
 * 更多售后服务详情
 */
@property (strong,nonatomic) NSString * strLabTitle;

/**
 * 更多描述
 * 更多售后服务详情
 *  两者内容
 */
@property (strong,nonatomic) NSString * strCoentText;

@property (strong, nonatomic) IBOutlet UITextView *textview;

@end

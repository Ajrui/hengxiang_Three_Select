//
//  MainHomeViewController.h
//  E_Supermarket_Slient
//
//  Created by osoons on 15/6/11.
//  Copyright (c) 2015年 E_Supermarket_Slient. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
#import "ViewController.h"
#import "FPPopoverController.h"
#import "CollectionViewController.h"
#import "KRLCollectionViewGridLayout.h"
#import "ClassSerchViewController.h"
@interface HomeViewController : UIViewController<UIScrollViewDelegate,UIScrollViewAccessibilityDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UITextFieldDelegate,KeyWordSearchDelegate,FPPopoverControllerDelegate,CheckSelectTypeDelegate,UISearchBarDelegate>


@property (strong,nonatomic) IBOutlet UITableView * tableView;
/**
 *  
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrHistoryData;
/**
 *
 * 所有推荐arrAllTypeData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrRemandData;
/**
 *
 * 类型选择的值 数据元
 */
@property (strong,nonatomic) NSString * strType;

    /**
     * 顶上的UITopView
     */
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;



/**
 * 菜单选择控件
 * MenuCollectionView
 */

@property (strong, nonatomic) IBOutlet UICollectionView *MenuCollectionView;
/**
 * 菜单选择控件
 * arrayMenuData 数据源
 */

@property(strong,nonatomic) NSMutableArray *arrayMenuData;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;

/**
*
* 左侧滑动
*/
@property (weak, nonatomic) IBOutlet UIButton *btnLeftSlide;
//滚动的ScrollView多张图片UIScrollView
@property (strong, nonatomic) IBOutlet UICollectionView *RollingScrollView;

/**
 *
 * 随着图片滚动文字信息跟着改变UILabel
 */
@property (strong, nonatomic) IBOutlet UILabel *RollingText;
/**
 *
 * 滚动的点UIPageControl
 */
@property (strong, nonatomic) IBOutlet UIPageControl *RollingPagePoint;
/**
 *  首页广告
 */
@property(strong,nonatomic) NSMutableArray *arrayBannerData;

@property (weak, nonatomic) IBOutlet UIButton *btnSearch;

/**嗖嗖字符串***/
@property (weak, nonatomic) IBOutlet UITextField *textSearch;


/**超市百货***/

- (IBAction)btnOneAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnOne;

@property (weak, nonatomic) IBOutlet UILabel *labOne;



/** ***/
/**镇内镇内送餐 ***/

- (IBAction)btnTwo:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnTwwo;

@property (weak, nonatomic) IBOutlet UILabel *labTwo;




/** 衣物洗涤 ***/
- (IBAction)btnThree:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnThree;

@property (weak, nonatomic) IBOutlet UILabel *labThree;



/**医药诊所 ***/
- (IBAction)btnFour:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnFour;

@property (weak, nonatomic) IBOutlet UILabel *labFour;



/** 手机数码 ***/
- (IBAction)btnFive:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnFive;

@property (weak, nonatomic) IBOutlet UILabel *labFive;



/** 电脑家电***/
- (IBAction)btnSix:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnSix;

@property (weak, nonatomic) IBOutlet UILabel *labSix;



/** 眼镜钟表 ***/
- (IBAction)btnSeven:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnSeven;

@property (weak, nonatomic) IBOutlet UILabel *labSeven;

@property (strong, nonatomic) IBOutlet UITextField *searchText;


/** ***/


- (IBAction)btnEhight:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnEhight;

@property (weak, nonatomic) IBOutlet UILabel *labEhight;




/**
 * 菜单选择控件 uibuttion
 * arrButtionData 数据源
 */

@property(strong,nonatomic) NSMutableArray *arrButtionData;
/**
 * 菜单选择控件 lab
 * arrLabData 数据源
 */

@property(strong,nonatomic) NSMutableArray *arrLabData;

/**
 * 菜单选择控件 lab
 * arrLabData 数据源
 */

@property(strong,nonatomic) NSMutableArray *arrLocalTypeData;
/***历史记录***/
@property (strong, nonatomic) IBOutlet UITableView *tabView;

@end

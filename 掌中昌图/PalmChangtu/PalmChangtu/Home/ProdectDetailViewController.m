//
//  ProdectDetailViewController.m
//  PalmChangtu
//
//  Created by shaorui on 15/6/16.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "ProdectDetailViewController.h"
#import "MoreDescribeViewController.h"
#import "SettlementViewController.h"
#import "CommentViewController.h"
#import "commentsCell.h"
#import "BaseUrlConfig.h"
#import "BannerCell.h"
#import "ShareUserModel.h"
#import "MainTabbarController.h"
#import "JSONKit.h"
#import "Common.h"
#import "LoadingView.h"
#import "NetWarningView.h"
#import "AFNetworking.h"
#import "BaseUrlConfig.h"

#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"

#import "all_commodity_TypeModel.h"
#import "localJSONModel.h"
#import "MTLJSONAdapter.h"

#import "GetRemandModle.h"

#import "ShangPinListModel.h"
#import "CDPImageCollectionView.h"

@interface ProdectDetailViewController ()<CDPImageCollectionViewDelegate>
{
    NSTimer  *timer_;
    NSInteger currentPage_;

    BOOL isRequest;

}

@property (strong, nonatomic) IBOutlet CDPImageCollectionView * imageCollectionView;

@end

@implementation ProdectDetailViewController

-(void)viewDidAppear:(BOOL)animated
{
    if (isRequest) {
        isRequest =NO;

        [self getCommodityDetailActionRequest];

    }


}
-(void)didSelectImageWithNumber:(NSInteger)number
{
    NSLog(@"%@",[NSNumber numberWithInteger:number]);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    isRequest = YES;
    [_v_Top.lalTitel setText:@"详情"];
    [_v_Top.lalTitel setHidden:NO];
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;

    [_v_Top.btnGoback removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];

    [_v_Top.btnGoback addTarget:self action:@selector(btnGoBack:) forControlEvents:UIControlEventTouchUpInside];


    [self.btnAddGouWuChe removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];

    [self.btnAddGouWuChe addTarget:self action:@selector(btnAddGouWuCheAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.btnLiJiGouMai removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];

    [self.btnLiJiGouMai addTarget:self action:@selector(btnLiJiGouMaiAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
















//    NSDictionary * AA1  = [NSDictionary dictionaryWithObjectsAndKeys:@"加盟会员",keyName,@"QQ20150616-1",KeyImageName, nil];
//    NSDictionary * AA2  = [NSDictionary dictionaryWithObjectsAndKeys:@"登录",keyName,@"SDWebImageDefault",KeyImageName, nil];
//    NSDictionary * AA3  = [NSDictionary dictionaryWithObjectsAndKeys:@"关于我们",keyName,@"关于我们",KeyImageName, nil];
//    NSDictionary * AA4  = [NSDictionary dictionaryWithObjectsAndKeys:@"在线反馈",keyName,@"QQ20150616-1",KeyImageName, nil];
//    NSDictionary * AA5  = [NSDictionary dictionaryWithObjectsAndKeys:@"通知消息",keyName,@"通知消息",KeyImageName, nil];
//    NSDictionary * AA6  = [NSDictionary dictionaryWithObjectsAndKeys:@"我的订单",keyName,@"QQ20150616-1",KeyImageName, nil];
//    
    [self SetBannerCollectionView];
    [self addCustomGestures];
    _arrayBannerData = [[NSMutableArray alloc]   init];



    
    _arrTextDescData = [[NSMutableArray alloc] init];
    
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}


-(void)btnGoBack:(UIButton *)goback

{


    [self dismissViewControllerAnimated:YES completion:^{

    }];

}
-(void)btnAddGouWuCheAction:(UIButton *)btnAddGouWuCheAc

{



    [self addShoppingCartActionRequest];


}

-(void)btnLiJiGouMaiAction:(UIButton *)btnLiJiGouMaiA
{


    SettlementViewController * SettlementVie =[[SettlementViewController alloc] initWithNibName:@"SettlementViewController" bundle:nil];
    SettlementVie.shangPingId  =[NSString  stringWithFormat:@"%@",self.strShangPingID];

    SettlementVie.arrData = self.arrYiJianData;
    SettlementVie.strYiJianGouMai  =[NSString  stringWithFormat:@"%@",@"strYiJianGouMai"];


    [self presentViewController:SettlementVie animated:YES completion:0];


    
}

#pragma mark - schedule tasks

-(void)SlideBanner:(NSTimer *)timer
{
    BOOL animate = YES;
    if([_arrayBannerData count]!=0)
        {
        
        currentPage_++;
        if (currentPage_ >= [_arrayBannerData count]) {
            currentPage_ = 0;
            animate = NO;
        }
        
        NSIndexPath *index = [NSIndexPath indexPathForRow:currentPage_ inSection:0];
        [_RollingScrollView scrollToItemAtIndexPath:index atScrollPosition:UICollectionViewScrollPositionNone animated:animate];
        
        }
    
}

- (void)startTimer {
    timer_ = [[NSTimer alloc] initWithFireDate:[NSDate dateWithTimeIntervalSinceNow:3.0] interval:3.0 target:self selector:@selector(SlideBanner:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer_ forMode:NSRunLoopCommonModes];
    
}

- (void)stopTimer {
    [timer_ invalidate];
    timer_ = nil;
}


#pragma mark - 布局collectionview

-(void)SetBannerCollectionView
{
    
        //user nib
    CGFloat widthFlow  = self.RollingScrollView.frame.size.width;
    CGFloat heightFlow = self.RollingScrollView.frame.size.height;
    
    UICollectionViewFlowLayout *flowOut = [[UICollectionViewFlowLayout alloc] init];
    [flowOut setItemSize:CGSizeMake(widthFlow, heightFlow)];
    
    [flowOut setMinimumLineSpacing:0.0];
    [flowOut setMinimumInteritemSpacing:0.0];
    [flowOut setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [self.RollingScrollView setCollectionViewLayout:flowOut];
    
        //user nib
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([BannerCell class]) bundle:nil];
    [self.RollingScrollView registerNib:nib forCellWithReuseIdentifier:[NSString stringWithFormat:@"%@", [BannerCell class]]];
    
        //user nib
    [self.RollingScrollView setDelegate:self];
    [self.RollingScrollView setDataSource:self];
    [self.RollingScrollView setShowsVerticalScrollIndicator:NO];
    [self.RollingScrollView setAlwaysBounceVertical:NO];
    [self.RollingScrollView setAlwaysBounceHorizontal:YES];
    [self.RollingScrollView setPagingEnabled:YES];
    
    
        //    TableView
        //user nib
    UINib *nibCell = [UINib nibWithNibName:NSStringFromClass([commentsCell class]) bundle:nil];
    [self.tableView registerNib:nibCell forCellReuseIdentifier:[NSString stringWithFormat:@"%@", [commentsCell class]]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    _arrData = [[NSMutableArray alloc] init];
    
    
}



#pragma mark -UICollectionView

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    if (collectionView==self.RollingScrollView) {
        return 1;
        
    }
    else
        return 0;
    
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if(collectionView == self.RollingScrollView)
        {
        return self.arrayBannerData.count;
        }
    
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
        //
    static NSString *cellIdent = @"BannerCell";
    UICollectionViewCell * cell;
    cell = (BannerCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdent forIndexPath:indexPath];
    
    
    if ([collectionView isEqual:_RollingScrollView])
        {
        
        if ([self.arrayBannerData count] != 0) {


            NSDictionary  * dic  = [_arrayBannerData objectAtIndex:indexPath.row];

            [_RollingPagePoint setCurrentPage:indexPath.row];
            
            _RollingText.text = [NSString stringWithFormat:@""];
            
            
//            [((BannerCell  *)cell ).imvMain setImage:[UIImage imageNamed:[[_arrayBannerData objectAtIndex:indexPath.row] objectForKey:KeyImageName]]];

        NSURL *urlImage = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",HostImage,[dic objectForKey:@"photoUrl"]]];

         [((BannerCell *)cell).imvMain  sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"SDWebImageDefault"]];

        }
        }
    return cell;
    
    
    
}
#pragma mark - UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (collectionView==self.RollingScrollView) {
        
    }
    
        //    BannerCell * cell = (BannerCell*)[collectionView cellForItemAtIndexPath:indexPath];
    
}




#pragma mark - standard tableview datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        // Return the number of sections.
    return 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 129;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([_arrData count])
        {
        return [_arrData count];
        }
    else
        {
        return 0;
        }
    
    
}


#pragma mark - Standard TableView delegates
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * identifier = @"commentsCell";
    commentsCell * Cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    Cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    
    if ([_arrData count]!=0) {
        
//        
//        NSDictionary * dic  = [_arrData objectAtIndex:indexPath.row];
//        
//        
//        NSData * data = [[NSData alloc] initWithBase64EncodedString:[dic objectForKey:@"goods_thumb"] options:NSDataBase64DecodingIgnoreUnknownCharacters];

        
            //        [Cell.imaegProdect sd_setImageWithURL:[NSURL URLWithString:] placeholderImage:[UIImage imageNamed:@"SDWebImageDefault"]];
               //        Cell.btnDelteAction.tag  = indexPath.row;
            //        [Cell.btnDelteAction removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
            //
            //        [Cell.btnDelteAction addTarget:self action:@selector(btnDelteAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    
    return Cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
    
}
- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated
{
    
      
    
}
-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return YES;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    
        //    if (scrollView == self.CollectionView) {
        //
        //    }
        //    else if(scrollView == self.tableView)
        //    {
        //            onevalues+=10;
        //            twovalues+=onevalues;
        //
        //            [self ShowGoodsRequest:_strMethond andNSinter:nil];
        //
        //    }
    
    
}

#pragma mark - 添加自定义的手势（若不自定义手势，不需要下面的代码）

- (void)addCustomGestures {
    /*
     *注意：
     *添加自定义手势时，必须设置UIGestureRecognizer的属性cancelsTouchesInView 和 delaysTouchesEnded 为NO,
     *否则影响地图内部的手势处理
     */
    UITapGestureRecognizer *singleTouch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleViewBaseInfoTouchTap:)];
    singleTouch.delegate = self;
    singleTouch.numberOfTapsRequired = 1;
    singleTouch.cancelsTouchesInView = NO;
    singleTouch.delaysTouchesEnded = NO;
    
    [self.singleTouch addGestureRecognizer:singleTouch];
    
    /*
     *注意：
     *添加自定义手势时，必须设置UIGestureRecognizer的属性cancelsTouchesInView 和 delaysTouchesEnded 为NO,
     *否则影响地图内部的手势处理
     */
    UITapGestureRecognizer *singleTouchScale = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTouchScaleTap:)];
    singleTouchScale.delegate = self;
    singleTouchScale.cancelsTouchesInView = NO;
    singleTouchScale.delaysTouchesEnded = NO;
    [singleTouchScale requireGestureRecognizerToFail:singleTouch];
    [self.singleTouchScale addGestureRecognizer:singleTouchScale];
    
    
    UITapGestureRecognizer *singleTouchGoods = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleTouchGoodsTap:)];
    singleTouchGoods.delegate = self;
    singleTouchGoods.cancelsTouchesInView = NO;
    singleTouchGoods.delaysTouchesEnded = NO;
    [singleTouchGoods requireGestureRecognizerToFail:singleTouchScale];
    [self.singleTouchGoods addGestureRecognizer:singleTouchGoods];
    
    
}

//基本信息
- (void)handlesingleViewBaseInfoTouchTap:(UITapGestureRecognizer *)handlesingleViewBaseInfo{
    
    
    MoreDescribeViewController * MoreDescribe =[[MoreDescribeViewController alloc] initWithNibName:@"MoreDescribeViewController" bundle:nil];
    MoreDescribe.strLabTitle = @"更多描述";
    MoreDescribe.strCoentText = self.textViewContt.text;


    [self presentViewController:MoreDescribe animated:YES
     
                     completion:^{
                         
                     }];

    
    NSLog(@"my handlesingleTouchTap");
}

//售后服务

- (void)handleSingleTouchScaleTap:(UITapGestureRecognizer *)handleSingleTouchScaleTap {
    /*
     *do something
     */
    
    MoreDescribeViewController * MoreDescribe =[[MoreDescribeViewController alloc] initWithNibName:@"MoreDescribeViewController" bundle:nil];
    MoreDescribe.strLabTitle = @"更多售后服务";
    MoreDescribe.strCoentText = self.textViewContt.text;

    
    
    [self presentViewController:MoreDescribe animated:YES
     
                     completion:^{
                         
                     }];
    NSLog(@"my handleSingleTouchScaleTap");
}
//更多评价
- (void)handlesingleTouchGoodsTap:(UITapGestureRecognizer *)handlesingleTouchGoodsTap {
    /*
     *do something
     */
    NSLog(@"my handlesingleTouchGoodsTap");
    
    CommentViewController * comment =[[CommentViewController alloc] initWithNibName:@"CommentViewController" bundle:nil];
    [self presentViewController:comment animated:YES completion:^{
        
    }];
    
    
    
}





#pragma mark - network request getCommodityDetailAction

-(void)getCommodityDetailActionRequest
{
    [[LoadingView shareLoadingView] show];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,getCommodityDetailAction];



    NSDictionary *params = nil;

    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];


    if ([self.strModelshangping isEqualToString:@"ClassSerchViewController"]) {
            params = @{

                                 @"commodityId":[NSString stringWithFormat:@"%@",_modelshangping.getRemandId],
                                 @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                                 @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId]
                                 
                                 };
    }
else
{


    params = @{

               @"commodityId":[NSString stringWithFormat:@"%@",_model.getRemandId],
               @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
               @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId]

               };

}





    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {


         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];

         NSDictionary * dicRespon = responseObject;
         [self GGRequestSuccessWith:dicRespon];

         [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [[LoadingView shareLoadingView] dismissAnimated:YES];
         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObject9WithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];

}

-(void)GGRequestSuccessWith:(NSDictionary *)dicRespon
{

    
    
    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
    {
//图片信息
        [_arrayBannerData removeAllObjects];
//文本信息
        [_arrTextDescData removeAllObjects];

        [_arrData removeAllObjects];


            NSError *error = nil;
        ShangPinListModel *modelTmp =
        [MTLJSONAdapter modelOfClass:[ShangPinListModel class] fromJSONDictionary:dicRespon error:&error];

        _arrayBannerData  = modelTmp.webCommodityPhotos;

        self.strShangPingID = modelTmp.getRemandId;

        self.labName.text = modelTmp.name;


        self.labPrice.text  =  [NSString stringWithFormat:@"¥%@",modelTmp.price];
        self.textViewContt.text = modelTmp.content;

        self.labTotalNum.text =  [NSString stringWithFormat:@"已售%@件",modelTmp.saleNo];
        self.labSacleSevrices.text  = modelTmp.afterSalesAddress;

        self.labHaoPin.text  =[NSString stringWithFormat:@"好评%@条",    [NSNumber numberWithInteger:modelTmp.webCommodityEvaluate.count]];
        self.labChaPinNUm.text  =[NSString stringWithFormat:@"好评%@条",    [NSNumber numberWithInteger:modelTmp.webCommodityEvaluate.count]];
        
        // _imageCollectionView为横向滚动
        self.imageCollectionView =[[CDPImageCollectionView alloc] initWithFrame:self.imageCollectionView.frame andImageUrlArr:self.arrayBannerData scrollDirection:UICollectionViewScrollDirectionHorizontal];
        
        
        //        [ _imageCollectionView openTheTimerAndSetTheDuration:2];
        
        self.imageCollectionView.strDetail  = @"strDetail";
        //代理方法获得点击第几张图片,如不需要可不设置
        self.imageCollectionView.delegate=self;
        [self.view addSubview: _imageCollectionView];

    
//content
        _arrData  = modelTmp.webCommodityEvaluate;
        _arrYiJianData = [[NSMutableArray alloc] initWithObjects:modelTmp, nil];
        [self.RollingScrollView reloadData];
        
        
        
        [self.tableView reloadData];

        

        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        
    }
    else
    {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
    }
    
}




#pragma mark - network request addShoppingCartAction

-(void)addShoppingCartActionRequest
{
    [[LoadingView shareLoadingView] show];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,addShoppingCartAction];



    NSDictionary *params = nil;

    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];


    if ([self.strModelshangping isEqualToString:@"ClassSerchViewController"]) {
        params = @{

                   @"commodityId":[NSString stringWithFormat:@"%@",_modelshangping.getRemandId],
                   @"commodityModel":[NSString stringWithFormat:@"%@",@"1"],
                   @"commodityColor":@"2",
                    @"commodityNo":@"1",
                   @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                   @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId],
                   @"userId":[NSString stringWithFormat:@"%@",shareUser.user.user_id]


                   };
    }
    else
    {


        params = @{

                   @"commodityId":[NSString stringWithFormat:@"%@",_model.getRemandId],
                   @"commodityModel":[NSString stringWithFormat:@"%@",@"1"],
                   @"commodityColor":@"2",
                   @"commodityNo":@"1",
                   @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                   @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId],
                   @"userId":[NSString stringWithFormat:@"%@",shareUser.user.user_id]


                   };

    }





    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {


         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];

         NSDictionary * dicRespon = responseObject;
         [self addShoppingCartActionSuccessWith:dicRespon];

         [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [[LoadingView shareLoadingView] dismissAnimated:YES];
         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObject9WithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];

}

-(void)addShoppingCartActionSuccessWith:(NSDictionary *)dicRespon
{



    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
    {
       
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        
    }
    else
    {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

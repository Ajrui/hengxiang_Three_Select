//
//  HistoryTabCell.h
//  PalmChangtu
//
//  Created by osoons on 15/7/24.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTabCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labHistory;
@property (strong, nonatomic) IBOutlet UIButton * btnDelteHistoryAction;
@end

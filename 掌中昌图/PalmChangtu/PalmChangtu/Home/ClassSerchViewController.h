//
//  SerchClassViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/22.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BATableViewKit/BATableView.h"
#import "localJSONModel.h"
#import "UITopBanner.h"

@protocol KeyWordSearchDelegate <NSObject>
@optional
-(void)userSearchTextString:(NSString *)str;

@end
@interface ClassSerchViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate,KeyWordSearchDelegate,BATableViewDelegate,UITextFieldDelegate>


@property (nonatomic, strong) IBOutlet BATableView *contactTableView;
/**
 * 品牌类型
 **/
@property (nonatomic, strong) NSMutableArray * dataPinPaiiSource;

@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) NSMutableArray
 * dataTileSource;

@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
@property (strong,nonatomic) IBOutlet UITableView * tabView;

@property (strong,nonatomic) IBOutlet UITableView * tableView;
/**
 * 传过来的typeid
 **/
@property (strong,nonatomic) NSString * strType;
/**
 * 传过来的typeid
 **/
@property (strong,nonatomic) localJSONModel * mdoelType;




/**
 *
 * arrData 数据元
 * 商品列表的数据
 */
@property (strong,nonatomic) NSMutableArray * arrData;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;

/**
 * 品牌
 */
@property (strong, nonatomic) IBOutlet UIButton *btnPinPai;
@property (strong, nonatomic) IBOutlet UIImageView *imagePinPai;

/**
 *
 * 销量
 */
@property (strong, nonatomic) IBOutlet UIButton *btnXiaoLiang;
@property (strong, nonatomic) IBOutlet UIImageView *imageXiaoLiang;
@property (strong, nonatomic) IBOutlet UIImageView *imagePXXiaoLiang;

/**
 * 价格
 */
@property (strong, nonatomic) IBOutlet UIButton *btnJiaGe;
/**
 * 价格图片
 */
@property (strong, nonatomic) IBOutlet UIImageView *imageJiaGe;

@property (strong, nonatomic) IBOutlet UIImageView *imagePXJiaGe;

/**
 * 评价
 */
@property (strong, nonatomic) IBOutlet UIButton *btnPingJia;
@property (strong, nonatomic) IBOutlet UIImageView *imagePingJia;
/**
 * 排序评价
 */
@property (strong, nonatomic) IBOutlet UIImageView *imagePXPingJia;

/**
 * 选择的ID
 **/
@property (strong,nonatomic) NSString * oriderTypeId;
/**
 *
 *  显示搜索view
 *
 **/
@property (strong, nonatomic) IBOutlet UIView *searchVivew;


@property (weak, nonatomic) IBOutlet UIButton *btnCancleSearch;

/**嗖嗖字符串***/
@property (weak, nonatomic) IBOutlet UITextField *textSearch;






@end

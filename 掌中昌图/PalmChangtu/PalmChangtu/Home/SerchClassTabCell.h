//
//  SerchClassTabCell.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/22.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SerchClassTabCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *image;

@property (strong, nonatomic) IBOutlet UILabel *labName;
@property (strong, nonatomic) IBOutlet UILabel *labContent;
@property (strong, nonatomic) IBOutlet UILabel *labPrice;

@property (strong, nonatomic) IBOutlet UILabel *labScale;
@property (strong, nonatomic) IBOutlet UILabel *labScore;

@end

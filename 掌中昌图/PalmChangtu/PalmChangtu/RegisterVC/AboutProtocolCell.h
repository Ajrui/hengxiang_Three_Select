//
//  AboutProtocolCell.h
//  PalmChangtu
//
//  Created by osoons on 15/6/25.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutProtocolCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labName;

@end

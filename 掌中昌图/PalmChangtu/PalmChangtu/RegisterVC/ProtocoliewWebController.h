//
//  ProtocoliewWebController.h
//  PalmChangtu
//
//  Created by osoons on 15/6/24.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface ProtocoliewWebController : UIViewController<UIWebViewDelegate>
/**
 * 自定义顶部的UI
 */
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

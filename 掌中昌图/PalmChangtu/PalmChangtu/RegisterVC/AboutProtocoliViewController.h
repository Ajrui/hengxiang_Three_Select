//
//  AboutProtocoliViewController.h
//  PalmChangtu
//
//  Created by osoons on 15/6/25.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface AboutProtocoliViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
/**
 * 自定义顶部的UI
 */
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic) NSMutableArray *arrayData;

@end

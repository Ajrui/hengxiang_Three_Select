//
//  UserRegisterViewController.h
//  PalmChangtu
//
//  Created by osoons on 15/6/23.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface UserRegisterViewController : UIViewController<UITextFieldDelegate>
/**
 * 自定义顶部的UI
 */
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
/**
 * 昵称
 **/
@property (weak, nonatomic) IBOutlet UITextField *textNickName;
/**
 * 电话号码
 **/
@property (weak, nonatomic) IBOutlet UITextField *textPhoneNum;
/**
 * 输入密码
 **/
@property (weak, nonatomic) IBOutlet UITextField *textPassWord;
/**
 * 再次确认密码
 **/
@property (weak, nonatomic) IBOutlet UITextField *textAlingPasss;

@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
/**
 * 协议
 **/
@property (weak, nonatomic) IBOutlet UIButton *btnProtocol;
/**
 * 1买家 2商家
 **/

@property (weak, nonatomic) NSString * string;
//收到的验证码

@property (strong, nonatomic) IBOutlet UITextField *textVerayCode;
@property (weak, nonatomic) IBOutlet UIButton *btnVerification;

//得到的验证码
@property (strong, nonatomic) NSString * strCode;

@end

//
//  ProtocoliewWebController.m
//  PalmChangtu
//
//  Created by osoons on 15/6/24.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "ProtocoliewWebController.h"
#import "MainTabbarController.h"
#import "BaseUrlConfig.h"
#import "JSONKit.h"
#import "Common.h"
#import "LoadingView.h"
#import "NetWarningView.h"
#import "AFNetworking.h"
#import "BaseUrlConfig.h"
#import "ShareUserModel.h"
@interface ProtocoliewWebController ()

@end

@implementation ProtocoliewWebController
-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self RequestRec];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [_v_Top.lalTitel setText:@"协协议声明"];
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    [_v_Top.btnGoback addTarget:self action:@selector(btnBack:) forControlEvents:UIControlEventTouchUpInside];
    

    NSURL *  url =[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",HostServer,register_commit]];
    
    
    [_webView setDelegate:self];
    NSURLRequest *request=[[NSURLRequest alloc] initWithURL:url];
    [self.webView loadRequest:request];
    
    [self.webView setScalesPageToFit:YES];

    


    
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)btnBack:(UIButton *)back
{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
//    添加指示器
        UIActivityIndicatorView *aiv = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        aiv.tag = 1000;
        aiv.frame = CGRectMake((_webView.frame.size.width-20)/2, (_webView.frame.size.height-20)/2, 20, 20);
        [aiv startAnimating];
        [_webView addSubview:aiv];
    

}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
            // finished loading, hide the activity indicator in the status bar
        UIActivityIndicatorView *aiv = (UIActivityIndicatorView*)[_webView viewWithTag:1000];
        [aiv stopAnimating];
        aiv = nil;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    UIActivityIndicatorView *aiv = (UIActivityIndicatorView*)[_webView viewWithTag:1000];
    [aiv stopAnimating];
    aiv = nil;
}

#pragma mark - network request

-(void)RequestRec
{
    [[LoadingView shareLoadingView] show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,statementAaction];
    
    
    
    [manager POST:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         
         NSDictionary * dicRespon = responseObject;

         NSString * strJson = [dicRespon JSONString];
         [self.webView loadHTMLString:strJson baseURL:nil];
         
         
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [[LoadingView shareLoadingView] dismissAnimated:YES];
         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

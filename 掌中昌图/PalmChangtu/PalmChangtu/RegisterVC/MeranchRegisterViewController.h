//
//  RegisterViewController.h
//  E_Supermarket_ Merchants
//
//  Created by osoons on 15/6/1.
//  Copyright (c) 2015年 E_Supermarket_ Merchants. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BouthLineView.h"
#import "UITopBanner.h"
@interface MeranchRegisterViewController : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate>


/**
 * 自定义顶部的UI
 */
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;

@property (strong,nonatomic)  NSString * VerificationString;

/**申请账号**/

@property (strong, nonatomic) IBOutlet UITextField *textAccount;


/**申请人姓名:**/

@property (strong, nonatomic) IBOutlet UITextField *textName;
/**证件号码**/
@property (strong, nonatomic) IBOutlet UITextField *textIDCard;

/**所属分类:**/
@property (strong, nonatomic) IBOutlet BouthLineView *viewType;


/**商家介绍**/

@property (strong, nonatomic) IBOutlet UITextField *textDescription;


/**
 * 注册手机账号登录
 */
@property (strong,nonatomic) IBOutlet UITextField * textPhoneNum;

/**填写地址**/

@property (strong, nonatomic) IBOutlet UITextField *textAddress;

/**设置密码 **/
@property (strong,nonatomic) IBOutlet UITextField * textSetPassword;
/**再次设置密码 **/
@property (strong,nonatomic) IBOutlet UITextField * textSetAlginPassword;
/** 验证码 **/
@property (strong,nonatomic) IBOutlet UITextField * textVerificationCode;
/**提交注册按钮**/
@property (strong,nonatomic) IBOutlet UIButton * btnRegister;
/** 获取验证码 **/
@property (strong,nonatomic) IBOutlet UIButton * btnVerification;
/***/



@end

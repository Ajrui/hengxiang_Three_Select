//
//  AboutProtocoliViewController.m
//  PalmChangtu
//
//  Created by osoons on 15/6/25.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "AboutProtocoliViewController.h"
#import "AugreementWebViewController.h"

#import "AboutProtocolCell.h"
@interface AboutProtocoliViewController ()

@end

@implementation AboutProtocoliViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_v_Top.lalTitel setText:@"相关协议"];
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    
    
    
    NSDictionary * dic0 = @{
                            @"name":@"用户注册协议",
                            @"type":@"1"
                           };
    NSDictionary * dic1 = @{
                            @"name":@"商家注册协议",
                            @"type":@"2"
                           };
    NSDictionary * dic2 = @{
                            @"name":@"商家相关协议",
                            @"type":@"3"
                           };
    NSDictionary * dic3 = @{
                            @"name":@"关于我们",
                            @"type":@"4"
                           };
    
    
    _arrayData =  [[NSMutableArray alloc] initWithObjects:dic0,dic1,dic2,dic3, nil];
    
    //    TableView
    //user nib
    UINib *nibCell = [UINib nibWithNibName:NSStringFromClass([AboutProtocolCell class]) bundle:nil];
    [self.tableView registerNib:nibCell forCellReuseIdentifier:[NSString stringWithFormat:@"%@", [AboutProtocolCell class]]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;

}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  _arrayData.count;
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString * identifier = @"AboutProtocolCell";
    AboutProtocolCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    NSDictionary * dic = [_arrayData objectAtIndex:indexPath.row];
    
    
    cell.labName.text = [dic objectForKey:@"name"];
    return  cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    AugreementWebViewController * AugreementView = [[AugreementWebViewController alloc] initWithNibName:@"AugreementWebViewController" bundle:nil];
    NSDictionary * dic = [_arrayData objectAtIndex:indexPath.row];
    
    

    AugreementView.strTypeName = [dic objectForKey:@"name"];
    AugreementView.strTypeID = [dic objectForKey:@"type"];

    [self.navigationController pushViewController:AugreementView animated:YES];
//    [self presentViewController:AugreementView animated:YES completion:NULL];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

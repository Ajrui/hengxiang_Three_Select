//
//  AugreementWebViewController.h
//  PalmChangtu
//
//  Created by osoons on 15/6/25.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface AugreementWebViewController : UIViewController<UIWebViewDelegate>
/**
 * 自定义顶部的UI
 */
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
/***name协议类型***/
@property (strong, nonatomic) NSString * strTypeID;

/***name协议名字***/
@property (strong, nonatomic) NSString * strTypeName;
@end

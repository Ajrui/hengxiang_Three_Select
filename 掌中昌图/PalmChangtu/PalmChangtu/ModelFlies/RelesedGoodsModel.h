//
//  RelesedGoodsModel.h
//  PalmChangtu
//
//  Created by osoons on 15/7/16.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
@interface RelesedGoodsModel : MTLModel<MTLJSONSerializing>

/*

 说明	英文名	类型

 说明	英文名	类型
 数据集合	arr	见下表
 */
/**
 *   创建时间	createTime	date
 *   createTime
 **/
@property (strong,nonatomic) NSString *createTime;

/**
 *   更新时间	updateTime	date
 * updateTime
 **/
@property (strong,nonatomic) NSString *updateTime;

/**
 *  是否删除	isDelete	int
 * isDelete
 **/
@property (strong,nonatomic) NSNumber *isDelete;

/**
 *  是否有效	isValid	int
 * isValid
 **/
@property (strong,nonatomic) NSNumber *isValid;

/**
 *  商品类型	type	varchar
 * type
 **/
@property (strong,nonatomic) NSString *type;

/**
 *  商家名称		varchar
 *  sellerName
 **/
@property (strong,nonatomic) NSString *message;
/**
 *  商家名称		varchar
 *  sellerName
 **/
@property (strong,nonatomic) NSString *sellerName;

/**
 *  销售数	saleNo	int
 *  saleNo
 **/
@property (strong,nonatomic) NSNumber * saleNo;

/**
 *  评价次数	evaluateNo	int
 *  evaluateNo
 **/
@property (strong,nonatomic) NSNumber * evaluateNo;
/**
 *  评价分数	evaluateScore	int
 * evaluateScore
 **/
@property (strong,nonatomic) NSNumber * evaluateScore;
/**
 *   内容描述	content	varchar、
 *  content
 **/
@property (strong,nonatomic) NSString *content;;

/**
 *  商品id
 *
 **/
@property (strong,nonatomic) NSString * relesedgoodsID;
/**
 *  商家userId	userId	varchar
 * userId
 **/
@property (strong,nonatomic) NSString * userId;
/**
 *  商品名称	name	varchar
 *  name
 **/
@property (strong,nonatomic) NSString *name;
/**
 *  图片路径	photoUrl	varchar
 *  商品展览图片 photoUrl
 **/
@property (strong,nonatomic) NSString * photoUrl;
/**
 *  价格	price	varchar
 *  price   商品显示价格
 **/
@property (strong,nonatomic) NSString * price;

/**
 *  售后地址	afterSalesAddress	varchar
 *  afterSalesAddress  售后地址
 **/
@property (strong,nonatomic) NSString * afterSalesAddress;
@property (strong,nonatomic) NSString *evaluateLevel;
@property (strong,nonatomic) NSString *evaluateStatus;
@property (strong,nonatomic) NSString *commodityColor;
@property (strong,nonatomic) NSString *commodityModel;
/***commodityId商品id***/
@property (strong,nonatomic) NSString *commodityId;

@property (strong,nonatomic) NSString *commodityNo;

@property (strong,nonatomic) NSString *evaluateContent;

@property (strong,nonatomic) NSString *orderId;
/**
 *  商家userId
 * 商家userId
 **/
@property (strong,nonatomic) NSString *sellerUserId;
@end

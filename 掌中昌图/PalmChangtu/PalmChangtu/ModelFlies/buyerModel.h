//
//  buyerModel.h
//  PalmChangtu
//
//  Created by osoons on 15/6/23.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
@interface buyerModel : MTLModel<MTLJSONSerializing>
//字段名	类型	长度	是否允许null
//Id	id	varchar	50	N
//用户id	userId	varchar	50	N
//收货电话	takeOverPhone	varchar	11	Y
//是否推送信息	isPush	int	11	Y
//创建时间	createTime	date		N
//更新时间	updateTime	date		N
/**
 * //Id	id  byerId
 **/
@property (assign,nonatomic) NSNumber *    byerId;

/**
 * 用户id	userId
 **/
@property (assign,nonatomic) NSNumber *    userId;

/**
 * 收货电话
 **/
@property (strong,nonatomic) NSString *     takeOverPhone;

/**
 * 是否推送信息	isPush
 **/
@property (assign,nonatomic) NSNumber *    isPush;
/**
 * 创建时间
 **/
@property (strong,nonatomic) NSString *     createTime;
/**
 * 更新时间
 **/
@property (strong,nonatomic) NSString *     updateTime;


@end

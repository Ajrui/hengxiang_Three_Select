//
//  GetRemandModle.h
//  PalmChangtu
//
//  Created by osoons on 15/6/29.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "MTLModel.h"

#import "MTLJSONAdapter.h"
@interface GetRemandModle : MTLModel<MTLJSONSerializing>



/*
 
 说明	英文名	类型

 说明	英文名	类型
 数据集合	arr	见下表
 */

/**
 *  推荐商品类型id
 *
 **/
@property (strong,nonatomic) NSString * getRemandId;
/**
 *  商家userId	userId	varchar
 * userId
 **/
@property (strong,nonatomic) NSString * userId;
/**
 *  商品名称	name	varchar
 *  name
 **/
@property (strong,nonatomic) NSString *name;
/**
 *  图片路径	photoUrl	varchar
 *  商品展览图片 photoUrl
 **/
@property (strong,nonatomic) NSString * photoUrl;
/**
 *  价格	price	varchar
 *  price   商品显示价格
 **/
@property (strong,nonatomic) NSString * price;

/**
 *  售后地址	afterSalesAddress	varchar
 *  afterSalesAddress  售后地址
 **/
@property (strong,nonatomic) NSString * afterSalesAddress;
/**
 *   内容描述	content	varchar、
 *  content
 **/
@property (strong,nonatomic) NSString *content;;
/**
 *  评价次数	evaluateNo	int
 *  evaluateNo
 **/
@property (strong,nonatomic) NSNumber * evaluateNo;
/**
 *  评价分数	evaluateScore	int
 * evaluateScore
 **/
@property (strong,nonatomic) NSNumber * evaluateScore;
/**
 *  销售数	saleNo	int
 *  saleNo
 **/
@property (strong,nonatomic) NSNumber * saleNo;
/**
 *  商家名称		varchar
 *  sellerName
 **/
@property (strong,nonatomic) NSString *sellerName;
/**
 *  商品类型	type	varchar
 * type
 **/
@property (strong,nonatomic) NSString *type;

/**
 *  是否有效	isValid	int
 * isValid
 **/
@property (strong,nonatomic) NSNumber *isValid;

/**
 *  是否删除	isDelete	int
 * isDelete
 **/
@property (strong,nonatomic) NSNumber *isDelete;

/**
 *   创建时间	createTime	date
 *   createTime
 **/
@property (strong,nonatomic) NSString *createTime;

/**
 *   更新时间	updateTime	date
 * updateTime
 **/
@property (strong,nonatomic) NSString *updateTime;


/**
 *   是否成功	success	boolean

 *   success
 **/
@property (strong,nonatomic) NSString *success;

/**
 *   消息	message	varchar
 * message
 **/
@property (strong,nonatomic) NSString *message;
/**
 *   数据集合
 * arr
 **/
@property (strong,nonatomic) NSMutableArray *arrRemandData;






@end

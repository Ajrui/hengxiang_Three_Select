//
//  localJSONModel.h
//  PalmChangtu
//
//  Created by osoons on 15/6/26.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
@interface localJSONModel : MTLModel<MTLJSONSerializing>

/*
 "createTime": "2015-06-25",
 "id": "1",
 "isTop": "0",
 "name": "超市百货",
 "orderNo": "1",
 "parentId": "0",
 "photoUrl": "0",
 "updateTime": "2015-06-25"
 */

/**
 * 创建时间
 **/
@property (strong,nonatomic) NSString *     createTime;
/**
 * //Id	id  localTypeID 本地类型id
 **/
@property (assign,nonatomic) NSNumber *    localTypeID;

/**
 * 是否置顶
 **/
@property (assign,nonatomic) NSNumber *    isTop;

/**
 *  "name": "超市百货",
 *  分类类型
 **/
@property (strong,nonatomic) NSString *     name;



/**
 * 顺序id
 **/
@property (assign,nonatomic) NSNumber *    orderNo;

/**
 * parentId id
 **/
@property (assign,nonatomic) NSNumber *    parentId;
/**
 * photoUrl 图片
 **/
@property (assign,nonatomic) NSString *    photoUrl;

/**
 * 更新时间
 **/
@property (strong,nonatomic) NSString *     updateTime;


@end

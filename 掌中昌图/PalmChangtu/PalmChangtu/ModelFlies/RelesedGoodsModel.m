//
//  RelesedGoodsModel.m
//  PalmChangtu
//
//  Created by osoons on 15/7/16.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "RelesedGoodsModel.h"

@implementation RelesedGoodsModel
+(NSDictionary *)JSONKeyPathsByPropertyKey
{


    return @{
             @"updateTime":@"updateTime",
             @"createTime":@"createTime",
             @"message":@"message",
             @"isDelete":@"isDelete",
             @"isValid":@"isValid",
             @"type":@"type",
             @"sellerName":@"sellerName",
             @"saleNo":@"saleNo",
             @"evaluateScore":@"evaluateScore",
             @"evaluateNo":@"evaluateNo",
             @"content":@"content",
             @"afterSalesAddress":@"afterSalesAddress",
             @"price":@"price",
             @"photoUrl":@"photoUrl",
             @"name":@"name",
             @"userId":@"userId",
             @"relesedgoodsID":@"id",
             @"commodityColor":@"commodityColor",
             @"commodityId":@"commodityId",
             @"commodityModel":@"commodityModel",
            @"commodityNo":@"commodityNo",
             @"evaluateContent":@"evaluateContent",
             @"evaluateLevel":@"evaluateLevel",
             @"evaluateStatus":@"evaluateStatus",
             @"orderId":@"orderId",
             @"sellerUserId":@"sellerUserId"

             
             };
}

@end

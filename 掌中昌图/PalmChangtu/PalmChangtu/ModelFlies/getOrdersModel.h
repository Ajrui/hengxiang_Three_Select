//
//  getOrdersModel.h
//  PalmChangtu
//
//  Created by osoons on 15/7/17.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
@interface getOrdersModel : MTLModel<MTLJSONSerializing>


/***地址id***/

@property (atomic,assign) NSString * addressId;
/***buyTime 购买时间***/

@property (atomic,assign) NSString * buyTime;
/**
 *   内容描述	content	varchar、
 *  content
 **/
@property (strong,nonatomic) NSString *content;

/**
 *   创建时间	createTime	date
 *   createTime
 **/
@property (strong,nonatomic) NSString *createTime;

/**
 * 运费 freight
 * 运费
 **/
@property (strong,nonatomic) NSString *freight;

/**
 * id
 * 得到的id
 **/
@property (strong,nonatomic) NSString *GetOrderIdd;
/***商品总数量***/
@property (strong,nonatomic) NSString *number;
/**
 *订单商品
 *                    {
 afterSalesAddress = "";
 commodityColor = "";
 commodityId = "";
 commodityModel = "";
 commodityNo = 1;
 content = "";
 createTime = "2015-07-10";
 evaluateContent = "";
 evaluateLevel = 0;
 evaluateNo = "";
 evaluateScore = "";
 evaluateStatus = 0;
 id = "14f90feb-5be8-4c34-bf60-0838fa04f319";
 name = "\U7535\U8111";
 orderId = "afefc96b-65a3-45fd-86dd-5ee157a0bb49";
 photoUrl = "c3b39637-386e-4db5-89ef-aed5881dc732_0.jpg";
 price = 25;
 saleNo = "";
 sellerUserId = "";
 type = 9;
 updateTime = "2015-07-10";
 userId = 20150708021323000006;
 }
 **/
@property (strong,nonatomic) NSMutableArray * orderCommodity;
/**
 * 订单状态
 **/
@property (strong,nonatomic) NSMutableArray * orderStatus;
/**
 *  价格	price	varchar
 *  price   商品显示价格
 **/
@property (strong,nonatomic) NSString * price;
/**
 *  商家商家id sellerUserId		varchar
 *  sellerUserId
 **/
@property (strong,nonatomic) NSString *sellerUserId;

/**
 * 收货时间 takeOverTime		varchar
 *  takeOverTime
 **/
@property (strong,nonatomic) NSString *takeOverTime;

/**
 *   更新时间	updateTime	date
 * updateTime
 **/
@property (strong,nonatomic) NSString *updateTime;
/**
 *  商家userId	userId	varchar
 * userId
 **/
@property (strong,nonatomic) NSString * userId;

/**
 *  商家快递 phone	varchar
 * phone
 **/
@property (strong,nonatomic) NSString *phone;
///**
// *  商家name	name	varchar
// * name
// **/
//@property (strong,nonatomic) NSString *name;

@end

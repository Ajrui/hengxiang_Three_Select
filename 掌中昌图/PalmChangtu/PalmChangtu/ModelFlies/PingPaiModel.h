//
//  PingPaiModel.h
//  PalmChangtu
//
//  Created by osoons on 15/7/13.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "MTLModel.h"

#import "MTLJSONAdapter.h"
@interface PingPaiModel : MTLModel<MTLJSONSerializing>
/**
 *  商品类型id
 *
 **/
@property (strong,nonatomic) NSString * commodityTypeId;
/**
 *  品牌名称
 *  品牌名称
 **/
@property (strong,nonatomic) NSString * brandName;
/**
 *  modelid
 **/
@property (strong,nonatomic) NSString *modelid;
/**
 *  createTime
 *  createTime
 **/
@property (strong,nonatomic) NSString * createTime;
/**
 *  updateTime
 *  updateTime
 **/
@property (strong,nonatomic) NSString * updateTime;

@end

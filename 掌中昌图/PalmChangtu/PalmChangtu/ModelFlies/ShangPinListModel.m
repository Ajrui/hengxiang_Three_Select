//
//  ShangPinListModel.m
//  PalmChangtu
//
//  Created by osoons on 15/7/13.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//
//21.获取商品列表

#import "ShangPinListModel.h"

@implementation ShangPinListModel
+(NSDictionary *)JSONKeyPathsByPropertyKey
{


    return @{
             @"updateTime":@"updateTime",
             @"createTime":@"createTime",
                @"message":@"message",
             @"isDelete":@"isDelete",
             @"isValid":@"isValid",
             @"type":@"type",
             @"sellerName":@"sellerName",
             @"saleNo":@"saleNo",
             @"evaluateScore":@"evaluateScore",
             @"evaluateNo":@"evaluateNo",
             @"content":@"content",
             @"afterSalesAddress":@"afterSalesAddress",

             @"price":@"price",

             @"photoUrl":@"photoUrl",
             @"name":@"name",
             @"userId":@"userId",
             @"getRemandId":@"id",
            @"webCommodityPhotos":@"webCommodityPhotos",
             @"webCommoditySubdivision":@"webCommoditySubdivision",
             @"webCommodityEvaluate":@"webCommodityEvaluate"


             };
}

@end

//
//  buyer_delivery_infoModel.h
//  PalmChangtu
//
//  Created by osoons on 15/6/23.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
@interface buyer_delivery_infoModel : MTLModel<MTLJSONSerializing>
//买家收货信息
//字段名	类型	长度	是否允许null
//Id	id	varchar	50	N
//用户id	userId	varchar	50	N
//姓名	receiverName	varchar	50	N
//电话	receiverPhone	varchar	50	N
//区县	receiverDistrict	varchar	50	N
//收货地址	receiverAddress	varchar	120	Y
//是否默认	isDefault	Int	1	N
//创建时间	createTime	date		N

/**
 * //Id	id  buyer_deliveryId 买家发货信息id
 **/
@property (assign,nonatomic) NSNumber *    buyer_deliveryId;

/**
 * 用户id	userId
 **/
@property (assign,nonatomic) NSNumber *    userId;

/**
 * 姓名
 **/
@property (strong,nonatomic) NSString *     receiverName;

/**
 * 电话
 **/
@property (strong,nonatomic) NSString *     receiverPhone;
/**
 * 区县
 **/
@property (strong,nonatomic) NSString *     receiverDistrict;
/**
 * 收货地址
 **/
@property (strong,nonatomic) NSString *     receiverAddress;
/**
 * 是否默认
 **/
@property (assign,nonatomic) NSNumber *    isDefault;
/**
 * 创建时间
 **/
@property (strong,nonatomic) NSString *     createTime;
///**
// * 更新时间
// **/
//@property (strong,nonatomic) NSString *     updateTime;


@end

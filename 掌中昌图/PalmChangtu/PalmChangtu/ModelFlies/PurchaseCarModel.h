//
//  PurchaseCarModel.h
//  PalmChangtu
//
//  Created by shaorui on 15/7/15.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"
@interface PurchaseCarModel : MTLModel<MTLJSONSerializing>

/**
 *  售后地址	afterSalesAddress	varchar
 *  afterSalesAddress  售后地址
 **/
@property (strong,nonatomic) NSString * afterSalesAddress;

/**
 *  商品颜色	commodityColor	varchar
 *  commodityColor  商品颜色
 **/
@property (strong,nonatomic) NSString * commodityColor;


/**
 *  商品id
 *	commodityId	varchar
 *  commodityId  商品id

 **/
@property (strong,nonatomic) NSString * commodityId;

/**
 *  商品型号
 *	commodityModel	varchar
 *  commodityModel  商品型号
 **/
@property (strong,nonatomic) NSString * commodityModel;

/**
 *  商品数量
 *	commodityNo	varchar
 *  commodityNo  商品数量
 **/
@property (strong,nonatomic) NSString * commodityNo;

/**
*   创建时间	createTime	date
*   createTime
**/
@property (strong,nonatomic) NSString *createTime;

/**
 *   更新时间	updateTime	date
 * updateTime
 **/
@property (strong,nonatomic) NSString *updateTime;




/**
 *  评价描述	evaluateContent	int
 * 评价描述
 **/
@property (strong,nonatomic) NSString *evaluateContent;


/**
 *  评价状态	evaluateStatus	int
 * evaluateLevel

 **/


@property (strong,nonatomic) NSNumber *evaluateStatus;


/**
 *  评价状态	evaluateLevel	int
 * evaluateLevel
 * 评价等级
 **/
@property (strong,nonatomic) NSNumber *evaluateLevel;

/**
 *  数据集合购物id	id	int
 * id
 * 数据集合 购物id
 **/
@property (strong,nonatomic) NSString *Purchaseid;


/**
 *  商品名称 是否成功	name	varchar
 *  name
 **/
@property (strong,nonatomic) NSString *name;
/**
 *  订单号	orderId	varchar
 * orderId
 **/
@property (strong,nonatomic) NSString *orderId
;


/**
 *  图片路径	photoUrl	varchar
 *  商品展览图片 photoUrl
 **/
@property (strong,nonatomic) NSString * photoUrl;

/**
 *  价格	price	varchar
 *  price   商品显示价格
 **/
@property (strong,nonatomic) NSString * price;

/**
 *  商家商家id sellerUserId		varchar
 *  sellerUserId
 **/
@property (strong,nonatomic) NSString *sellerUserId;





/**
 *  商品类型	type	varchar
 *  type
 **/
@property (strong,nonatomic) NSString *type;

/**
 *  商家名称		varchar
 *  sellerName
 **/
@property (strong,nonatomic) NSString *message;

/**
 *  销售数	saleNo	int
 *  saleNo
 **/
@property (strong,nonatomic) NSNumber * saleNo;

/**
 *  评价次数	evaluateNo	int
 *  evaluateNo
 **/
@property (strong,nonatomic) NSNumber * evaluateNo;
/**
 *  评价分数	evaluateScore	int
 * evaluateScore
 **/
@property (strong,nonatomic) NSNumber * evaluateScore;
/**
 *   内容描述	content	varchar、
 *  content
 **/
@property (strong,nonatomic) NSString *content;;
/**
 *  商家userId	userId	varchar
 * userId
 **/
@property (strong,nonatomic) NSString * userId;


@property (atomic,assign) BOOL selectState;




@end

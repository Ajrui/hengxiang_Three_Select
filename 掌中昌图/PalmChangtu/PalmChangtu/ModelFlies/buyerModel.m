//
//  buyerModel.m
//  PalmChangtu
//
//  Created by osoons on 15/6/23.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "buyerModel.h"

@implementation buyerModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"byerId":@"id",
             @"userId":@"userId",
             @"takeOverPhone":@"takeOverPhone",
             @"isPush":@"isPush",
             @"createTime":@"createTime",
             @"updateTime":@"updateTime",
             
             };
    
}

@end

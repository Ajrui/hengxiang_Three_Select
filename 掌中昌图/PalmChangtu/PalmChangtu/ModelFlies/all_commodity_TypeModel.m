//
//  all_commodity_TypeModel.m
//  PalmChangtu
//
//  Created by osoons on 15/6/24.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "all_commodity_TypeModel.h"

@implementation all_commodity_TypeModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
                 @"createTime":@"createTime",
                 @"Typeid":@"id",
                 @"isTop":@"isTop",
                 @"name":@"name",
                 @"orderNo":@"orderNo",
                 @"photoUrl":@"photoUrl",
                 @"parentId":@"parentId",
                 @"updateTime":@"updateTime"
             
             };
    
  
}

@end

//
//  all_commodity_TypeModel.h
//  PalmChangtu
//
//  Created by osoons on 15/6/24.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"

@interface all_commodity_TypeModel : MTLModel<MTLJSONSerializing>

/**
 * 创建时间
 **/
@property (strong,nonatomic) NSString *     createTime;
/**
 *  Typeid id
 **/
@property (assign,nonatomic) NSNumber *    Typeid;
/**
 *  是否置顶
 **/
@property (assign,nonatomic) NSNumber *    isTop;

/**
 * 类型 名字
 **/
@property (strong,nonatomic) NSString *     name;

/**
 * 顺序号
 **/

@property (assign,nonatomic) NSNumber *    orderNo;

/**
 * 图片url
 **/
@property (strong,nonatomic) NSString *     photoUrl;

/**
 * 父id
 **/
@property (strong,nonatomic) NSString *    parentId;

/**
 * 更新时间
 **/
@property (assign,nonatomic) NSString *    updateTime;



@end

//
//  localJSONModel.m
//  PalmChangtu
//
//  Created by osoons on 15/6/26.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "localJSONModel.h"

@implementation localJSONModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             
             @"parentId":@"parentId",
             @"photoUrl":@"photoUrl",
             @"orderNo":@"orderNo",
             @"name":@"name",
             @"isTop":@"isTop",
             @"localTypeID":@"id",
             @"createTime":@"createTime",
             @"updateTime":@"updateTime",
             
             };
}
@end

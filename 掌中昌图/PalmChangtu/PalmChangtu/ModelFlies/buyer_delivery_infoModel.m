//
//  buyer_delivery_infoModel.m
//  PalmChangtu
//
//  Created by osoons on 15/6/23.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "buyer_delivery_infoModel.h"

@implementation buyer_delivery_infoModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"buyer_deliveryId":@"id",
             @"userId":@"userId",
             @"receiverName":@"receiverName",
             @"receiverPhone":@"receiverPhone",
             @"receiverDistrict":@"receiverDistrict",
             @"receiverAddress":@"receiverAddress",
             @"isDefault":@"isDefault",
             @"createTime":@"createTime",
             };
    
}
@end


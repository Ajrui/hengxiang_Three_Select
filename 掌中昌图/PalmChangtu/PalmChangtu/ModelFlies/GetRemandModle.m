//
//  GetRemandModle.m
//  PalmChangtu
//
//  Created by osoons on 15/6/29.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "GetRemandModle.h"

@implementation GetRemandModle
+(NSDictionary *)JSONKeyPathsByPropertyKey
{


    return @{
             @"getRemandId":@"id",
             @"userId":@"userId",
             @"name":@"name",
             @"photoUrl":@"photoUrl",
             @"price":@"price",
             @"afterSalesAddress":@"afterSalesAddress",
             @"content":@"content",
             @"evaluateNo":@"evaluateNo",
             @"evaluateScore":@"evaluateScore",
             @"saleNo":@"saleNo",
             @"sellerName":@"sellerName",
             @"type":@"type",
             @"isValid":@"isValid",
             @"isDelete":@"isDelete",
             @"createTime":@"createTime",
             @"updateTime":@"updateTime",
             @"success":@"success",
             @"message":@"message",
             @"arrRemandData":@"arr"
             };
}

@end

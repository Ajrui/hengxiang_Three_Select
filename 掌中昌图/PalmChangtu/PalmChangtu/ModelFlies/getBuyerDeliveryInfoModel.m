//
//  getBuyerDeliveryInfoModel.m
//  PalmChangtu
//
//  Created by osoons on 15/6/25.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "getBuyerDeliveryInfoModel.h"

@implementation getBuyerDeliveryInfoModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"buyerAddressId":@"id",
             @"userId":@"userId",
             @"receiverName":@"receiverName",
             @"receiverPhone":@"receiverPhone",
             @"receiverDistrict":@"receiverDistrict",
             @"receiverAddress":@"receiverAddress",
             @"isDefault":@"isDefault",
             @"createTime":@"createTime",
             @"updateTime":@"updateTime",
             @"rePostcode":@"rePostcode",
             @"reStreet":@"reStreet"
             };
    
}
@end


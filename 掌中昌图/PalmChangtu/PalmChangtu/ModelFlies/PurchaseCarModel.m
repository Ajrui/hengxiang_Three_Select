

//
//  PurchaseCarModel.m
//  PalmChangtu
//
//  Created by shaorui on 15/7/15.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "PurchaseCarModel.h"

@implementation PurchaseCarModel
+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"userId":@"userId",
             @"afterSalesAddress":@"afterSalesAddress",
             @"commodityColor":@"commodityColor",
             @"commodityId":@"commodityId",
             @"commodityModel":@"commodityModel",
             @"commodityNo":@"commodityNo",
             @"createTime":@"createTime",
             @"updateTime":@"updateTime",
             @"evaluateContent":@"evaluateContent",
             @"evaluateStatus":@"evaluateStatus",
             @"evaluateLevel":@"evaluateLevel",
             @"Purchaseid":@"id",
             @"name":@"name",
             @"orderId":@"orderId",
             @"photoUrl":@"photoUrl",
             @"message":@"message",
             @"price":@"price",
             @"sellerUserId":@"sellerUserId",
             @"type":@"type",
             @"content":@"content",
             @"saleNo":@"saleNo",
             @"evaluateNo":@"evaluateNo",
             @"selectState":@"selectState"
             
             };


    
}

@end

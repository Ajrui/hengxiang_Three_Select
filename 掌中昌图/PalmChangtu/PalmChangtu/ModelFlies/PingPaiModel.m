//
//  PingPaiModel.m
//  PalmChangtu
//
//  Created by osoons on 15/7/13.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "PingPaiModel.h"

@implementation PingPaiModel
+(NSDictionary *)JSONKeyPathsByPropertyKey
{


    return @{
             @"modelid":@"id",
             @"commodityTypeId":@"commodityTypeId",
             @"brandName":@"brandName",
             @"createTime":@"createTime",
             @"updateTime":@"updateTime",
            };
}
@end

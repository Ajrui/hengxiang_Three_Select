//
//  getOrdersModel.m
//  PalmChangtu
//
//  Created by osoons on 15/7/17.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "getOrdersModel.h"

@implementation getOrdersModel
+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
                @"addressId":@"addressId",
                @"buyTime":@"buyTime",
                @"content":@"content",
                @"createTime":@"createTime",
                @"freight":@"freight",
                @"GetOrderIdd":@"id",
                @"number":@"number",
                @"orderCommodity":@"orderCommodity",
                @"orderStatus":@"orderStatus",
                @"price":@"price",
                @"sellerUserId":@"sellerUserId",
                @"takeOverTime":@"takeOverTime",
                @"updateTime":@"updateTime",
                @"userId":@"userId",
                @"phone":@"kuaidi_phone"


             };

};

@end

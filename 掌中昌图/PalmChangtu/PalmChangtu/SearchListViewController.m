//
//  SearchListViewController.m
//  
//
//  Created by shaorui on 15/10/13.
//
//

#import "SearchListViewController.h"
#import "SerchClassTabCell.h"
#import "ShangPinListModel.h"
@interface SearchListViewController ()

@end

@implementation SearchListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_v_Top.lalTitel setText:@"搜索"];
    
    [_v_Top.lalTitel setHidden:NO];
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    
    [_v_Top.btnGoback removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_v_Top.btnGoback addTarget:self action:@selector(btnGoBack:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnSearchAction removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnSearchAction addTarget:self action:@selector(btnSearchActioning:) forControlEvents:
     UIControlEventTouchUpInside];
    [self.textSearch becomeFirstResponder];
    self.textSearch.returnKeyType = UIReturnKeySearch;
    self.textSearch.delegate  =self;
    // Do any additional setup after loading the view from its nib.
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        // Return the number of sections.
  
        return  1;
    }


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
            return  50;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ([_arrData count]) {

            return [_arrData count];
            }
    
    
    else
        return  0;
    
    
}


#pragma mark - Standard TableView delegates
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    

        static NSString * identifier = @"SerchClassTabCell";
        SerchClassTabCell * Cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        Cell.selectionStyle  = UITableViewCellSelectionStyleGray;
    return  Cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    


    
    
}
- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated
{
    
        //    SerchClassTabCell * cell =  (SerchClassTabCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        //    cell.contentView.backgroundColor = [UIColor clearColor];
    
    
    
}
-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return YES;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(void)btnGoBack:(UIButton *)goback

{
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    
    if (textField.returnKeyType == UIReturnKeySearch)
        {
        
        
        [textField resignFirstResponder];

        return NO;
        
        
        }
    else
    
    return YES;
}
-(void)btnSearchActioning:(UIButton *)goback

{
    
    [self.view endEditing:YES];
    
    
    
    [self.delegateKeyWord userSearchTextString:self.textSearch.text];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  AlreadyBoughtCell.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/22.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlreadyBoughtCell : UITableViewCell
/**
 *产品 图片
 *
 */
@property (strong, nonatomic) IBOutlet UIImageView *imageProdect;
/**
 * 名字
 *
 */
@property (strong, nonatomic) IBOutlet UILabel *labProdectName;
/**
 * g购买倍数
 *
 */
@property (strong, nonatomic) IBOutlet UILabel *labXx;

/**
 * 价格
 *
 */
@property (strong, nonatomic) IBOutlet UILabel *labPrce;
/**
 * 去评价
 */
@property (strong, nonatomic) IBOutlet UIButton *btnGoComment;
/**
 * g购买时间
 *
 */
@property (strong, nonatomic) IBOutlet UILabel *labTime;
@end

//
//  MeViewController.m
//  PalmChangtu
//
//  Created by shaorui on 15/6/14.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "ReturnInstructionsViewController.h"
#import "AlreadyBoughtGoodsViewController.h"
#import "AfterComplaintViewController.h"
#import "ShippingAddressViewController.h"
#import "UserRegisterViewController.h"
#import "SetAccountViewController.h"
#import "AcountInfoViewController.h"
#import "InDeliveryViewController.h"
#import "MeranchRegisterViewController.h"
#import "LoginViewController.h"
#import "MeViewController.h"
#import "ShareUserModel.h"
#import "MainTabbarController.h"
#import "ContactUSViewController.h"
#import "ShareUserModel.h"
#import "BaseUrlConfig.h"
#import "AFNetworking.h"
#import "LoadingView.h"
#import "NetWarningView.h"
#import "GTMDefines.h"
#import "GTMBase64.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "NotLoggedInViewController.h"
@interface MeViewController ()

@end

@implementation MeViewController
#pragma mark - 生命周期

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
    }
    return self;
}



-(void)viewWillAppear:(BOOL)animated
{
    ShareUserModel * shareModel = [ShareUserModel shareUserModel];
    
    
    
    
}


-(void)viewDidAppear:(BOOL)animated
{
    
    ShareUserModel * userUpdate = [ShareUserModel shareUserModel];
    
    
    
    self.labNameText.text=  [NSString stringWithFormat:@"欢迎%@登录",userUpdate.user.userName];
    
    if (userUpdate.user.photoUrl.length!=0) {
            //        NSData * data = [[NSData alloc] initWithBase64EncodedString:[ShareUserModel shareUserModel].user.photoUrl options:NSDataBase64DecodingIgnoreUnknownCharacters];
            //
            //
            //        UIImage *_decodedImage = [UIImage imageWithData:data];
        
        
        
        
        NSLog(@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",hostUserImage,userUpdate.user.photoUrl]]);
        
        
        [self.imageHead sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",hostUserImage,userUpdate.user.photoUrl]] placeholderImage:[UIImage imageNamed:@"我的默认头像"]];
    }
    else
        {
        [self.imageHead setImage:[UIImage imageNamed:@"我的默认头像"]];
        
        
        }
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_v_Top.lalTitel setText:@"掌中昌图"];
    
    [_v_Top.btnGoback setHidden:YES];
    _v_Top.parentController = self.navigationController;
    
    [_btnAplayRegister removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnAplayRegister addTarget:self action:@selector(btnRegisterAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnLoginAction removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnLoginAction addTarget:self action:@selector(btnLoginAction:) forControlEvents:UIControlEventTouchUpInside];
    [self  addCustomGestures];

    // Do any additional setup after loading the view from its nib.
}


- (void)btnLoginAction:(UIButton *)btnLogin
{


    NotLoggedInViewController *  NotLoggedInView  =[[NotLoggedInViewController alloc] initWithNibName:@"NotLoggedInViewController" bundle:nil];
    
    [self presentViewController: NotLoggedInView animated:NO completion:NULL];
//    LoginViewController * comment =[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
//    
//    [self presentViewController:comment animated:YES completion:^{
//        
//    }];
//    

    
    
}

- (void)btnRegisterAction:(UIButton *)btnRegister
{
    
//    MeranchRegisterViewController * RegisterView =[[MeranchRegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    UserRegisterViewController * RegisterView =[[UserRegisterViewController alloc] initWithNibName:@"UserRegisterViewController" bundle:nil];

    
    [self presentViewController:RegisterView animated:YES completion:^{
        
    }];
    
    
}

#pragma mark - 添加自定义的手势（若不自定义手势，不需要下面的代码）

- (void)addCustomGestures {
    /*
     *注意：
     *添加自定义手势时，必须设置UIGestureRecognizer的属性cancelsTouchesInView 和 delaysTouchesEnded 为NO,
     *否则影响地图内部的手势处理
     */
    UITapGestureRecognizer *singleTouch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleViewDeliveryTouchTap:)];
    singleTouch.delegate = self;
    singleTouch.numberOfTapsRequired = 1;
    singleTouch.cancelsTouchesInView = NO;
    singleTouch.delaysTouchesEnded = NO;
    
    [self.viewDelivery addGestureRecognizer:singleTouch];
    
    /*
     *注意：
     *添加自定义手势时，必须设置UIGestureRecognizer的属性cancelsTouchesInView 和 delaysTouchesEnded 为NO,
     */
    
//    已购商品
    UITapGestureRecognizer *singleviewAlreadyBoughtGoodsTouchScale = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleviewAlreadyBoughtGoodsTouchGoodsTap:)];
    singleviewAlreadyBoughtGoodsTouchScale.delegate = self;
    singleviewAlreadyBoughtGoodsTouchScale.cancelsTouchesInView = NO;
    singleviewAlreadyBoughtGoodsTouchScale.delaysTouchesEnded = NO;
//    [singleviewAlreadyBoughtGoodsTouchScale requireGestureRecognizerToFail:singleTouch];
    [self.viewAlreadyBoughtGoods addGestureRecognizer:singleviewAlreadyBoughtGoodsTouchScale];
    
//   送货地址
    UITapGestureRecognizer *singleviewGoodsAddressTouchGoods = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleviewAddressTouchTap:)];
        singleviewGoodsAddressTouchGoods.delegate = self;
        singleviewGoodsAddressTouchGoods.cancelsTouchesInView = NO;
        singleviewGoodsAddressTouchGoods.delaysTouchesEnded = NO;
//    [singleviewGoodsAddressTouchGoods requireGestureRecognizerToFail:singleTouchScale];
    [self.viewGoodsAddress addGestureRecognizer:singleviewGoodsAddressTouchGoods];

//账号信息
    UITapGestureRecognizer *singleviewAccountInfo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleleAccountAccountInfo:)];
    singleviewAccountInfo.delegate = self;
    singleviewAccountInfo.cancelsTouchesInView = NO;
    singleviewAccountInfo.delaysTouchesEnded = NO;
        //    [singleviewConnect requireGestureRecognizerToFail:singleviewConnect];
    [self.viewAccountInfo addGestureRecognizer:singleviewAccountInfo];


//    账号设置
    
    UITapGestureRecognizer *singleviewSetAccountInfo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleleSetAccountInfo:)];
    singleviewSetAccountInfo.delegate = self;
    singleviewSetAccountInfo.cancelsTouchesInView = NO;
    singleviewSetAccountInfo.delaysTouchesEnded = NO;
        //    [singleviewConnect requireGestureRecognizerToFail:singleviewConnect];
    [self.viewSet addGestureRecognizer:singleviewSetAccountInfo];
//退货须知
    
    UITapGestureRecognizer *singleviewReturnInstructions = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleSingleviewReturnInstructions:)];
    singleviewReturnInstructions.delegate = self;
    singleviewReturnInstructions.cancelsTouchesInView = NO;
    singleviewReturnInstructions.delaysTouchesEnded = NO;
        //    [singleviewAboutAgreement requireGestureRecognizerToFail:singleTouchScale];
    
    [self.viewReturnInstructions addGestureRecognizer:singleviewReturnInstructions];
    
    
    
        //售后申诉
    
    UITapGestureRecognizer *singleviewAfterComplaint = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleAfterComplaintTouchGoodsTap:)];
    singleviewAfterComplaint.delegate = self;
    singleviewAfterComplaint.cancelsTouchesInView = NO;
    singleviewAfterComplaint.delaysTouchesEnded = NO;
        //    [singleviewAboutAgreement requireGestureRecognizerToFail:singleTouchScale];
    
    [self.viewAfterComplaint addGestureRecognizer:singleviewAfterComplaint];
    

    
   
}



- (void)handlesingleleAccountAccountInfo:(UITapGestureRecognizer *)handlesingleviewAccountInfo
{
    
    
    AcountInfoViewController * AcountInfoView =[[AcountInfoViewController alloc] initWithNibName:@"AcountInfoViewController" bundle:nil];
    
    
    [self.navigationController pushViewController:AcountInfoView animated:YES];
}


//送货地址


- (void)handlesingleviewAddressTouchTap:(UITapGestureRecognizer *)handlesingleviewAddress
{
    
    
    ShippingAddressViewController * ShippingAddress =[[ShippingAddressViewController alloc] initWithNibName:@"ShippingAddressViewController" bundle:nil];
    
    
    [self.navigationController pushViewController:ShippingAddress animated:YES];
}


#pragma mark -  正在送货


- (void)handlesingleViewDeliveryTouchTap:(UITapGestureRecognizer *)handlesingleViewBaseInfo{
    
    
    InDeliveryViewController * InDeliveryView =[[InDeliveryViewController alloc] initWithNibName:@"InDeliveryViewController" bundle:nil];
    
    [self.navigationController pushViewController:InDeliveryView animated:YES];
    
    
}
//    已购商品
- (void)handlesingleviewAlreadyBoughtGoodsTouchGoodsTap:(UITapGestureRecognizer *)handlesingleRelesdGoodsTouchTap {
    
    
    
    
    AlreadyBoughtGoodsViewController * AlreadyBoughtGoodsView =[[AlreadyBoughtGoodsViewController alloc] initWithNibName:@"AlreadyBoughtGoodsViewController" bundle:nil];
    
    
    [self.navigationController pushViewController:AlreadyBoughtGoodsView animated:YES];
    
    
    NSLog(@"my handlesingleTouchTap");
}




    //账号设置
- (void)handlesingleleSetAccountInfo:(UITapGestureRecognizer *)viewSetAccountInfo {
    
    SetAccountViewController * SetAccountView =[[SetAccountViewController alloc] initWithNibName:@"SetAccountViewController" bundle:nil];
    [self.navigationController pushViewController:SetAccountView animated:YES];
    
    
    
}
    //退货须知
- (void)handlesingleSingleviewReturnInstructions:(UITapGestureRecognizer *)viewAccountInfo {
    /*
     *do something
     */
    
    ReturnInstructionsViewController * ReturnInstructions =[[ReturnInstructionsViewController alloc] initWithNibName:@"ReturnInstructionsViewController" bundle:nil];
    [self.navigationController pushViewController:ReturnInstructions animated:YES];
    
    
    
}

    //售后申述
- (void)handlesingleAfterComplaintTouchGoodsTap:(UITapGestureRecognizer *)handlesingleTouchGoodsTap {
    
    AfterComplaintViewController * AfterComplaint =[[AfterComplaintViewController alloc] initWithNibName:@"AfterComplaintViewController" bundle:nil];
    [self.navigationController pushViewController:AfterComplaint animated:YES];
    
    
    
}










- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

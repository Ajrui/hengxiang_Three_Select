//
//  ReturnInstructionsViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/18.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface ReturnInstructionsViewController : UIViewController
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;

@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;

@end

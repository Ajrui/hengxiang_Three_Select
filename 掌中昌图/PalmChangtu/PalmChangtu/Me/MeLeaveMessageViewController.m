//
//  MeLeaveMessageViewController.m
//  PalmChangtu
//
//  Created by osoons on 15/6/24.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "MeLeaveMessageViewController.h"
#import "ShareUserModel.h"
#import "MainTabbarController.h"
#import "JSONKit.h"
#import "Common.h"
#import "LoadingView.h"
#import "NetWarningView.h"
#import "AFNetworking.h"
#import "BaseUrlConfig.h"
@interface MeLeaveMessageViewController ()

@end

@implementation MeLeaveMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_v_Top.lalTitel setText:@"我的留言"];
    
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    _textFeed.delegate = self;
    _textViewFeedConent.delegate = self;
    [_textFeed becomeFirstResponder];
    
    [_btnfeedBack removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnfeedBack addTarget:self action:@selector(btnfeedBackAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    if (textField == _textFeed) {
        [textField resignFirstResponder];
    }
    
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        
        [textView resignFirstResponder];
        return NO;
        
    }
    
    return YES;
    
}


-(void)btnfeedBackAction:(UIButton *)sender
{
    
    
    
    if (strIsEmpty(self.textFeed.text)) {
        
        [[MainTabbarController ShareTabBarController] showMessage:@"留言标题不能为空"];
        return;
        
    }
    else if (strIsEmpty(self.textViewFeedConent.text))
    {
        [[MainTabbarController ShareTabBarController] showMessage:@"留言内容不能为空"];
        
        return;
    }
    
    
    [self FeedBackRequest];
    
}


#pragma mark - network request

-(void)FeedBackRequest
{
    [[LoadingView shareLoadingView] show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,my_messageAction];
    
    
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];
    
    
    
    NSDictionary *params = @{
                             
                             @"userId":[NSString stringWithFormat:@"%@",shareUser.user.user_id],
                             @"messageTitle":[NSString stringWithFormat:@"%@",self.textFeed.text],
                             @"messageContent":[NSString stringWithFormat:@"%@",self.textViewFeedConent.text],
                             @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                             @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId]
                             
                             };
    
    
    
    
    
    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         
         NSDictionary * dicRespon = responseObject;
         [self RequestSuccessWith:dicRespon];
         
         [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [[LoadingView shareLoadingView] dismissAnimated:YES];
         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];
    
}

-(void)RequestSuccessWith:(NSDictionary *)dicRespon
{
    
    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
    {
        
        
        
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        
        [self.navigationController popViewControllerAnimated:YES ];
        
        
    }
    else
    {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

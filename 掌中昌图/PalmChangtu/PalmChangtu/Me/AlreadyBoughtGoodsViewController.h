//
//  AlreadyBoughtGoodsViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/18.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface AlreadyBoughtGoodsViewController : UIViewController<UITextViewDelegate,UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic) IBOutlet UITableView * tableView;
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;

@property (strong, nonatomic) IBOutlet UITextView *textView;

@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
@property (strong, nonatomic) IBOutlet UIImageView *imageHead;
@property (strong, nonatomic) IBOutlet UILabel *labNameText;


/**
 * 确定
 */
@property (strong, nonatomic) IBOutlet UIButton *btnSure;

/**
 *
 * 取消
 */
@property (strong, nonatomic) IBOutlet UIButton * btnCancle;

/**
 * 好评
 */
@property (strong, nonatomic) IBOutlet UIButton *btnGoodComment;

/**
 * 差评
 */
@property (strong, nonatomic) IBOutlet UIButton *btnBadComment;

/**
 * 图片
 */
@property (strong, nonatomic) IBOutlet UIImageView *imageGoodComment;
/**
 * tp 
 */
@property (strong, nonatomic) IBOutlet UIImageView *imageBadComment;

/**
 * 图片
 */
@property (strong, nonatomic) IBOutlet UIView *viewComment;

@property (strong, nonatomic) NSString * evaluateLevel;

@end

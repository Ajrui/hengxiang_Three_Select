//
//  FeedBackViewController.h
//  PalmChangtu
//
//  Created by osoons on 15/6/24.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface FeedBackViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate>
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
@property (weak, nonatomic) IBOutlet UITextField *textFeed;
@property (weak, nonatomic) IBOutlet UITextView *textViewFeedConent;
@property (weak, nonatomic) IBOutlet UIButton *btnfeedBack;

@end

//
//  AcountInfoViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/18.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
#import "BouthLineView.h"
@interface AcountInfoViewController : UIViewController<UIGestureRecognizerDelegate,UITextInputDelegate,UITextFieldDelegate>
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;

@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
/**
 *  @author ruishao
 *
 *  更换手机号
 */
@property (strong, nonatomic) IBOutlet BouthLineView *modifyPhone;
/**
 *  @author ruishao
 *
 *  更换密码
 */
@property (strong, nonatomic) IBOutlet BouthLineView *modeifyPassword;
/**
 *  @author ruishao
 *  更换头像
 *
 */
@property (strong, nonatomic) IBOutlet BouthLineView *imageHead;
@property (strong, nonatomic) IBOutlet UIImageView *imageHeadView;

@property (strong, nonatomic) IBOutlet UILabel *labMuQianDeJiFen;
@property (strong, nonatomic) IBOutlet UILabel *labNameText;


/**
 *
 *  输入的新手机号
 *
 */

@property (strong, nonatomic) NSString * strPhone;
/**
 *  m密码
 */
@property (strong, nonatomic) NSString * passWord;

@property (strong, nonatomic) IBOutlet BouthLineView *modIfyUserName;


@property (strong, nonatomic) IBOutlet BouthLineView *viewDengJi;

@property (strong, nonatomic) IBOutlet BouthLineView *ViewJiFen;

@property (strong, nonatomic) IBOutlet UILabel *labname;

@property (strong,nonatomic)UIImage* image;
@property(strong,nonatomic)IBOutlet UIImageView * imageHeadchange
;
@end

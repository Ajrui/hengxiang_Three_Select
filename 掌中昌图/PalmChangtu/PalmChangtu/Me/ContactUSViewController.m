//
//  ContactUSViewController.m
//  PalmChangtu
//
//  Created by osoons on 15/6/24.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "ContactUSViewController.h"
#import "ShareUserModel.h"
#import "MainTabbarController.h"
#import "JSONKit.h"
#import "Common.h"
#import "LoadingView.h"
#import "NetWarningView.h"
#import "AFNetworking.h"
#import "BaseUrlConfig.h"
@interface ContactUSViewController ()

@end

@implementation ContactUSViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self ContactUSRequest];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_v_Top.lalTitel setText:@"联系我们"];
    
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    // Do any additional setup after loading the view from its nib.
}



#pragma mark - network request

-(void)ContactUSRequest
{
    [[LoadingView shareLoadingView] show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,contact_usAction];
    
    
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];
    
    
    
    NSDictionary *params = @{
                             
                             @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                             @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId]
                             
                             };
    
    
    
    
    
    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         
         NSDictionary * dicRespon = responseObject;
         [self RequestSuccessWith:dicRespon];
         
         [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [[LoadingView shareLoadingView] dismissAnimated:YES];
         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];
    
}

-(void)RequestSuccessWith:(NSDictionary *)dicRespon
{
    
    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
    {
        
        
        
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        
        _lanName.text = [dicRespon objectForKey:@"name"];
        
        NSArray * arr = [dicRespon objectForKey:@"infos"];
        
        if ([arr count]!=0) {
            
            if ([arr objectAtIndex:0]!=nil) {
                _lan1.text = [[[dicRespon objectForKey:@"infos"] objectAtIndex:0] objectForKey:@"name"];
                _labPhoneNum.text = [[[dicRespon objectForKey:@"infos"] objectAtIndex:0] objectForKey:@"value"];
            }
            if ([arr objectAtIndex:1]!=nil) {
                _lan2.text = [[[dicRespon objectForKey:@"infos"] objectAtIndex:1] objectForKey:@"name"];
                _lan2Name.text = [[[dicRespon objectForKey:@"infos"] objectAtIndex:1] objectForKey:@"value"];

            }
            if ([arr objectAtIndex:2]!=nil) {
                
                _lan3.text = [[[dicRespon objectForKey:@"infos"] objectAtIndex:2] objectForKey:@"name"];
                _lan3Name.text = [[[dicRespon objectForKey:@"infos"] objectAtIndex:2] objectForKey:@"value"];
                

            }
            if ([arr objectAtIndex:3]!=nil) {
                
                
                _lan4.text = [[[dicRespon objectForKey:@"infos"] objectAtIndex:3] objectForKey:@"name"];
                _lan4Name.text = [[[dicRespon objectForKey:@"infos"] objectAtIndex:3] objectForKey:@"value"];
                

            }
            if ([arr objectAtIndex:4]!=nil) {
                
                
                
                
                
                _lan5.text = [[[dicRespon objectForKey:@"infos"] objectAtIndex:4] objectForKey:@"name"];
                _lan5Name.text = [[[dicRespon objectForKey:@"infos"] objectAtIndex:4] objectForKey:@"value"];

            }
      
            
            
            
            }
        
      
        


        
        
    }
    else
    {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

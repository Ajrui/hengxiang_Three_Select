//
//  ShippingAddressViewController.m
//  PalmChangtu
//
//  Created by shaorui on 15/6/18.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "ShippingAddressViewController.h"
#import "ShippingAddressCell.h"
#import "ShareUserModel.h"
#import "MainTabbarController.h"
#import "JSONKit.h"
#import "Common.h"
#import "LoadingView.h"
#import "NetWarningView.h"
#import "AFNetworking.h"
#import "BaseUrlConfig.h"
#import "GTMDefines.h"
#import "GTMBase64.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "getBuyerDeliveryInfoModel.h"
#import "MTLJSONAdapter.h"
#import "MJRefresh.h"
@interface ShippingAddressViewController ()
{
    BOOL AddressInfoRequestBool;
    
}
@end

@implementation ShippingAddressViewController
-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
    ShareUserModel * userUpdate = [ShareUserModel shareUserModel];
    self.labNameText.text=  [NSString stringWithFormat:@"欢迎%@登录",userUpdate.user.userName];

    
    if (userUpdate.user.photoUrl.length!=0) {
            //        NSData * data = [[NSData alloc] initWithBase64EncodedString:[ShareUserModel shareUserModel].user.photoUrl options:NSDataBase64DecodingIgnoreUnknownCharacters];
            //
            //
            //        UIImage *_decodedImage = [UIImage imageWithData:data];
        
        
        
        
        NSLog(@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",hostUserImage,userUpdate.user.photoUrl]]);
        
        
        [_imageHead sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",hostUserImage,userUpdate.user.photoUrl]] placeholderImage:[UIImage imageNamed:@"我的默认头像"]];
    }
    else
        {
        [self.imageHead setImage:[UIImage imageNamed:@"我的默认头像"]];
        
        
        }
    

}

-(void)viewDidAppear:(BOOL)animated
{
    if (AddressInfoRequestBool) {
        AddressInfoRequestBool = YES;
        [self AddressInfoRequest];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupRefresh];
    AddressInfoRequestBool = YES;
    _strUrlMetond = [NSString stringWithFormat:@"%@",insert_BuyerDeliveryInfoAction];
    [_v_Top.lalTitel setText:@"送货地址"];
    
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    
    _viewHiden.hidden = YES;
    _dataPicker.hidden = YES;
    
    _textName.delegate = self;
    _textPhone.delegate = self;

    [_btnSetDefault removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnSetDefault addTarget:self action:@selector(btnSetDefault:) forControlEvents:UIControlEventTouchUpInside];
    

    
    [_btnAddress removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnAddress addTarget:self action:@selector(btnAddressAction:) forControlEvents:UIControlEventTouchUpInside];

    
    [_btnAddAddress removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnAddAddress addTarget:self action:@selector(btnAddAddressAction:) forControlEvents:UIControlEventTouchUpInside];


    
    [_btnSaveAction removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnSaveAction addTarget:self action:@selector(btnSaveAction:) forControlEvents:UIControlEventTouchUpInside];

    
    
    [_btnDeleteAction removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [_btnDeleteAction addTarget:self action:@selector(btnDeleteAction:) forControlEvents:UIControlEventTouchUpInside];

    // Do any additional setup after loading the view from its nib.
    //    TableView
    //user nib
    UINib *nibCell = [UINib nibWithNibName:NSStringFromClass([ShippingAddressCell class]) bundle:nil];
    [self.tableView registerNib:nibCell forCellReuseIdentifier:[NSString stringWithFormat:@"%@", [ShippingAddressCell class]]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    _arrData = [[NSMutableArray alloc] init];
    
    
    
    
    _arrPickData = [[NSMutableArray alloc]init];
//                    initWithObjects:@"八面城镇",@"昌图镇",@"老城镇",@"毛家店镇",@"宝力镇",@"亮中桥镇",@"三江口镇",@"双庙子镇",@"泉头镇",@"金家镇",@"朝阳镇",@"头道镇",@"马仲河镇",@"此路树镇",@"老四平镇",@"大洼镇",@"傅家镇",@"四合镇",@"古榆树镇",@"七家子镇",@"前双井子镇",@"四面城镇",@"东嘎镇",@"曲家店镇",@"通江口镇",@"大四家子镇",@"下二台乡,@平安堡乡,@十八家子乡",@"长发乡",@"太平乡",@"大兴乡",@"后窑乡",nil];

    
    [self.dataPicker reloadAllComponents];
    
    
        //售后申诉
    
    UITapGestureRecognizer *singlehiddenView1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesinglehiddenViewTouchTap:)];
    singlehiddenView1.delegate = self;
    singlehiddenView1.cancelsTouchesInView = NO;
    singlehiddenView1.delaysTouchesEnded = NO;
        //    [singleviewAboutAgreement requireGestureRecognizerToFail:singleTouchScale];
    
    [self.hiddenView1 addGestureRecognizer:singlehiddenView1];
    
        //售后申诉
    
    UITapGestureRecognizer *singlehiddenView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesinglehiddenViewTouchTap:)];
    singlehiddenView.delegate = self;
    singlehiddenView.cancelsTouchesInView = NO;
    singlehiddenView.delaysTouchesEnded = NO;
        //    [singleviewAboutAgreement requireGestureRecognizerToFail:singleTouchScale];
    
    [self.hiddenView addGestureRecognizer:singlehiddenView];
    
    [self webGetAreaActionRequest];
    
}
- (void)handlesinglehiddenViewTouchTap:(UITapGestureRecognizer *)handlesingleviewAccountInfo
{
    
    
    [self.view endEditing:YES];
    [[[UIApplication sharedApplication] keyWindow]endEditing:YES];
}

-(void)btnAddressAction:(UIButton *)AddressAction
{
    
    
    _dataPicker.hidden = NO;
   
    
 
}
-(void)btnSetDefault:(UIButton *)setDefault
{
    
    
    getBuyerDeliveryInfoModel  * model = [_arrData objectAtIndex:self.btnSetDefault.tag];
    
    [self SetDefaultInfoRequest:model];
    
}

-(void)btnAddAddressAction:(UIButton *)AddressAction
{
    [_textName becomeFirstResponder];
    
    [_textPhone nextResponder];
    _viewHiden.hidden = NO;
    _btnAddAddress.hidden = YES;
    _btnSetDefault.hidden = YES;
    _btnDeleteAction.hidden = YES;
    [_btnSaveAction setTitle:@"保存" forState:UIControlStateNormal];
    _strUrlMetond = [NSString stringWithFormat:@"%@",insert_BuyerDeliveryInfoAction];

    
    
}

-(void)btnSaveAction:(UIButton *)SaveAction
{
        
    _viewHiden.hidden = YES;
    _dataPicker.hidden = YES;
    _btnAddAddress.hidden = NO;
    _tableView.hidden = NO;
    
    
    
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];
    
    NSDictionary *params  = nil;
    
    if ([_strUrlMetond isEqualToString:insert_BuyerDeliveryInfoAction]) {
        params = @{
                   
                   @"receiverAddress":[NSString stringWithFormat:@"%@",self.textDetailDress.text],
                   @"receiverDistrict":[NSString stringWithFormat:@"%@",self.btnchantu.titleLabel.text],
                   @"reStreet":[NSString stringWithFormat:@"%@",self.btnAddress.titleLabel.text],
                   @"rePostcode":[NSString stringWithFormat:@"%@",self.textBianma.text],

                   @"receiverPhone":[NSString stringWithFormat:@"%@",self.textPhone.text],
                   @"receiverName":[NSString stringWithFormat:@"%@",self.textName.text],

                   @"userId":[NSString stringWithFormat:@"%@",shareUser.user.user_id],
                   
                   @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                   @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId],



                   };

    }
    else if([_strUrlMetond isEqualToString:updateBuyerDeliveryInfoAction])
    {
    
        getBuyerDeliveryInfoModel * model = [_arrData objectAtIndex:self.btnSaveAction.tag];

        params = @{
                    @"id":[NSString stringWithFormat:@"%@",model.buyerAddressId],
                   @"receiverAddress":[NSString stringWithFormat:@"%@",self.textDetailDress.text],
                   @"receiverDistrict":[NSString stringWithFormat:@"%@",self.btnchantu.titleLabel.text],
                    @"reStreet":[NSString stringWithFormat:@"%@",self.btnAddress.titleLabel.text],
                    @"rePostcode":[NSString stringWithFormat:@"%@",self.textBianma.text],

                   @"receiverPhone":[NSString stringWithFormat:@"%@",self.textPhone.text],
                   @"receiverName":[NSString stringWithFormat:@"%@",self.textName.text],
                   @"userId":[NSString stringWithFormat:@"%@",shareUser.user.user_id],
                   
                   @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                   @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId]
                   
                   };

        
    }
    

    
[self ShippingAddressRequest:params];
    
    
    
    
    
    
    

    
    
    
    
    
    
    
}
-(void)btnDeleteAction:(UIButton *)deleteAction
{
    
    
    
    if ([_arrData count]!=0) {
        getBuyerDeliveryInfoModel * model = [_arrData objectAtIndex:self.btnDeleteAction.tag];
        [self DeleteInfoRequest:model];
        
        

    }
    
    
    
    
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView.scrollEnabled == self.tableView.scrollEnabled) {
        [self setTextValuesNil];

    }
}

#pragma mark - standard UIPickerViewDataSource datasource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView

{
    return 1;

}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{

    return _arrPickData.count;


}



#pragma mark - standard UIPickerViewDelegate 



- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString * rowstr = nil;
    if (self.arrPickData.count!=0) {

        NSDictionary * dicPrarms = [self.arrPickData objectAtIndex:row];

            rowstr = [ dicPrarms objectForKey:@"area_name"];

    }



//    [_btnAddress setTitle:rowstr forState:UIControlStateNormal];
    return rowstr;
    
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{

    NSString * rowstr = nil;
    
    if (self.arrPickData.count!=0) {

        NSDictionary * dicPrarms = [self.arrPickData objectAtIndex:row];

        rowstr = [ dicPrarms objectForKey:@"area_name"];
        
    }



    [_btnAddress setTitle:rowstr forState:UIControlStateNormal];

    
    _dataPicker.hidden  =  YES;
    
    
    NSLog(@"%@",[_arrPickData objectAtIndex:row]);
    NSLog(@"%@",[_arrPickData objectAtIndex:component]);

    
    
    
}



#pragma mark - standard tableview datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        // Return the number of sections.
    return 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 80;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([_arrData count])
        {
        return [_arrData count];
        }
    else
        {
        return 0;
        }
    
    
}


#pragma mark - Standard TableView delegates
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * identifier = @"ShippingAddressCell";
    ShippingAddressCell * Cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    Cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    
    if ([_arrData count]!=0) {
        getBuyerDeliveryInfoModel * model = [_arrData objectAtIndex:indexPath.row];
        
        
        Cell.labName.text = model.receiverName;
        
        Cell.labPhone.text = model.receiverPhone;
        Cell.labAddress.text = [NSString stringWithFormat:@"%@%@",model.receiverDistrict,model.receiverAddress];
 
    }
    
                   //        [Cell.imaegProdect sd_setImageWithURL:[NSURL URLWithString:] placeholderImage:[UIImage imageNamed:@"SDWebImageDefault"]];
        
    return Cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    



    if ([self.viewString isEqualToString:@"ShippingAddressViewController"]) {

        [self webGetAreaActionRequest];

        getBuyerDeliveryInfoModel * model = [_arrData objectAtIndex:indexPath.row];

        NSString * strIDName = nil;

        for (NSDictionary * dic in self.arrPickData) {


            if ([model.reStreet isEqualToString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"area_name"]]])
            {


                strIDName   = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]];
                
            }
            
        }

        [self dismissViewControllerAnimated:YES completion:^{


            [self.delegate getBuyerDeliveryInfo:model andTypeIDStr:strIDName];

        }];
    }
    else
    {

        [self btnAddAddressAction:nil];
        if ([_arrData count]!=0) {
            _btnSetDefault.hidden = NO;
            _btnDeleteAction.hidden = NO;
            getBuyerDeliveryInfoModel * model = [_arrData objectAtIndex:indexPath.row];
            self.textName.text = model.receiverName;

            self.textPhone.text = model.receiverPhone;
            self.textDetailDress.text = model.receiverAddress;

            [self.btnAddress setTitle:model.receiverDistrict forState:UIControlStateNormal];

            self.btnDeleteAction.tag = indexPath.row;
            self.btnSaveAction.tag = indexPath.row;
            self.btnSetDefault.tag = indexPath.row;

            _strUrlMetond =[NSString stringWithFormat:@"%@",updateBuyerDeliveryInfoAction];
            [_btnSaveAction setTitle:@"修改并保存" forState:UIControlStateNormal];
            
            
        }


    }



    
}
- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated
{
    
    
    
    
}
-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return YES;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}





#pragma mark - network request  添加地址

-(void)ShippingAddressRequest:(NSDictionary *)params
{
    

    [[LoadingView shareLoadingView] show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,insert_BuyerDeliveryInfoAction];



    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         
         NSDictionary * dicRespon = responseObject;
         [self RequestSuccessWith:dicRespon];
         
         [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [[LoadingView shareLoadingView] dismissAnimated:YES];
         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];
    
}

-(void)RequestSuccessWith:(NSDictionary *)dicRespon
{
    
    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
    {
        
        
        
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        [self setTextValuesNil];
        [self AddressInfoRequest];

        
    }
    else
    {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
    }
    
}

-(void)setTextValuesNil
{
    
    self.textName.text = nil;
    
    self.textPhone.text = nil;
    self.textDetailDress.text = nil;
    
    [self.btnAddress setTitle:@"选择区县" forState:UIControlStateNormal];
    
    _viewHiden.hidden = YES;
    _dataPicker.hidden = YES;
    _btnAddAddress.hidden = NO;
    _tableView.hidden = NO;

    
    


}





#pragma mark - network request  查询地址

-(void)AddressInfoRequest
{
    
    
    [[LoadingView shareLoadingView] show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,getBuyerDeliveryInfoAction];
    
    
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];
    
    //
        NSDictionary *params = @{
                                 
                                @"userId":[NSString stringWithFormat:@"%@",shareUser.user.user_id],
    
                                 @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                                @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId]
    
                                 };

    
    
    
    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         
         NSDictionary * dicRespon = responseObject;
         [self AddressInfoRequestSuccessWith:dicRespon];
         
         [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [[LoadingView shareLoadingView] dismissAnimated:YES];
         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];
    
}

-(void)AddressInfoRequestSuccessWith:(NSDictionary *)dicRespon
{
    
    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
    {
        
        
        
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
    
        NSArray *arrBussi = [dicRespon objectForKey:@"arr"];

        if (arrBussi && [arrBussi isKindOfClass:[arrBussi class]])
        {
        [_arrData removeAllObjects];

            for (NSDictionary *dicTmp in arrBussi)
            {
                NSError *error = nil;
                getBuyerDeliveryInfoModel *modelTmp =
                [MTLJSONAdapter modelOfClass:[getBuyerDeliveryInfoModel class] fromJSONDictionary:dicTmp error:&error];
                if(error == nil)
                {
                    [_arrData addObject:modelTmp];
                }
            }
            
        }

        
        [self.tableView reloadData];
        
        
    }
    else
    {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
    }
    
}



#pragma mark - network request  删除地址地址

-(void)DeleteInfoRequest:(getBuyerDeliveryInfoModel *)model
{
    
    
    [[LoadingView shareLoadingView] show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,delete_BuyerDeliveryInfoAction];
    
    
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];
    
    //
    NSDictionary *params = @{
                             
                             @"id":[NSString  stringWithFormat:@"%@",model.buyerAddressId],

                             
                             @"userId":[NSString stringWithFormat:@"%@",shareUser.user.user_id],
                             
                             @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                             @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId]
                             
                             };
    
    
    
    
    
    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         
         NSDictionary * dicRespon = responseObject;
         [self DeleteInfoRequestSuccessWith:dicRespon];
         
         [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [[LoadingView shareLoadingView] dismissAnimated:YES];
         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];
    
}

-(void)DeleteInfoRequestSuccessWith:(NSDictionary *)dicRespon
{
    
    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
    {
        
        [self setTextValuesNil];
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        
        [_arrData removeObjectAtIndex:self.btnDeleteAction.tag];

            [self.tableView reloadData];
        
        
    }
    else
    {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
    }
    
}

#pragma mark - network request  设为默认地址地址

-(void)SetDefaultInfoRequest:(getBuyerDeliveryInfoModel *)model
{
    
    
    [[LoadingView shareLoadingView] show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,setup_DefalutBuyerDeliveryInfoAction];
    
    
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];
    
    //
    NSDictionary *params = @{
                             
                             @"id":[NSString  stringWithFormat:@"%@",model.buyerAddressId],
                             
                             
                             @"userId":[NSString stringWithFormat:@"%@",shareUser.user.user_id],
                             
                             @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                             @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId]
                             
                             };
    
    
    
    
    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         
         NSDictionary * dicRespon = responseObject;
         [self DeleteInfoRequestSuccessWith:dicRespon];
         
         [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [[LoadingView shareLoadingView] dismissAnimated:YES];
         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];
    
}

-(void)SetDefaultInfoRequestSuccessWith:(NSDictionary *)dicRespon
{
    
    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
    {
        
        [self setTextValuesNil];
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        
        
        [self.tableView reloadData];
        
        
    }
    else
    {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
    }
    
}






#pragma mark - network request  查询区域地址

-(void)webGetAreaActionRequest
{

    [[LoadingView shareLoadingView] show];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServerGetArea,webGetAreaAction];


    [manager POST:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {


         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];

         NSDictionary * dicRespon = responseObject;
         [self webGetAreaActionRequestSuccessWith:dicRespon];

         [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [[LoadingView shareLoadingView] dismissAnimated:YES];
         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];

}

-(void)webGetAreaActionRequestSuccessWith:(NSDictionary *)dicRespon
{

    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
    {



        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];

        [_arrPickData removeAllObjects];

        NSArray *arrBussi = [dicRespon objectForKey:@"list"];

        self.arrPickData = [arrBussi mutableCopy];
        [self.dataPicker reloadAllComponents];


    }
    else
    {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
    }
    
}


#pragma mark 集成刷新控件
- (void)setupRefresh
{
    
        // 1.下拉刷新(进入刷新状态就会调用self的headerRereshing)
    [self.tableView addHeaderWithTarget:self action:@selector(headerRereshing)];
        // 设置文字(也可以不设置,默认的文字在MJRefreshConst中修改)
    self.tableView.headerPullToRefreshText = headerPullToRefreshTextkey;
    self.tableView.headerReleaseToRefreshText  = headerReleaseToRefreshTextkey;
    self.tableView.headerRefreshingText = headerRefreshingTextKey;
    
        // 2.上拉加载更多(进入刷新状态就会调用self的footerRereshing)
    [self.tableView addFooterWithTarget:self action:@selector(footerRereshing)];
    self.tableView.footerPullToRefreshText = footerPullToRefreshTextKey;
    self.tableView.footerReleaseToRefreshText =footerPullToRefreshTextKey;
    self.tableView.footerRefreshingText = footerRefreshingTextKey;
    
}

-(void)headerRereshing{
    
    
    
    
    
    
    
    [self AddressInfoRequest];
    
    
    
    

    [self.tableView headerEndRefreshing];
    
}

-(void)footerRereshing{
    
    
    
    
    
    
    
    
    
    
    [self AddressInfoRequest];
    [self.tableView footerEndRefreshing];

    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

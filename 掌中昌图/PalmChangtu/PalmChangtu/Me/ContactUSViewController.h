//
//  ContactUSViewController.h
//  PalmChangtu
//
//  Created by osoons on 15/6/24.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface ContactUSViewController : UIViewController
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;

@property (weak, nonatomic) IBOutlet UILabel *lan1;

/***联系电话***/
@property (weak, nonatomic) IBOutlet UILabel *labPhoneNum;
/***联系电话***/

@property (weak, nonatomic) IBOutlet UILabel *lanPhoneNum2;
/***公司名称***/

@property (weak, nonatomic) IBOutlet UILabel *lanName;

/**
 * 公司图片logo
 **/
@property (weak, nonatomic) IBOutlet UIImageView *image;



/******/

@property (weak, nonatomic) IBOutlet UILabel *lan2;

@property (weak, nonatomic) IBOutlet UILabel *lan2Name;

@property (weak, nonatomic) IBOutlet UILabel *lan3;

@property (weak, nonatomic) IBOutlet UILabel *lan3Name;

@property (weak, nonatomic) IBOutlet UILabel *lan4;

@property (weak, nonatomic) IBOutlet UILabel *lan4Name;

@property (weak, nonatomic) IBOutlet UILabel *lan5;

@property (weak, nonatomic) IBOutlet UILabel *lan5Name;



@end

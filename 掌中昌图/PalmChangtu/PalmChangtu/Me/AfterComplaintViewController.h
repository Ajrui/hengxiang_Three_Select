//
//  AfterComplaintViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/18.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface AfterComplaintViewController : UIViewController<UIActionSheetDelegate>
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;
/**
 * 发送
 *
 */
@property (strong, nonatomic) IBOutlet UIButton *btnSendAction;

/**
 * 拨打电话
 *
 */
@property (strong, nonatomic) IBOutlet UIButton *btnTellAction;

@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;

@property (weak, nonatomic) IBOutlet UITextView *textViewConnet;

@property (strong, nonatomic) IBOutlet UIView *tab
;
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event;

@end

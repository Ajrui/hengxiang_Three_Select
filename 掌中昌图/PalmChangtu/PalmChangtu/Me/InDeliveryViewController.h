//
//  InDeliveryViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/18.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface InDeliveryViewController : UIViewController<UIActionSheetDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>


@property (strong,nonatomic) IBOutlet UITableView * tableView;
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;
@property (strong, nonatomic) IBOutlet UIImageView *imageHead;
@property (strong, nonatomic) IBOutlet UILabel *labNameText;


@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
@end

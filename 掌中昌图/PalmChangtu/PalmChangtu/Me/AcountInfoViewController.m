//
//  AcountInfoViewController.m
//  PalmChangtu
//
//  Created by shaorui on 15/6/18.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "AcountInfoViewController.h"
#import "JiFenGuiZeViewController.h"
#import "ClassSerchViewController.h"
#import "ProdectDetailViewController.h"
#import "KRLCollectionViewGridLayout.h"
#import "FPPopoverController.h"

#import "FPPopoverView.h"
#import "FPTouchView.h"
#import "CollectionViewController.h"
#import "BaseUrlConfig.h"
#import "AFNetworking.h"
#import "ShareUserModel.h"
#import "SDImageCache.h"
#import "UIButton+WebCache.h"
#import "UIButton+AFNetworking.h"
#import "MenuCell.h"
#import "BannerCell.h"
#import "BusinessGoodsCell.h"
#import "ShareUserModel.h"

#import "JSONKit.h"
#import "Common.h"
#import "LoadingView.h"
#import "NetWarningView.h"
#import "AFNetworking.h"
#import "BaseUrlConfig.h"
#import "TestCollectionViewController.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "Common.h"
#import "all_commodity_TypeModel.h"
#import "localJSONModel.h"
#import "MTLJSONAdapter.h"
#import "MainTabbarController.h"
#import "ContactUSViewController.h"
#import "ShareUserModel.h"
#import "BaseUrlConfig.h"
#import "AFNetworking.h"
#import "LoadingView.h"
#import "NetWarningView.h"
#import "GTMDefines.h"
#import "GTMBase64.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "NotLoggedInViewController.h"
@interface AcountInfoViewController ()

@end

@implementation AcountInfoViewController
-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated
{
    
    ShareUserModel * userUpdate = [ShareUserModel shareUserModel];
    

    self.labNameText.text=  [NSString stringWithFormat:@"欢迎%@登录",userUpdate.user.userName];

    if (userUpdate.user.photoUrl.length!=0) {
            //        NSData * data = [[NSData alloc] initWithBase64EncodedString:[ShareUserModel shareUserModel].user.photoUrl options:NSDataBase64DecodingIgnoreUnknownCharacters];
            //
            //
            //        UIImage *_decodedImage = [UIImage imageWithData:data];
        
        
        
        
        NSLog(@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",hostUserImage,userUpdate.user.photoUrl]]);
        
        
        [self.imageHeadView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",hostUserImage,userUpdate.user.photoUrl]] placeholderImage:[UIImage imageNamed:@"我的默认头像"]];
    }
    else
        {
        [self.imageHeadView setImage:[UIImage imageNamed:@"我的默认头像"]];
        
        
        }
    

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_v_Top.lalTitel setText:@"账号信息"];
    
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    [self addCustomGestures];

    self.labname.text = [ShareUserModel shareUserModel].user.userName;
    UITapGestureRecognizer *imageHeadSingleview = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageHeadSingleviewTouchGoodsTap:)];
    imageHeadSingleview.delegate = self;
    imageHeadSingleview.cancelsTouchesInView = NO;
    imageHeadSingleview.delaysTouchesEnded = NO;
    //    [singleviewAboutAgreement requireGestureRecognizerToFail:singleTouchScale];
    self.imageHeadchange.userInteractionEnabled = YES;
    [self.imageHeadchange  addGestureRecognizer:imageHeadSingleview];
    // Do any additional setup after loading the view from its nib.
}




#pragma mark - 添加自定义的手势（若不自定义手势，不需要下面的代码）

- (void)addCustomGestures {

    UITapGestureRecognizer *singleModTouch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleviemodIfyUserNameTap:)];
    singleModTouch.delegate = self;
    singleModTouch.numberOfTapsRequired = 1;
    singleModTouch.cancelsTouchesInView = NO;
    singleModTouch.delaysTouchesEnded = NO;
    
    [self.modIfyUserName addGestureRecognizer:singleModTouch];

    
    
    UITapGestureRecognizer *viewDengJiTouch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleviewDengJiTouchTap:)];
    viewDengJiTouch.delegate = self;
    viewDengJiTouch.numberOfTapsRequired = 1;
    viewDengJiTouch.cancelsTouchesInView = NO;
    viewDengJiTouch.delaysTouchesEnded = NO;
    
    [self.viewDengJi addGestureRecognizer:viewDengJiTouch];

    UITapGestureRecognizer *ViewJiFenTouch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesinglevViewJiFenTap:)];
    ViewJiFenTouch.delegate = self;
    ViewJiFenTouch.numberOfTapsRequired = 1;
    ViewJiFenTouch.cancelsTouchesInView = NO;
    ViewJiFenTouch.delaysTouchesEnded = NO;
    
    [self.ViewJiFen addGestureRecognizer:ViewJiFenTouch];

    /*
     *注意：
     *添加自定义手势时，必须设置UIGestureRecognizer的属性cancelsTouchesInView 和 delaysTouchesEnded 为NO,
     *否则影响地图内部的手势处理
     */
    UITapGestureRecognizer *singleTouch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesinglevieimageHeadTap:)];
    singleTouch.delegate = self;
    singleTouch.numberOfTapsRequired = 1;
    singleTouch.cancelsTouchesInView = NO;
    singleTouch.delaysTouchesEnded = NO;
    
    [self.imageHead addGestureRecognizer:singleTouch];
    
    /*
     *注意：
     *添加自定义手势时，必须设置UIGestureRecognizer的属性cancelsTouchesInView 和 delaysTouchesEnded 为NO,
     *否则影响地图内部的手势处理
     */
    UITapGestureRecognizer *singleTouchScale = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleViewmodeifyPasswordTap:)];
    singleTouchScale.delegate = self;
    singleTouchScale.cancelsTouchesInView = NO;
    singleTouchScale.delaysTouchesEnded = NO;
    [singleTouchScale requireGestureRecognizerToFail:singleTouch];
    [self.modeifyPassword addGestureRecognizer:singleTouchScale];
    
    
    UITapGestureRecognizer *singleTouchGoods = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlesingleviewmodifyPhoneTap:)];
    singleTouchGoods.delegate = self;
    singleTouchGoods.cancelsTouchesInView = NO;
    singleTouchGoods.delaysTouchesEnded = NO;
    [singleTouchGoods requireGestureRecognizerToFail:singleTouchScale];
    [self.modifyPhone addGestureRecognizer:singleTouchGoods];
    
  
}

-(void)handlesinglevieimageHeadTap:(UITapGestureRecognizer *)tap
{
    
    
    
    
}
-(void)handlesinglevViewJiFenTap:(UITapGestureRecognizer *)tap
{
    
    
    JiFenGuiZeViewController * jf = [[JiFenGuiZeViewController alloc] initWithNibName:@"JiFenGuiZeViewController" bundle:nil];
    
    [self.navigationController pushViewController:jf animated:YES];
}
-(void)handlesingleviewDengJiTouchTap:(UITapGestureRecognizer *)tap
{
    [self MygetUserScoreActionRequest];
    
}


-(void)handlesingleviemodIfyUserNameTap:(UITapGestureRecognizer *)modetap
{






    UIAlertController  * contorell = [UIAlertController alertControllerWithTitle:@"提示" message:@"请输入新的用户名" preferredStyle:UIAlertControllerStyleAlert];


    [contorell addTextFieldWithConfigurationHandler:^(UITextField *textField) {


        textField.delegate = self;

        textField.placeholder = @"请输入新的用户名";
        NSLog(@" self.strPhone%@ " ,textField.text);



    }];




    UIAlertAction * action  = [UIAlertAction actionWithTitle:@"确定更新用户名" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {



        [self.view endEditing:YES];
        UITextField    * textFiled = contorell.textFields.lastObject;
        _strPhone = [NSString stringWithFormat:@"%@",textFiled.text];


        NSLog(@" 定更新用户名 %@ " ,self.strPhone);

        [self updateUserNameActionRequest:textFiled.text];


    }];


    [contorell addAction:action];



    [self presentViewController:contorell animated:YES completion:^{
        
    }];
    
    




}
-(void)handlesingleViewmodeifyPasswordTap:(UITapGestureRecognizer *)tap
{
 
    
    
    
    
    UIAlertController  * contorell = [UIAlertController alertControllerWithTitle:@"提示" message:@"请输入你的手机号和新密码 " preferredStyle:UIAlertControllerStyleAlert];

    [contorell addTextFieldWithConfigurationHandler:^(UITextField *textField) {


        textField.delegate = self;
        textField.keyboardType =UIKeyboardTypeNamePhonePad;
        textField.placeholder = @"请设置新密码";



    }];

    [contorell addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        
        
        textField.delegate = self;
        textField.keyboardType =UIKeyboardTypePhonePad;
        textField.placeholder = @"请输入新的手机号码";
        NSLog(@" self.strPhone%@ " ,textField.text);
        
        
        
    }];
    
    
    
    
    UIAlertAction * action  = [UIAlertAction actionWithTitle:@"修改密码" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
                    [self.view endEditing:YES];
            UITextField    * textFiled = contorell.textFields.firstObject;
            UITextField    * pass = contorell.textFields.lastObject;

            _passWord = [NSString stringWithFormat:@"%@",textFiled.text];
            
            _strPhone = [NSString stringWithFormat:@"%@",pass.text];
            NSLog(@" self.strPhone%@ " ,self.strPhone);
            
            [self getmodeifyPassworRequest];



            
            
        }];
        
        
        [contorell addAction:action];
        
        
        
        [self presentViewController:contorell animated:YES completion:^{
            
        }];
    
    
    
}
-(void)handlesingleviewmodifyPhoneTap:(UITapGestureRecognizer *)tapsing
{
    
    
    
    
    
    UIAlertController  * contorell = [UIAlertController alertControllerWithTitle:@"提示" message:@"请输入你的手机号" preferredStyle:UIAlertControllerStyleAlert];

    
    [contorell addTextFieldWithConfigurationHandler:^(UITextField *textField) {
       
        
        textField.delegate = self;
        textField.keyboardType =UIKeyboardTypePhonePad;
        textField.placeholder = @"请输入新的手机号码";
        NSLog(@" self.strPhone%@ " ,textField.text);
        
        

    }];
        
    
    
    
    UIAlertAction * action  = [UIAlertAction actionWithTitle:@"确定更换手机号" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        
        [self.view endEditing:YES];
        UITextField    * textFiled = contorell.textFields.lastObject;
        _strPhone = [NSString stringWithFormat:@"%@",textFiled.text];
        
        
        NSLog(@" self.strPhone%@ " ,self.strPhone);

        [self getmodifyPhoneRequest];


    }];

    
    [contorell addAction:action];



    [self presentViewController:contorell animated:YES completion:^{
        
    }];
    
    
    
}



#pragma mark - network request getmodeifyPassworRequest

-(void)getmodeifyPassworRequest
{
    [[LoadingView shareLoadingView] show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,modify_passwordAction];
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];
    
    NSString * strPaw = [Common md5_32:_passWord];

    NSDictionary *params = @{
                             
                             @"password":[NSString stringWithFormat:@"%@",strPaw],
                             @"phone":self.strPhone,
                             @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId]

                             };
    
    
    
    
    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
     
     
         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     
     NSDictionary * dicRespon = responseObject;
     [self ModifyMessageRequestSuccessWith:dicRespon];
     
     [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
     [[LoadingView shareLoadingView] dismissAnimated:YES];
     [[NetWarningView sharedNetWarningView] show];  //警告网络异常
                                                    //              NSData *doubi = operation.error    ;
                                                    //                  //     JSON格式的NSString转换成NSMutableDictionary
                                                    //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
                                                    //
                                                    //              NSError *err = nil;
                                                    //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSLog(@"Error: %@", error);
     NSLog(@"请求出错了: - %@ \n%@",
           [error localizedDescription],
           [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];
    
}


#pragma mark - network request getmodifyPhoneRequest

-(void)getmodifyPhoneRequest
{
    [[LoadingView shareLoadingView] show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,modify_phoneAction];
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];
    
    
    
    NSDictionary *params = @{
                             
                             @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.loginName],
                             @"phone":self.strPhone,
                             @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId]
                             };
    
    
    
    
    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
     
     
         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     
     NSDictionary * dicRespon = responseObject;
     [self ModifyMessageRequestSuccessWith:dicRespon];



     
     [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
     [[LoadingView shareLoadingView] dismissAnimated:YES];
     [[NetWarningView sharedNetWarningView] show];  //警告网络异常
                                                    //              NSData *doubi = operation.error    ;
                                                    //                  //     JSON格式的NSString转换成NSMutableDictionary
                                                    //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
                                                    //
                                                    //              NSError *err = nil;
                                                    //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSLog(@"Error: %@", error);
     NSLog(@"请求出错了: - %@ \n%@",
           [error localizedDescription],
           [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];
    
}

#pragma mark - network request updateUserNameActionRequest

-(void)updateUserNameActionRequest:(NSString *)userName
{
    [[LoadingView shareLoadingView] show];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,updateUserNameAction];
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];



    NSDictionary *params = @{

                             @"userName":userName,
                             @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId],
                             @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.loginName],
                             @"userId":[NSString stringWithFormat:@"%@",shareUser.user.user_id]


                             };




    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {


         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];

         NSDictionary * dicRespon = responseObject;
         [self ModifyMessageRequestSuccessWith:dicRespon];


         self.labname.text =_strPhone;
         [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [[LoadingView shareLoadingView] dismissAnimated:YES];
         [[NetWarningView sharedNetWarningView] show];  //警告网络异常
         //              NSData *doubi = operation.error    ;
         //                  //     JSON格式的NSString转换成NSMutableDictionary
         //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //              NSError *err = nil;
         //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
         NSLog(@"Error: %@", error);
         NSLog(@"请求出错了: - %@ \n%@",
               [error localizedDescription],
               [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];

}


-(void)ModifyMessageRequestSuccessWith:(NSDictionary *)dicRespon
{

    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
        {

        
            [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        
        }
    else
        {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        }
    
}



#pragma mark - network request getUserScoreAction

-(void)MygetUserScoreActionRequest
{
    [[LoadingView shareLoadingView] show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,getUserScoreAction];
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];
    
    
    
    NSDictionary *params = @{
                             

                             @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId],
                             @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.loginName],
                             @"userId":[NSString stringWithFormat:@"%@",shareUser.user.user_id]
                             
                             
                             };
    
    
    
    
    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
     
     
         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     
     NSDictionary * dicRespon = responseObject;
     [self getUserScoreActionviewJiFenRequestSuccessWith:dicRespon];
     
     

     [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
     [[LoadingView shareLoadingView] dismissAnimated:YES];
     [[NetWarningView sharedNetWarningView] show];  //警告网络异常
                                                    //              NSData *doubi = operation.error    ;
                                                    //                  //     JSON格式的NSString转换成NSMutableDictionary
                                                    //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
                                                    //
                                                    //              NSError *err = nil;
                                                    //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSLog(@"Error: %@", error);
     NSLog(@"请求出错了: - %@ \n%@",
           [error localizedDescription],
           [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];
    
}


-(void)getUserScoreActionviewJiFenRequestSuccessWith:(NSDictionary *)dicRespon
{
    
    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
        {
        
     
        
        
        self.labMuQianDeJiFen.text = [NSString stringWithFormat:@"你目前的积分等级是%@ 分",[dicRespon objectForKey:@"score"]];
        
        
        
        
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        
        }
    else
        {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        }
    
}


#pragma mark -  上传头像

- (void)imageHeadSingleviewTouchGoodsTap:(UITapGestureRecognizer *)imageHeadSingleview
{

    UIAlertController * alter  = [[UIAlertController alloc] init] ;


    UIAlertAction * alterTakeOpen = [UIAlertAction actionWithTitle:@"相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * __nonnull action) {
        [self pickImageFromAlbum];
    }];
    UIAlertAction * alterShoot = [UIAlertAction actionWithTitle:@"相机拍摄" style:UIAlertActionStyleDefault handler:^(UIAlertAction * __nonnull action) {
        [self takePhotoOpen];

    }];


    UIAlertAction * alterCancle = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * __nonnull action) {


    }];


    [alter addAction:alterTakeOpen];

    [alter addAction:alterShoot];

    [alter addAction:alterCancle];

    [self presentViewController:alter animated:YES completion:^{
        
    }];
    
    
    
    
}


//打开摄像头
-(void)takePhotoOpen
{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    //判断是否有摄像头
    if(![UIImagePickerController isSourceTypeAvailable:sourceType])
    {
        sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }

    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;   // 设置委托
    imagePickerController.sourceType = sourceType;
    imagePickerController.allowsEditing = YES;
    [self presentViewController:imagePickerController animated:YES completion:nil];  //需要以模态的形式展示
}

#pragma mark 从用户相册获取活动图片
- (void)pickImageFromAlbum
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];

    imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    imagePickerController.allowsEditing = YES;
    [self presentViewController:imagePickerController animated:YES completion:nil];  //需要以模态的形式展示
}


//完成拍照
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{}];

    self.image = [info objectForKey:UIImagePickerControllerEditedImage];

    if (self.image == nil)
    {
        self.image = [info objectForKey:UIImagePickerControllerOriginalImage];

    }
    [self.imageHeadchange setImage:self.image];


    UIAlertController * alter  = [[UIAlertController alloc] init] ;


    UIAlertAction * alterUpload = [UIAlertAction actionWithTitle:@"上传头像" style:UIAlertActionStyleDefault handler:^(UIAlertAction * __nonnull action) {


        NSData* pictureData = UIImageJPEGRepresentation(self.image,0.5);
        NSString *encodedImageStr = [pictureData base64Encoding];//图片转码成为base64Encoding，

        NSDictionary *dicParamJSON   = @{
                                         @"sessionId":[NSString stringWithFormat:@"%@",[ShareUserModel shareUserModel].user.sessionId],
                                         @"loginName":[NSString stringWithFormat:@"%@",[ShareUserModel shareUserModel].user.phone],
                                         @"img_str":encodedImageStr,
                                         @"userId":[NSString stringWithFormat:@"%@",[ShareUserModel shareUserModel].user.user_id]

                                         };

        [self ImageHeadRequest:dicParamJSON];
    }];




    UIAlertAction * alterCancle = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * __nonnull action) {
        [self.imageHeadchange setImage:[UIImage imageNamed:@"我的默认头像"]];
    }];

    [alter addAction:alterUpload];
    [alter addAction:alterCancle];
    [self presentViewController:alter animated:YES completion:^{

    }];

}
//用户取消拍照
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{



    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - //请求数据  上传头像

-(void)ImageHeadRequest:(NSDictionary *)param
{

    [[LoadingView shareLoadingView] show];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setTimeoutInterval:10];
    //    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应


    NSString *   strUrl = [NSString stringWithFormat:@"%@%@",HostServer,analysis_imgAction];
    //    NSString *userShopID = [[NSUserDefaults standardUserDefaults]valueForKey:keyUser_id];

    [manager POST:strUrl parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {


        NSLog(@"%@",responseObject);


        NSDictionary * dicRespon  = responseObject;
        [self ImageHeadRequestWith:dicRespon];

        [[LoadingView shareLoadingView] dismissAnimated:YES];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

        [[LoadingView shareLoadingView] dismissAnimated:YES];
        [[NetWarningView sharedNetWarningView] show];  //警告网络异常
        NSLog(@"Error: %@", error);
        NSLog(@"请求出错了: - %@ %@",
              [error localizedDescription],
              [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    }];




}

-(void)ImageHeadRequestWith:(NSDictionary *)dicRespon
{

    NSString * success = [dicRespon objectForKey:@"success"];

    if ( success.integerValue == 1)
    {

        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        
        ShareUserModel * userUpdate = [ShareUserModel shareUserModel];
        
        NSString * img_str = [dicRespon objectForKey:@"img_str"];
        
        [userUpdate.user setPhotoUrl:img_str];
        
        [self viewDidAppear:YES];
        
    }
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

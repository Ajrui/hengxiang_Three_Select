//
//  ShippingAddressViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/18.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
#import "SettlementViewController.h"
@interface ShippingAddressViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIGestureRecognizerDelegate,getBuyerDeliveryInfoValueDelegate>
@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;
@property (strong, nonatomic) IBOutlet UIImageView *imageHead;

@property (strong, nonatomic) UIImage * image;

@property(nonatomic,assign) id <getBuyerDeliveryInfoValueDelegate> delegate;
@property (strong,nonatomic) NSString * strUrlMetond;
@property (strong,nonatomic) IBOutlet UILabel *labNameText;

@property (strong,nonatomic) IBOutlet UITableView * tableView;
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;
/**
 *
 * arrPickData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrPickData;





/**
 *
 * 添加信新地址
 */
@property (strong, nonatomic) IBOutlet UIButton *btnAddAddress;

/**
 * 收货人姓名
 */

@property (strong, nonatomic) IBOutlet UITextField *textName;

/**
 * 收货人电话
 */
@property (strong, nonatomic) IBOutlet UITextField *textPhone;


/**
 * 收货地址
 */
@property (strong, nonatomic) IBOutlet UIButton *btnAddress;


/**
 * 收货人详细地址
 */
@property (strong, nonatomic) IBOutlet UITextField *textDetailDress;

/**
 *  删除地址
 */
@property (strong, nonatomic) IBOutlet UIButton *btnDeleteAction;

/**
 * 设为默认
 */
@property (strong, nonatomic) IBOutlet UIButton *btnSetDefault;

/**
 * 保存
 */
@property (strong, nonatomic) IBOutlet UIButton *btnSaveAction;

/**
 * 显示或影藏
 */
@property (strong, nonatomic) IBOutlet UIScrollView *viewHiden;

/**
 * dataPicker
 */
@property (strong, nonatomic) IBOutlet UIPickerView *dataPicker;
@property (strong, nonatomic) IBOutlet UIButton *btnchantu;

/**
 *
 */
@property (strong, nonatomic) IBOutlet UIView *hiddenView;

/**
 *
 */
@property (strong, nonatomic) IBOutlet UIView *hiddenView1;
@property (strong, nonatomic) IBOutlet UITextField *textBianma;


@property (strong,nonatomic) NSString * viewString;

@end

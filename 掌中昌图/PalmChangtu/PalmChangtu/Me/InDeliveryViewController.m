//
//  InDeliveryViewController.m
//  PalmChangtu
//
//  Created by shaorui on 15/6/18.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import "InDeliveryViewController.h"
#import "InDeliveryTabCell.h"

#import "CommentViewController.h"
#import "commentsCell.h"
#import "BaseUrlConfig.h"
#import "BannerCell.h"
#import "ShareUserModel.h"
#import "MainTabbarController.h"
#import "JSONKit.h"
#import "Common.h"
#import "LoadingView.h"
#import "NetWarningView.h"
#import "AFNetworking.h"
#import "BaseUrlConfig.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "all_commodity_TypeModel.h"
#import "localJSONModel.h"
#import "MTLJSONAdapter.h"
#import "GetRemandModle.h"
#import "ShangPinListModel.h"
#import "PurchaseCarModel.h"
#import "getOrdersModel.h"
#import "MJRefresh.h"
@interface InDeliveryViewController ()
{

    
    BOOL request;
    
    NSInteger currentNum;
    NSInteger  pageNum;
}
@end

@implementation InDeliveryViewController
-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{

    
    if (request) {
        request=NO;


        ShareUserModel * userUpdate = [ShareUserModel shareUserModel];

        self.labNameText.text=  [NSString stringWithFormat:@"欢迎%@登录",userUpdate.user.userName];

        if (userUpdate.user.photoUrl.length!=0) {

        NSLog(@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",hostUserImage,userUpdate.user.photoUrl]]);


        [_imageHead sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",hostUserImage,userUpdate.user.photoUrl]] placeholderImage:[UIImage imageNamed:@"我的默认头像"]];

    }
    else
    {
        [self.imageHead setImage:[UIImage imageNamed:@"我的默认头像"]];


    }


        [self getOrdersActionActionRequest];

        
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    request = YES;
    currentNum = 0;
    pageNum = 10;
    
    [_v_Top.lalTitel setText:@"正在送货"];
    
    [_v_Top.btnGoback setHidden:NO];
    _v_Top.parentController = self.navigationController;
    [self setupRefresh];
    
    UINib *nibCell = [UINib nibWithNibName:NSStringFromClass([InDeliveryTabCell class]) bundle:nil];
    [self.tableView registerNib:nibCell forCellReuseIdentifier:[NSString stringWithFormat:@"%@", [InDeliveryTabCell class]]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    _arrData = [[NSMutableArray alloc] init];

    // Do any additional setup after loading the view from its nib.
}


#pragma mark - standard tableview datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        // Return the number of sections.
    return 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 120;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([_arrData count])
        {
        return [_arrData count];
        }
    else
        {
        return 0;
        }
    
    
}


#pragma mark - Standard TableView delegates
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * identifier = @"InDeliveryTabCell";
    InDeliveryTabCell * Cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    Cell.selectionStyle  = UITableViewCellSelectionStyleNone;

    
    if ([_arrData count]!=0) {
        
        getOrdersModel * modeltemp  = [_arrData objectAtIndex:indexPath.row];

        if (modeltemp.orderCommodity.count!=0) {
        
            Cell.labProdectName.text = [[modeltemp.orderCommodity objectAtIndex:0] objectForKey:@"name"];
            [Cell.imageProdect sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",HostImage,[[modeltemp.orderCommodity objectAtIndex:0] objectForKey:@"photoUrl"]]]];
            Cell.labXx.text = [NSString stringWithFormat:@"%@X", [[modeltemp.orderCommodity objectAtIndex:0] objectForKey:@"commodityNo"]];

                    Cell.labPrce.text = [NSString stringWithFormat:@"¥%@",[[modeltemp.orderCommodity objectAtIndex:0] objectForKey:@"price"]];



            if (modeltemp.phone == nil) {
//
                [Cell.btnKuaiDi setTitle:[NSString stringWithFormat:@"联系快递"] forState:UIControlStateNormal];

            }
            else
                {
                
                [Cell.btnKuaiDi setTitle:[NSString stringWithFormat:@"%@",@"联系快递"] forState:UIControlStateNormal];
                
                }
            



        }
        [Cell.btnKuaiDi removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
        [Cell.btnKuaiDi addTarget:self action:@selector(tapCallNumber:) forControlEvents:UIControlEventTouchUpInside];

        Cell.labTime.text = modeltemp.buyTime;


    }
        //        [Cell.imaegProdect sd_setImageWithURL:[NSURL URLWithString:] placeholderImage:[UIImage imageNamed:@"SDWebImageDefault"]];
    
    return Cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
        //    InDeliveryTabCell * cell =  (InDeliveryTabCell *)[tableView cellForRowAtIndexPath:indexPath];
        //    cell.contentView.backgroundColor = [UIColor clearColor];
        //
        //
        //    ProdectDetailViewController * detail  = [[ProdectDetailViewController alloc] initWithNibName:@"ProdectDetailViewController" bundle:nil  ];
        //
        //    [self.navigationController pushViewController:detail animated:YES ];
    
    
    
}
- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated
{
    
//    InDeliveryTabCell * cell =  (InDeliveryTabCell *)[self.tableView cellForRowAtIndexPath:indexPath];
//    cell.contentView.backgroundColor = [UIColor clearColor];
    
    
    
}
-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return YES;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
#pragma mark - controller events
-(void)tapCallNumber:(UIButton *)button
{
    
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"是否拨打"
                                                       delegate:self
                                              cancelButtonTitle:@"取消"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:@"180-1234-5678", nil];
    [sheet dismissWithClickedButtonIndex:1 animated:YES];
    sheet.delegate =self;
    [sheet setTag:0];
    [sheet showInView:self.view];
    
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (actionSheet.tag) {
        case 0:
        {
        if (buttonIndex == 0)
            {
            NSURL *phoneNumberURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [actionSheet buttonTitleAtIndex:0]]];
            
            [[UIApplication sharedApplication] openURL:phoneNumberURL];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark 集成刷新控件
- (void)setupRefresh
{
    
        // 1.下拉刷新(进入刷新状态就会调用self的headerRereshing)
    [self.tableView addHeaderWithTarget:self action:@selector(headerRereshing)];
        // 设置文字(也可以不设置,默认的文字在MJRefreshConst中修改)
    self.tableView.headerPullToRefreshText = headerPullToRefreshTextkey;
    self.tableView.headerReleaseToRefreshText  = headerReleaseToRefreshTextkey;
    self.tableView.headerRefreshingText = headerRefreshingTextKey;
    
        // 2.上拉加载更多(进入刷新状态就会调用self的footerRereshing)
    [self.tableView addFooterWithTarget:self action:@selector(footerRereshing)];
    self.tableView.footerPullToRefreshText = footerPullToRefreshTextKey;
    self.tableView.footerReleaseToRefreshText =footerPullToRefreshTextKey;
    self.tableView.footerRefreshingText = footerRefreshingTextKey;
    
}

-(void)headerRereshing{
    
    currentNum  = pageNum;
    
    
    pageNum = currentNum + pageNum ;
    NSLog(@"--------%d-----\n--------%d",currentNum,pageNum);
    
    
    
    [self getOrdersActionActionRequest];
    
    
    [self.tableView headerEndRefreshing];
    
}

-(void)footerRereshing{
    
    currentNum  = pageNum;
    
    
    pageNum = currentNum + pageNum ;
    
    
    NSLog(@"--------%d-----\n--------%d",currentNum,pageNum);
    
    
    
    [self getOrdersActionActionRequest];
    [self.tableView footerEndRefreshing];
    
    
    
}

//getOrdersAction

#pragma mark - network request getOrdersAction

-(void)getOrdersActionActionRequest

{
    [[LoadingView shareLoadingView] show];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFJSONResponseSerializer serializer];//响应
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",HostServer,getOrdersAction];
    
    ShareUserModel * shareUser  = [ShareUserModel shareUserModel];
    
    
    NSDictionary * params = @{
                              
                              @"orderStatus":[NSString stringWithFormat:@"%@",@"2"],
                              @"currentNum":[NSString stringWithFormat:@"%@",[NSNumber numberWithInteger:currentNum]],
                              @"pageNum":[NSString stringWithFormat:@"%@",[NSNumber numberWithInteger:pageNum]],
                              @"loginName":[NSString stringWithFormat:@"%@",shareUser.user.phone],
                              @"sessionId":[NSString stringWithFormat:@"%@",shareUser.user.sessionId],
                              @"userId":[NSString stringWithFormat:@"%@",shareUser.user.user_id]
                              
                              };
    
    
    
    
    
    
    [manager POST:strUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
     
     
         //               NSData *doubi = responseObject   ;
         //                   //     JSON格式的NSString转换成NSMutableDictionary
         //               NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
         //
         //               NSError *err = nil;
         //               NSDictionary *dicRespon  = [NSJSONSerialization JSONObjectWithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     
     NSDictionary * dicRespon = responseObject;
     [self getOrdersActionSuccessWith:dicRespon];
     
     
     
     [[LoadingView shareLoadingView] dismissAnimated:YES];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
     [[LoadingView shareLoadingView] dismissAnimated:YES];
     [[NetWarningView sharedNetWarningView] show];  //警告网络异常
                                                    //              NSData *doubi = operation.error    ;
                                                    //                  //     JSON格式的NSString转换成NSMutableDictionary
                                                    //              NSString *strImge =  [[NSString alloc]initWithData:doubi encoding:NSUTF8StringEncoding];
                                                    //
                                                    //              NSError *err = nil;
                                                    //              NSDictionary *dicRespon  = [NSJSONSerialization JSONObject9WithData:[strImge dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
     NSLog(@"Error: %@", error);
     NSLog(@"请求出错了: - %@ \n%@",
           [error localizedDescription],
           [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
     }];
    
}

-(void)getOrdersActionSuccessWith :(NSDictionary *)dicRespon
{
    
    
    
    NSString * code = [dicRespon objectForKey:@"success"];
    if (code.integerValue ==1)
        {
        
        

            NSArray *arrBussi = [dicRespon objectForKey:@"arr"];
        
        if (arrBussi.count==0) {
            
            currentNum  = 0 ;
            
            
            pageNum = 10 ;
            return ;
            
        }
            if (arrBussi && [arrBussi isKindOfClass:[arrBussi class]])
            {
            
                 [_arrData removeAllObjects];

                for (NSDictionary *dicTmp in arrBussi)
                {
                    NSError *error = nil;
                    getOrdersModel *modelTmp =
                    [MTLJSONAdapter modelOfClass:[getOrdersModel class] fromJSONDictionary:dicTmp error:&error];
                    if(error == nil)
                    {
                        [_arrData addObject:modelTmp];
                    }
                }

            }
                [self.tableView reloadData];
        
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        
        }
    else
        {
        [[MainTabbarController ShareTabBarController] showMessage:[dicRespon objectForKey:@"message"]];
        
        }
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

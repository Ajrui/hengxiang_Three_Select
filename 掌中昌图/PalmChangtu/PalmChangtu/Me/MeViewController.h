//
//  MeViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/14.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
#import "BouthLineView.h"
@interface MeViewController : UIViewController<UIGestureRecognizerDelegate>
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;

@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;

@property (strong, nonatomic) IBOutlet UILabel *labMuQianDeJiFen;
@property (strong, nonatomic) IBOutlet UILabel *labNameText;

@property (strong, nonatomic) IBOutlet UIImageView *imageHead;

/**
 *
 * 申请注册
 */
@property (strong, nonatomic) IBOutlet UIButton *btnAplayRegister;


/**
 * 账号登录
 *
 */
@property (strong, nonatomic) IBOutlet UIButton *btnLoginAction;

/**
 * 正在送货
 *
 */
@property (strong, nonatomic) IBOutlet BouthLineView *viewDelivery;
/**
 *
 * 已购商品
 */
@property (strong, nonatomic) IBOutlet BouthLineView *viewAlreadyBoughtGoods;

/**
 * 账号信息
 *
 */
@property (strong, nonatomic) IBOutlet BouthLineView *viewAccountInfo;


/**
 * 送货地址
 *
 */
@property (strong, nonatomic) IBOutlet BouthLineView *viewGoodsAddress;

/**
 * 相关协议
 *
 */
@property (strong, nonatomic) IBOutlet BouthLineView *viewSet;

/**
 * 退货须知
 *
 */
@property (strong, nonatomic) IBOutlet BouthLineView *viewReturnInstructions;


/**
 * 售后申诉
 *
 */
@property (strong, nonatomic) IBOutlet BouthLineView *viewAfterComplaint;


/**
 *
 *
 */

/**
 *
 *
 */

/**
 *
 *
 */
/**
 *
 *
 */
/**
 *
 *
 */
/**
 *
 *
 */


@end

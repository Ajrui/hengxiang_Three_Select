//
//  ShippingAddressCell.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/20.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShippingAddressCell : UITableViewCell
/**
 *电话
 **/
@property (weak, nonatomic) IBOutlet UILabel *labPhone;
/**
 *名字
 **/

@property (weak, nonatomic) IBOutlet UILabel *labName;
/**
 *地址
 **/


@property (weak, nonatomic) IBOutlet UILabel *labAddress;

@end

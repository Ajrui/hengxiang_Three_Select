//
//  SetAccountViewController.h
//  PalmChangtu
//
//  Created by shaorui on 15/6/18.
//  Copyright (c) 2015年 PalmChangtu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITopBanner.h"
@interface SetAccountViewController : UIViewController
/**
 *
 * arrData 数据元
 */
@property (strong,nonatomic) NSMutableArray * arrData;

@property (strong,nonatomic) IBOutlet UITopBanner * v_Top;

/**
 * 推送开关
 */
@property (strong, nonatomic) IBOutlet UISwitch *switchPush;
/**
 * 版本号
 */
@property (strong, nonatomic) IBOutlet UILabel *labVersionsCode;
/**
 * 内存大小
 */
@property (strong, nonatomic) IBOutlet UILabel *labNeiCun;
/**
 * 意见反馈
 **/

@property (strong, nonatomic) IBOutlet UIButton *btnFeedBack;
/**
 * 关于我们
 */
@property (strong, nonatomic) IBOutlet UIButton *btnAboutWe;
/**
 * 给我们留言
 */
@property (strong, nonatomic) IBOutlet UIButton *btnLeaveMessage;
/**
 * 退出登录
 **/
@property (strong, nonatomic) IBOutlet UIButton *btnLoginOut;

@property (weak, nonatomic) IBOutlet UIControl *btnLoginOutAction;


@property (strong, nonatomic) IBOutlet UIButton *btnClearCache;


@end
